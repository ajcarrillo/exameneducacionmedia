# -*- encoding: utf-8 -*
from django import forms

from app.accounts.forms import NewUserForm, UserPasswordForm
from app.mgeo.forms import DomicilioForm
from app.mgeo.models import Localidad, Municipio
from app.planteles.models import OfertaEducativa, Plantel, Especialidad, Subsistema, PoblacionEstudiantil
from exameneducacionmedia.utils import BetterForm
from general.magicnumbers import PROGRAMAS_ESTUDIO, PROGRAMAS_ESTUDIO_AND_EMPTY


class PlantelForm(BetterForm, DomicilioForm, NewUserForm, UserPasswordForm):
    referencia = forms.CharField(label='Nombre', widget=forms.TextInput(
        attrs={'class': 'form-control', 'required': 'required', 'autofocus': 'autofocus'}))
    cct = forms.CharField(label='CCT', widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}))
    telefono = forms.CharField(label='Teléfono ', widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}))
    pagina_web = forms.URLField(label='Página Web ',
                               widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required', 'pattern': 'http?://.+', 'placeholder': 'http://www.example.com'}))


class PlantelFormUpdate(BetterForm, DomicilioForm):
    referencia = forms.CharField(label='Nombre', widget=forms.TextInput(
        attrs={'class': 'form-control', 'required': 'required', 'autofocus': 'autofocus'}))
    cct = forms.CharField(label='CCT', widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}))
    telefono = forms.CharField(label='Teléfono ',
                               widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}))
    pagina_web = forms.URLField(label='Página Web ',
                                widget=forms.TextInput(
                                    attrs={'class': 'form-control', 'required': 'required', 'pattern': 'http?://.+',
                                           'placeholder': 'http://www.example.com'}))


class OfertaEducativaForm(forms.Form):
    referencia = forms.CharField(label='Especialidad:', widget=forms.TextInput(attrs={'class': 'form-control'}),
                                 required=True)
    descripcion = forms.CharField(label='Descripción:', widget=forms.Textarea(attrs={'class': 'form-control'}),
                                  required=True)
    clave_especialidad = forms.CharField(label='Clave:', widget=forms.TextInput(attrs={'class': 'form-control'}),
                                         required=True)
    programa_estudio = forms.ChoiceField(label='Programa:', widget=forms.Select(attrs={'class': 'form-control'}),
                                         required=True,
                                         choices=PROGRAMAS_ESTUDIO)
    activa = forms.BooleanField(initial=True, widget=forms.HiddenInput())
    grupos0 = forms.IntegerField(label='Total de grupos de nuevo ingreso',
                                 widget=forms.NumberInput(
                                     attrs={'class':         'form-control text-right', 'onfocus': 'this.select()',
                                            'data-sentinel': 'total0'}), min_value=0,
                                 required=True, initial=0)
    alumnos0 = forms.IntegerField(label='Total por grupos',
                                  widget=forms.NumberInput(
                                      attrs={'class':         'form-control text-right', 'onfocus': 'this.select()',
                                             'data-sentinel': 'total0'}), min_value=0,
                                  required=True, initial=0)


class AulaPlantelForm(forms.Form):
    referencia = forms.CharField(label='Aula', widget=forms.TextInput(attrs={'class': 'form-control'}), required=True)
    capacidad = forms.IntegerField(label='Capacidad', widget=forms.NumberInput(attrs={'class': 'form-control'}), required=True, initial=40)


class OfertaEducativaModelForm(forms.ModelForm):
    plantel = forms.IntegerField(widget=forms.HiddenInput())
    especialidad = forms.ModelChoiceField(Especialidad.objects.none(), empty_label='Especialidad',
                                          widget=forms.Select(attrs={'class': 'form-control'}))
    clave_especialidad = forms.CharField(max_length=15, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Clave especialidad'}))
    programa_estudio = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=PROGRAMAS_ESTUDIO_AND_EMPTY)
    activa = forms.BooleanField(widget=forms.HiddenInput(), initial=1)

    def __init__(self, *args, **kwargs):
        subsistema = kwargs.pop('subsistema', None)
        super(OfertaEducativaModelForm, self).__init__(*args, **kwargs)

        self.fields['especialidad'].queryset = Especialidad.objects.filter(subsistema=subsistema)

    def clean_plantel(self):
        data = self.cleaned_data['plantel']
        try:
            return Plantel.objects.get(pk=data)
        except Plantel.DoesNotExist:
            raise forms.ValidationError("El plantel no existe")

    class Meta:
        model = OfertaEducativa
        fields = '__all__'


class PoblacionEstudiantilForm(forms.ModelForm):
    oferta_educativa = forms.IntegerField(widget=forms.HiddenInput())
    semestre = forms.IntegerField(widget=forms.HiddenInput(), initial=1)
    grupos = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control text-right', 'placeholder': 'Grupos', 'readonly': 'readonly'}), min_value=1)
    alumnos = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control text-right', 'placeholder': 'Alumnos'}), min_value=1)
    reprobados = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control text-right', 'placeholder': 'Repetidores'}), min_value=0)

    def clean_oferta_educativa(self):
        data = self.cleaned_data['oferta_educativa']
        try:
            return OfertaEducativa.objects.get(pk=data)
        except OfertaEducativa.DoesNotExist:
            raise forms.ValidationError("La oferta no existe")

    class Meta:
        model = PoblacionEstudiantil
        fields = '__all__'
