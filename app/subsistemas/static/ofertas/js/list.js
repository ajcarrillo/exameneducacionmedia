"use strict";
/**
 * Created by andres on 01/09/16.
 */

(function () {
	var $modalProyeccionForm        = $('#agregar-proyeccion-form'),
		$alumnosProyeccion          = $("[data-field='grupos_proyeccion_1']"),
		$gruposProyeccion           = $("[data-field='alumnos_proyeccion_1']"),
		$totalProyeccion            = $("[data-field='total_proyeccion_1']"),
		$btnActivate                = $("[data-button='activate']"),
		$btnDeactivate              = $("[data-button='deactivate']"),
		$btnDeleteOferta            = $("[data-button='delete-oferta']"),
		$btnOpenModalProyeccionForm = $("[data-button='open-modal-proyeccion-form']");

	$('[data-toggle="tooltip"]').tooltip();

	$btnActivate.on("click", onClickActivateEspecialidad);
	$btnDeactivate.on("click", onClickDeactivateEspecialidad);
	$btnDeleteOferta.on("click", onClickBtnDeleteOferta);
	$btnOpenModalProyeccionForm.on("click", onClickOpenProyeccionForm);

	$gruposProyeccion.on("keyup", onKeyUpGruposProyeccion);
	$alumnosProyeccion.on("keyup", onKeyUpAlumnosProyeccion);

	function onKeyUpGruposProyeccion() {
		$totalProyeccion.val($(this).val()*$alumnosProyeccion.val());
	}

	function onKeyUpAlumnosProyeccion() {
		$totalProyeccion.val($(this).val()*$gruposProyeccion.val());
	}

	function onClickOpenProyeccionForm() {
		/*$modalProyeccionForm.modal();*/
	}

	$modalProyeccionForm.on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget)
		var url   = button.data('url');
		var modal = $(this);
		modal.find('.modal-body form').attr('action', url);
	});

	function onClickDeactivateEspecialidad() {
		var $form = $(this).closest("form");
		bootbox.dialog({
			message: "¿Desea desactivar la oferta educativa?",
			title: "Desactivar",
			buttons: {
				danger: {
					label: "Si",
					className: "btn-success",
					callback: function () {
						var url  = $form.attr("action"),
							data = $form.serialize();
						$.post(url, data, function (response) {
							if (!response.is_valid) {
								bootbox.alert({
									message: response.error,
									title: "Advertencia!",
									buttons: {
										ok: {
											label: "Aceptar",
											className: "btn-default"
										}
									}
								});
							} else {
								location.reload()
							}
						});
					}
				},
				success: {
					label: "No",
					className: "btn-default",
					callback: function () {
						console.log("dont post")
					}
				}

			}
		});

	}

	function onClickActivateEspecialidad() {
		var $form = $(this).closest("form");
		bootbox.dialog({
			message: "¿Desea activar la oferta educativa?",
			title: "Activar",
			buttons: {
				danger: {
					label: "Si",
					className: "btn-success",
					callback: function () {
						var url  = $form.attr("action"),
							data = $form.serialize();
						$.post(url, data, function (response) {
							if (!response.is_valid) {
								bootbox.alert({
									message: response.error,
									title: "Advertencia!",
									buttons: {
										ok: {
											label: "Aceptar",
											className: "btn-default"
										}
									}
								});
							} else {
								location.reload()
							}
						});
					}
				},
				success: {
					label: "No",
					className: "btn-default",
					callback: function () {
						console.log("dont post")
					}
				}

			}
		});
	}

	function onClickBtnDeleteOferta() {
		var $formDeleteOferta = $(this).closest("form");
		bootbox.dialog({
			message: "¿Desea eliminar la especialidad? Esta acción no se puede deshacer",
			title: "Eliminar",
			buttons: {
				danger: {
					label: "Confirmar",
					className: "btn-danger",
					callback: function () {
						var url  = $formDeleteOferta.attr("action"),
							data = $formDeleteOferta.serialize();
						$.post(url, data, function (response) {
							if (!response.is_valid) {
								bootbox.alert({
									message: response.error,
									title: "Advertencia!",
									buttons: {
										ok: {
											label: "Aceptar",
											className: "btn-default"
										}
									}
								});
							} else {
								location.reload()
							}
						});
					}
				},
				success: {
					label: "Cancelar",
					className: "btn-default",
					callback: function () {
						console.log("dont post")
					}
				}

			}
		});

	}


}());
