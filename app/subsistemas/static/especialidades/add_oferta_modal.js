"use strict";
/**
 * Created by andres on 05/10/16.
 */

(function () {
	var $ofertaEducativaForm = $('#oferta-educativa-form'),
		$alumnos             = $ofertaEducativaForm.find("input[name='alumnos']"),
		$grupos              = $ofertaEducativaForm.find("input[name='grupos']"),
		$total               = $ofertaEducativaForm.find("#total");

	$alumnos.on("keyup", onKeyUpAlumnos);
	$grupos.on("keyup", onKeyUpGrupos);

	function onKeyUpAlumnos() {
		$total.val($(this).val() * $grupos.val());
	}

	function onKeyUpGrupos() {
		$total.val($(this).val() * $alumnos.val());
	}

}());
