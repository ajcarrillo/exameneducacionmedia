"use strict";
/**
 * Created by lewis on 01/09/16.
 */

(function () {
	var $btnDeleteAforo = $("[data-button='delete-aforo']");
	$btnDeleteAforo.on("click", onClickBtnDeleteAforo);

	function onClickBtnDeleteAforo() {
		var $formDeleteAforo = $(this).closest("form");
		bootbox.dialog({
			message: "¿Desea eliminar el aforo? Esta acción no se puede deshacer",
			title: "Eliminar",
			buttons: {
				danger: {
					label: "Confirmar",
					className: "btn-danger",
					callback: function () {
						var url = $formDeleteAforo.attr("action"),
							data = $formDeleteAforo.serialize();
						$.post(url, data, function (response) {
							if (!response.is_valid) {
								bootbox.alert({
									message: response.error,
									title: "Advertencia!",
									buttons: {
										ok: {
											label: "Aceptar",
											className: "btn-default"
										}
									}
								});
							} else {
								location.reload()
							}
						});
					}
				},
				success: {
					label: "Cancelar",
					className: "btn-default",
					callback: function () {
						console.log("dont post")
					}
				}

			}
		});

	}
}());
