"use strict";
/**
 * Created by andres on 11/09/16.
 */

(function () {
	var $btnShowRejection   = $("[data-button='show-rejection']"),
		$btnEnablePlantel   = $("[data-button='enabled']"),
		$btnDisabledPlantel = $("[data-button='disabled']");

	$btnShowRejection.on("click", onClickShowRejection);
	$btnEnablePlantel.on("click", onClickEnabledPlantel);
	$btnDisabledPlantel.on("click", onClickDisabledPlantel);

	function onClickEnabledPlantel() {
		var $form = $(this).closest("form"),
			url   = $form.attr('action'),
			data  = $form.serialize();
		bootbox.dialog({
			message: "¿Desea desactivar el plantel?",
			title: "Activar",
			buttons: {
				danger: {
					label: "Si",
					className: "btn-success",
					callback: function () {
						$.post(url, data, function (response) {
							if (!response.is_valid) {
								bootbox.alert({
									message: response.error,
									title: "Advertencia!",
									buttons: {
										ok: {
											label: "Aceptar",
											className: "btn-default"
										}
									}
								});
							} else {
								location.reload()
							}
						});
					}
				},
				success: {
					label: "No",
					className: "btn-default",
					callback: function () {
						console.log("dont post")
					}
				}

			}
		});
	}

	function onClickDisabledPlantel() {
		var $form = $(this).closest("form"),
			url   = $form.attr('action'),
			data  = $form.serialize();
		bootbox.dialog({
			message: "¿Desea desactivar el plantel?",
			title: "Desactivar",
			buttons: {
				danger: {
					label: "Si",
					className: "btn-success",
					callback: function () {
						$.post(url, data, function (response) {
							if (!response.is_valid) {
								bootbox.alert({
									message: response.error,
									title: "Advertencia!",
									buttons: {
										ok: {
											label: "Aceptar",
											className: "btn-default"
										}
									}
								});
							} else {
								location.reload()
							}
						});
					}
				},
				success: {
					label: "No",
					className: "btn-default",
					callback: function () {
						console.log("dont post")
					}
				}

			}
		});

	}

	function onClickShowRejection() {
		var url = $(this).data('url');

		$.getJSON(url, function (data) {
			var msg = data.comentario + "<br><br>" +
				"<strong>Revisó: </strong>" + data.usuario_revision + "<br>" +
				"<strong>Fecha de revision: </strong>" + data.fecha_revision + "<br><br>" +
				"<strong>Envió: </strong>" + data.usuario_apertura + "<br>" +
				"<strong>Fecha de envío: </strong>" + data.fecha_apertura + "<br>";

			bootbox.dialog({
				title: "Motivo de rechazo",
				message: msg,
				buttons: {
					success: {
						label: "Aceptar",
						className: "btn-success"
					}
				}
			});
		})
	}
}());
