from __future__ import unicode_literals

from django.apps import AppConfig


class SubsistemasConfig(AppConfig):
    name = 'app.subsistemas'
