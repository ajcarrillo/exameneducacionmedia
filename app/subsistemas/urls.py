# -*- encoding: utf-8 -*
from django.conf.urls import url

from app.subsistemas import views

# **************************************
#
#   url: /subsistemas/
#   namespace: subsistemas
#
# **************************************

urlpatterns = [
    url(r'^$', views.StartTemplateView.as_view(), name='subsistemas_start'),

    url(r'^aspirantes-con-pago-sin-pase-al-examen/$', views.AspirantesConPagoSinProcesoCompletoView.as_view(), name='aspirantes_con_pago_sin_pase_al_examen'),
    url(r'^aspirantes-sin-pago/$', views.AspirantesSinPagoEfectuado.as_view(), name='aspirantes_sin_pago'),

    url(r'^especialidades/$', views.EspecialidadListView.as_view(), name='especialidades_list'),
    url(r'^especialidades/nuevo$', views.EspecialidadCreateView.as_view(), name='especialidades_create'),

    url(r'^aforo/enviar/$', views.EnviarAforo.as_view(), name='aforo_submit'),
    url(r'^aforo/rechazo/$', views.RevisionAforoRechazoView.as_view(), name='aforo_reject'),
    url(r'^oferta/enviar/$', views.EnviarOfertaEducativa.as_view(), name='ofertas_submit'),
    url(r'^oferta/rechazo/$', views.RevisionOfertaEducativaView.as_view(), name='ofertas_reject'),

    url(r'^oferta/nueva/$', views.OfertaEducativaCreateView.as_view(), name='ofertas_create'),
    url(r'^oferta/ajuste/(?P<oferta_id>[\d]+)/$', views.PoblacionEstudiantilView.as_view(), name='ofertas_adjust'),
    url(r'^oferta/ajuste/(?P<oferta_id>[\d]+)/delete/$', views.PoblacionEstudiantilDeleteView.as_view(), name='poblacion_delete'),

    url(r'^planteles/$', views.PlantelListView.as_view(), name='planteles_list'),
    url(r'^planteles/nuevo/$', views.PlantelCreateView.as_view(), name='planteles_create'),
    url(r'^planteles/activar/$', views.ActivatePlantelView.as_view(), name='planteles_activate'),
    url(r'^planteles/desactivar/$', views.DeactivatePlantelView.as_view(), name='planteles_deactivate'),
    url(r'^planteles/editar/(?P<pk>[\d]+)/$', views.PlantelUpdateView.as_view(), name='planteles_edit'),

    url(r'^planteles/(?P<plantel_id>[\d]+)/oferta/$', views.OfertaEducativaListView.as_view(), name='ofertas'),
    url(r'^planteles/(?P<plantel_id>[\d]+)/oferta/eliminar/$', views.OfertaEducativaDeleteView.as_view(), name='ofertas_delete'),
    url(r'^planteles/(?P<plantel_id>[\d]+)/oferta/activar/$', views.ActivateOfertaEducativa.as_view(), name='ofertas_activate'),
    url(r'^planteles/(?P<plantel_id>[\d]+)/oferta/desactivar/$', views.DeactivateOfertaEducativa.as_view(), name='ofertas_deactivate'),
    url(r'^planteles/(?P<plantel_id>[\d]+)/oferta/(?P<oferta_id>[\d]+)/proyeccion$', views.OfertaEducativaProyeccion.as_view(),
        name='ofertas_proyeccion'),

    url(r'^planteles/(?P<plantel_id>[\d]+)/aforo/$', views.AforoListView.as_view(), name='aforos_list'),
    url(r'^planteles/(?P<plantel_id>[\d]+)/aforo/eliminar/$', views.AforoDeleteView.as_view(), name='aforos_delete'),
]
