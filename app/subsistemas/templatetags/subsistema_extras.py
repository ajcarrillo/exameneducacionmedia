# -*- encoding: utf-8 -*
import datetime
from django import template
from django.urls import reverse
from django.utils.html import format_html

from app.administracion.models import RevisionAforo, RevisionOfertaEducativa, Revision

register = template.Library()


@register.simple_tag(name='get_ciclo_escolar')
def get_ciclo_escolar():
    now = datetime.datetime.now()
    return "{0} - {1}".format(now.year, now.year + 1)


@register.simple_tag(takes_context=True)
def revision_submit_button(context):
    subsistema = context.get('subsistema')
    if RevisionOfertaEducativa.objects.filter(subsistema_id=subsistema.pk, estado=Revision.ESTADO_EN_REVISION).exists():
        return format_html("<a href='#' class='btn btn-link'>La oferta educativa está en revisión</a>")

    if RevisionOfertaEducativa.objects.filter(subsistema_id=subsistema.pk, estado=Revision.ESTADO_ACEPTADO).exists():
        return format_html("<a href='#' class='btn btn-link'><i class='fa fa-check'></i> La oferta educativa ha sido aceptada</a>")

    if RevisionOfertaEducativa.objects.filter(subsistema_id=subsistema.pk, estado=Revision.ESTADO_CANCELADO).exists():
        return format_html("<a href='{}' class='btn btn-link'><i class='fa fa-share-square' aria-hidden='true'></i> Enviar oferta educativa</a> "
                           "<button style='color:red' class='btn btn-link' data-button='show-rejection' data-url='{}'>"
                           "<i class='fa fa-eye' aria-hidden='true'></i> "
                           " Ver motivo de rechazo</button>",
                           reverse("subsistemas:ofertas_submit"), reverse("subsistemas:ofertas_reject"))

    return format_html("<a href='{}' class='btn btn-link'><i class='fa fa-share-square' aria-hidden='true'></i> Enviar oferta educativa</a>",
                       reverse("subsistemas:ofertas_submit"))


@register.simple_tag(takes_context=True)
def revision_aforo_submit_button(context):
    subsistema = context.get('subsistema')
    if RevisionAforo.objects.filter(subsistema_id=subsistema.pk, estado=Revision.ESTADO_EN_REVISION).exists():
        return format_html("<a href='#' class='btn btn-link'>El aforo está en revisión</a>")

    if RevisionAforo.objects.filter(subsistema_id=subsistema.pk, estado=Revision.ESTADO_ACEPTADO).exists():
        return format_html("<a href='#' class='btn btn-link'><i class='fa fa-check'></i>El aforo ha sido aceptado</a>")

    if RevisionAforo.objects.filter(subsistema_id=subsistema.pk, estado=Revision.ESTADO_CANCELADO).exists():
        return format_html("<a href='{}' class='btn btn-link'><i class='fa fa-share-square' aria-hidden='true'></i> Enviar aforo</a>"
                           "<button style='color:red' class='btn btn-link' data-button='show-rejection' data-url='{}'>"
                           "<i class='fa fa-eye' aria-hidden='true'></i> "
                           " Ver motivo de rechazo</button>",
                           reverse("subsistemas:aforo_submit"), reverse("subsistemas:aforo_reject"))

    return format_html("<a href='{}' class='btn btn-link'><i class='fa fa-share-square' aria-hidden='true'></i> Enviar aforo</a>",
                       reverse("subsistemas:aforo_submit"))
