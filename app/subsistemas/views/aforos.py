# -*- encoding: utf-8 -*
from django.contrib import messages
from django.db import transaction
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.urls import reverse
from django.views import generic

from app.planteles.models import Aula, AulaPlantel, Plantel
from app.subsistemas.forms import AulaPlantelForm
from general.mixins import LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin


class AforoListView(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, generic.ListView):
    model = AulaPlantel
    template_name = "aforos/list.html"
    context_object_name = "aforos"

    def get_queryset(self):
        queryset = self.model.objects.select_related('aula', 'plantel').filter(plantel__id=self.kwargs['plantel_id'])
        return queryset

    def get_context_data(self, **kwargs):
        context = super(AforoListView, self).get_context_data(**kwargs)
        context.update({
            'plantel': Plantel.objects.select_related('subsistema').get(pk=self.kwargs['plantel_id']),
            'form': AulaPlantelForm(auto_id='%s')
        })
        return context

    def post(self, request, plantel_id):
        plantel = Plantel.objects.get(pk=plantel_id)
        transaction.set_autocommit(False)
        form = AulaPlantelForm(request.POST, auto_id='%s')
        try:
            if form.is_valid():
                self.tiene_revision_aforo_pendiente(request.user)
                aula = AulaPlantel()
                aula.referencia = form.cleaned_data['referencia']
                aula.capacidad = form.cleaned_data['capacidad']
                aula.plantel = plantel
                aula.save()

                transaction.commit()
                return HttpResponseRedirect(reverse('subsistemas:aforos_list', kwargs={'plantel_id': plantel.id}))
        except Exception as e:
            transaction.rollback()
            messages.info(request, e.message)
            return HttpResponseRedirect(reverse('subsistemas:aforos_list', kwargs={'plantel_id': plantel.id}))


class AforoCreateView(generic.ListView):
    pass


class AforoDeleteView(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, generic.DeleteView):
    def post(self, request, plantel_id):
        transaction.set_autocommit(False)
        try:
            subsistema = self.subsistema_asignado(request.user)
            self.tiene_revision_aforo_pendiente(request.user)
            plantel = Plantel.objects.get(pk=plantel_id)
            aula = Aula.objects.get(pk=request.POST['aula_id'])

            if plantel.subsistema != subsistema:
                raise Exception('No tiene los permisos para hacer este movimiento')

            aula_plantel = AulaPlantel.objects.get(plantel=plantel, aula=aula)
            aula_plantel.delete()

            if not AulaPlantel.objects.filter(aula=aula).exists():
                aula.delete()
            transaction.commit()
            return JsonResponse({'is_valid': True}, safe=False)
        except:
            transaction.rollback()
            raise
