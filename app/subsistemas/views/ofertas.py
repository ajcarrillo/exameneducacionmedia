# -*- encoding: utf-8 -*
from django.contrib import messages
from django.db import transaction
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.views import generic

from app.administracion.models import EtapaProceso
from app.planteles.models import OfertaEducativa, Especialidad, Subsistema, Plantel, PoblacionEstudiantilProyeccion, PoblacionEstudiantil
from app.subsistemas.forms import OfertaEducativaForm, OfertaEducativaModelForm, PoblacionEstudiantilForm
from general.mixins import LoginRequiredMixin, AsegurarEtapaMixin
from general.mixins import NeedsSubsistemaGroupAccessMixin


class OfertaEducativaListView(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, generic.ListView):
    template_name = 'ofertas/list.html'
    model = OfertaEducativa
    context_object_name = 'oferta_educativa'

    def get_queryset(self):
        queryset = self.model.objects.select_related('especialidad').filter(plantel=self.kwargs['plantel_id'])
        return queryset

    def get_context_data(self, **kwargs):
        context = super(OfertaEducativaListView, self).get_context_data(**kwargs)
        plantel = Plantel.objects.select_related('subsistema').get(pk=self.kwargs['plantel_id'])
        form = OfertaEducativaModelForm(auto_id='%s', initial={'plantel': plantel.id}, subsistema=plantel.subsistema)

        context.update({
            'form':             form,
            'plantel':          plantel,
            'is_ajuste_oferta': EtapaProceso.is_etapa("AJUSTE DE OFERTA")
        })

        return context

    def post(self, request, plantel_id=None):
        transaction.set_autocommit(False)
        subsistema = Subsistema.objects.get(responsable=self.request.user)
        try:
            plantel = Plantel.objects.get(pk=plantel_id)
            self.tiene_revision_pendiente(request.user)
            if plantel.subsistema != subsistema:
                raise Exception('No tienes los permisos para realizar esta acción')
            form = OfertaEducativaForm(request.POST, auto_id='%s')

            if not form.is_valid():
                raise Exception('Error al guarda los datos')

            try:
                especialidad = Especialidad.objects.get(referencia=form.cleaned_data['referencia'],
                                                        subsistema=subsistema)
            except Especialidad.DoesNotExist:
                especialidad = Especialidad()
                especialidad.referencia = form.cleaned_data['referencia']
                especialidad.descripcion = form.cleaned_data['descripcion']
                especialidad.subsistema = subsistema
                especialidad.save()

            if OfertaEducativa.objects.filter(plantel=plantel, especialidad=especialidad).exists():
                raise Exception('¡La especialidad {0} ya existe para este plantel!'.format(especialidad.referencia))

            oferta = OfertaEducativa()
            oferta.plantel = plantel
            oferta.especialidad = especialidad
            oferta.activa = True
            oferta.clave_especialidad = form.cleaned_data['clave_especialidad']
            oferta.programa_estudio = form.cleaned_data['programa_estudio']

            oferta.save()

            poblacion_estudiantil = PoblacionEstudiantilProyeccion()
            poblacion_estudiantil.oferta_educativa = oferta
            poblacion_estudiantil.grupos = form.cleaned_data['grupos0']
            poblacion_estudiantil.alumnos = form.cleaned_data['alumnos0']
            poblacion_estudiantil.semestre = 1

            poblacion_estudiantil.save()

            transaction.commit()

            return HttpResponseRedirect(reverse('subsistemas:ofertas', kwargs={'plantel_id': plantel_id}))
        except Exception as e:
            transaction.rollback()
            raise e


class OfertaEducativaCreateView(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, AsegurarEtapaMixin, generic.View):
    asegurar_etapa_de = "OFERTA"

    def post(self, request):
        subsistema = self.subsistema_asignado(request.user)
        form = OfertaEducativaModelForm(request.POST, auto_id='%s', subsistema=subsistema)
        try:
            transaction.set_autocommit(False)
            if OfertaEducativa.objects.filter(plantel__id=request.POST.get('plantel'), especialidad__id=request.POST.get('especialidad')).exists():
                raise Exception('¡La especialidad ya existe para este plantel!')
            self.tiene_revision_pendiente(request.user)
            if form.is_valid():
                oferta = form.save()
                poblacion_estudiantil = PoblacionEstudiantilProyeccion()
                poblacion_estudiantil.oferta_educativa = oferta
                poblacion_estudiantil.grupos = request.POST.get('grupos')
                poblacion_estudiantil.alumnos = request.POST.get('alumnos')
                poblacion_estudiantil.semestre = 1

                poblacion_estudiantil.save()

                transaction.commit()
            else:
                print (form.errors)
                for key, value in form.errors.items():
                    messages.add_message(request, messages.INFO, value)
                raise Exception('Error al guarda los datos')
        except Exception as e:
            transaction.rollback()
            messages.add_message(request, messages.INFO, e.message)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class OfertaEducativaDeleteView(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, generic.View):
    def post(self, request, plantel_id):
        transaction.set_autocommit(False)
        try:
            subsistema = self.subsistema_asignado(request.user)
            self.tiene_revision_pendiente(request.user)
            # self.etapa().asegurar_envio(subsistema)
            plantel = Plantel.objects.get(pk=plantel_id)
            especialidad = Especialidad.objects.get(pk=request.POST['especialidad_id'])
            if plantel.subsistema != subsistema or especialidad.subsistema != subsistema:
                raise Exception('No tiene los permisos para hacer este movimiento')
            oferta_educativa = OfertaEducativa.objects.get(plantel=plantel, especialidad=especialidad)
            PoblacionEstudiantilProyeccion.objects.filter(oferta_educativa=oferta_educativa).delete()
            oferta_educativa.delete()
            if not OfertaEducativa.objects.filter(especialidad=especialidad).exists():
                especialidad.delete()
            transaction.commit()
            return JsonResponse({'is_valid': True}, safe=False)
        except:
            transaction.rollback()
            raise


class ActivateOfertaEducativa(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, generic.View):
    def post(self, request, plantel_id):
        try:
            subsistema = self.subsistema_asignado(request.user)
            self.tiene_revision_pendiente(request.user)
            plantel = Plantel.objects.get(pk=plantel_id)
            especialidad = Especialidad.objects.get(pk=request.POST['especialidad_id'])
            if plantel.subsistema != subsistema or especialidad.subsistema != subsistema:
                raise Exception('No tiene los permisos para hacer este movimiento')
            oferta_educativa = OfertaEducativa.objects.get(plantel=plantel, especialidad=especialidad)
            oferta_educativa.activa = True
            oferta_educativa.save()
            return JsonResponse({'is_valid': True}, safe=False)
        except:
            raise


class DeactivateOfertaEducativa(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, generic.View):
    def post(self, request, plantel_id):
        try:
            subsistema = self.subsistema_asignado(request.user)
            self.tiene_revision_pendiente(request.user)
            plantel = Plantel.objects.get(pk=plantel_id)
            especialidad = Especialidad.objects.get(pk=request.POST['especialidad_id'])
            if plantel.subsistema != subsistema or especialidad.subsistema != subsistema:
                raise Exception('No tiene los permisos para hacer este movimiento')
            oferta_educativa = OfertaEducativa.objects.get(plantel=plantel, especialidad=especialidad)
            oferta_educativa.activa = False
            oferta_educativa.save()
            return JsonResponse({'is_valid': True}, safe=False)
        except:
            raise


class OfertaEducativaProyeccion(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, AsegurarEtapaMixin, generic.View):
    asegurar_etapa_de = "OFERTA"

    def post(self, request, plantel_id, oferta_id):
        try:
            oferta_educativa = OfertaEducativa.objects.get(pk=oferta_id)

            if oferta_educativa.poblacion_estudiantil_proyeccion_related.exists():
                proyeccion = oferta_educativa.poblacion_estudiantil_proyeccion_related.first()
            else:
                proyeccion = PoblacionEstudiantilProyeccion()
                proyeccion.oferta_educativa_id = oferta_id
                proyeccion.semestre = 1
            proyeccion.grupos = request.POST.get('grupos')
            proyeccion.alumnos = request.POST.get('alumnos')
            proyeccion.save()
        except Exception as e:
            messages.add_message(request, messages.ERROR, e.message)

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class PoblacionEstudiantilView(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, AsegurarEtapaMixin, generic.View):
    asegurar_etapa_de = "AJUSTE DE OFERTA"
    form_class = PoblacionEstudiantilForm
    template_name = 'ofertas/ajuste_oferta.html'
    subsistema = None

    def get_context_data(self, oferta_id):
        oferta_educativa = OfertaEducativa.objects.select_related('plantel', 'plantel__subsistema').get(pk=oferta_id)

        if self.subsistema != oferta_educativa.plantel.subsistema:
            raise Exception("No tiene permisos para realizar esta operación")

        context = {
            'plantel':          oferta_educativa.plantel,
            'subsistema':       self.subsistema_asignado(self.request.user),
            'oferta_educativa': oferta_educativa,
            'grupos':           oferta_educativa.poblacion_estudiantil_related.all()
        }

        return context

    def dispatch(self, request, *args, **kwargs):
        self.subsistema = self.subsistema_asignado(request.user)
        return super(PoblacionEstudiantilView, self).dispatch(request, *args, **kwargs)

    def render(self, request, oferta_id, form):
        context = self.get_context_data(oferta_id)
        context.update({'form': form})
        return render(request, self.template_name, context)

    def get(self, request, oferta_id):
        form = self.form_class(auto_id='%s', initial={'oferta_educativa': oferta_id, 'grupos': 1})
        return self.render(request, oferta_id, form)

    def post(self, request, oferta_id):
        form = self.form_class(request.POST, auto_id='%s')

        if not form.is_valid():
            return self.render(request, oferta_id, form)

        poblacion_estudiantil = PoblacionEstudiantil()
        poblacion_estudiantil.oferta_educativa = form.cleaned_data['oferta_educativa']
        poblacion_estudiantil.semestre = form.cleaned_data['semestre']
        poblacion_estudiantil.grupos = form.cleaned_data['grupos']
        poblacion_estudiantil.alumnos = form.cleaned_data['alumnos']
        poblacion_estudiantil.reprobados = form.cleaned_data['reprobados']

        poblacion_estudiantil.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class PoblacionEstudiantilDeleteView(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, AsegurarEtapaMixin, generic.View):
    asegurar_etapa_de = 'AJUSTE DE OFERTA'

    def post(self, request, oferta_id):
        grupo = PoblacionEstudiantil.objects.get(pk=oferta_id)
        grupo.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
