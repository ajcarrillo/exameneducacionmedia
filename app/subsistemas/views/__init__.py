# -*- encoding: utf-8 -*
from .aforos import *
from .aspirantes import *
from .especialidades import *
from .ofertas import *
from .planteles import *
from .subsistemas import *
