# -*- encoding: utf-8 -*
import six
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.views import generic

from app.planteles.models import Especialidad, Plantel
from app.subsistemas.forms import OfertaEducativaModelForm
from general.mixins import LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, AsegurarEtapaMixin


class EspecialidadListView(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, generic.ListView):
    model = Especialidad
    template_name = "especialidades/list.html"
    context_object_name = "especialidades"
    subsistema = None

    def get_queryset(self):
        subsistema = self.subsistema_asignado(self.request.user)
        return self.model.objects.filter(subsistema=subsistema)

    def get_context_data(self, **kwargs):
        context = super(EspecialidadListView, self).get_context_data(**kwargs)
        subsistema = self.subsistema_asignado(self.request.user)
        oferta_form = OfertaEducativaModelForm(auto_id='%s', subsistema=subsistema)
        context.update({
            'subsistema':  self.subsistema,
            'oferta_form': oferta_form
        })
        return context


class EspecialidadCreateView(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, AsegurarEtapaMixin, generic.View):
    asegurar_etapa_de = "OFERTA"
    subsistema = None

    def dispatch(self, request, *args, **kwargs):
        self.subsistema = self.subsistema_asignado(request.user)
        return super(EspecialidadCreateView, self).dispatch(request, *args, **kwargs)

    def post(self, request):
        referencia = request.POST.get('referencia')
        descripcion = request.POST.get('descripcion')

        try:
            if not Especialidad.objects.filter(referencia=referencia, subsistema=self.subsistema).exists():
                especialidad = Especialidad()
                especialidad.referencia = referencia
                especialidad.descripcion = descripcion
                especialidad.subsistema = self.subsistema
                especialidad.save()
        except Exception as e:
            messages.add_message(request, messages.INFO, e.message)

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
