# -*- encoding: utf-8 -*
import datetime

from django.db import transaction
from django.db.models import F
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.views import generic

from app.administracion.models import RevisionAforo, RevisionOfertaEducativa, Revision
from app.planteles.models import OfertaEducativa, Plantel, Subsistema
from general.mixins import LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, AsegurarEtapaMixin, GroupRequiredMixin, SubsistemaUtils


class StartTemplateView(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, generic.TemplateView):
    template_name = 'subsistemas/start.html'

    def get_context_data(self, **kwargs):
        context = super(StartTemplateView, self).get_context_data(**kwargs)
        subsistema = self.subsistema_asignado(self.request.user)
        especialidades = OfertaEducativa.objects.filter(plantel__subsistema=subsistema, activa=True).count()
        context.update({
            'planteles':                                subsistema.planteles_related.select_related('domicilio', 'domicilio__localidad', 'domicilio__localidad__municipio').all(),
            'subsistema':                               subsistema,
            'especialidades':                           especialidades,
            'aspirantes_sin_pago_efectuado':            Subsistema.aspirantes_sin_pago_efectuado(subsistema.list_of_aspirantes_sin_pago_efectuado()),
            'aspirantes_con_pago_sin_proceso_completo': Subsistema.aspirantes_sin_pago_efectuado(subsistema.list_of_aspirantes_con_pago_sin_proceso_completo())
        })

        return context


class EnviarOfertaEducativa(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, AsegurarEtapaMixin, generic.View):
    asegurar_etapa_de = "OFERTA"

    def get(self, request):
        try:
            subsistema = self.subsistema_asignado(request.user)
            subsistema.asegurar_telefono_pagina()
            subsistema.asegurar_oferta_completa()
            revision = RevisionOfertaEducativa()
            revision.usuario_apertura = request.user
            revision.fecha_apertura = datetime.datetime.today()
            revision.subsistema = subsistema
            revision.estado = Revision.ESTADO_EN_REVISION
            revision.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        except:
            raise


class RevisionOfertaEducativaView(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, generic.View):
    def get(self, request):
        subsistema = self.subsistema_asignado(request.user)
        revision = RevisionOfertaEducativa.objects.filter(subsistema=subsistema, estado=Revision.ESTADO_CANCELADO).order_by('-id')[0]
        data = {
            'fecha_apertura':   revision.fecha_apertura,
            'estado':           revision.estado,
            'comentario':       revision.comentario,
            'usuario_revision': revision.usuario_revision.get_full_name(),
            'usuario_apertura': revision.usuario_apertura.get_full_name(),
            'fecha_revision':   revision.fecha_revision,
        }
        return JsonResponse(data, safe=False)


class EnviarAforo(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, generic.View):
    def get(self, request):
        try:
            subsistema = self.subsistema_asignado(request.user)
            subsistema.asegurar_telefono_pagina()
            subsistema.asegurar_aforo_completo()
            revision = RevisionAforo()
            revision.usuario_apertura = request.user
            revision.fecha_apertura = datetime.datetime.today()
            revision.subsistema = subsistema
            revision.estado = Revision.ESTADO_EN_REVISION
            revision.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        except:
            raise


class RevisionAforoRechazoView(LoginRequiredMixin, NeedsSubsistemaGroupAccessMixin, generic.View):
    def get(self, request):
        subsistema = self.subsistema_asignado(request.user)
        revision = RevisionAforo.objects.filter(subsistema=subsistema, estado=Revision.ESTADO_CANCELADO).order_by('-id')[0]
        data = {
            'fecha_apertura':   revision.fecha_apertura,
            'estado':           revision.estado,
            'comentario':       revision.comentario,
            'usuario_revision': revision.usuario_revision.get_full_name(),
            'usuario_apertura': revision.usuario_apertura.get_full_name(),
            'fecha_revision':   revision.fecha_revision,
        }
        return JsonResponse(data, safe=False)


class ActivatePlantelView(LoginRequiredMixin, GroupRequiredMixin, SubsistemaUtils, generic.View):
    group_required = 'subsistema'

    def post(self, request):
        transaction.set_autocommit(False)
        try:
            plantel_id = request.POST['plantel_id']
            subsistema = self.subsistema_asignado(request.user)
            self.tiene_revision_pendiente(request.user)
            plantel = Plantel.objects.prefetch_related('oferta_eduacativa_related').get(pk=plantel_id)
            if plantel.subsistema != subsistema:
                raise Exception('No tiene los permisos para hacer este movimiento')
            plantel.oferta_eduacativa_related.update(activa=True)
            plantel.active = True
            plantel.save()
            transaction.commit()
            return JsonResponse({'is_valid': True}, safe=False)
        except:
            transaction.rollback()
            raise


class DeactivatePlantelView(LoginRequiredMixin, GroupRequiredMixin, SubsistemaUtils, generic.View):
    group_required = 'subsistema'

    def post(self, request):
        transaction.set_autocommit(False)
        try:
            plantel_id = request.POST['plantel_id']
            subsistema = self.subsistema_asignado(request.user)
            self.tiene_revision_pendiente(request.user)
            plantel = Plantel.objects.prefetch_related('oferta_eduacativa_related').get(pk=plantel_id)
            if plantel.subsistema != subsistema:
                raise Exception('No tiene los permisos para hacer este movimiento')
            plantel.oferta_eduacativa_related.update(activa=False)
            plantel.active = False
            plantel.save()
            transaction.commit()
            return JsonResponse({'is_valid': True}, safe=False)
        except:
            transaction.rollback()
            raise
