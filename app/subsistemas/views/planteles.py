# -*- encoding: utf-8 -*
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views import generic

from app.subsistemas.forms import PlantelForm, PlantelFormUpdate
from app.planteles.models import Plantel
from app.mgeo.models import Domicilio
from django.contrib.auth.models import User, Group

from general.mixins import LoginRequiredMixin, GroupRequiredMixin, SubsistemaUtils


class PlantelView(SubsistemaUtils, generic.View):
    model = Plantel
    form_class = PlantelForm

    def process_form(self, domicilio, form, plantel):
        try:
            domicilio.localidad = form.cleaned_data['localidad']
            domicilio.colonia = form.cleaned_data['colonia']
            domicilio.calle = form.cleaned_data['calle']
            domicilio.numero = form.cleaned_data['numero']
            domicilio.codigo_postal = form.cleaned_data['codigo_postal']
            domicilio.save()

            username = form.cleaned_data['username']

            if User.objects.filter(username=username).exists():
                raise Exception('El nombre de usuario ya existe')

            password = form.cleaned_data['password']
            email = form.cleaned_data['email']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']

            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name
            user.save()

            plantel.referencia = form.cleaned_data['referencia']
            plantel.cct = form.cleaned_data['cct']
            plantel.telefono = form.cleaned_data['telefono']
            plantel.pagina_web = form.cleaned_data['pagina_web']

            plantel.domicilio = domicilio
            plantel.responsable = user
            plantel.subsistema = self.subsistema_asignado(self.request.user)

            plantel.save()

            user.groups.clear()
            group = Group.objects.get(name='plantel')
            user.groups.add(group)
        except Exception as e:
            raise Exception(e)

    def process_update_form(self, domicilio, form, plantel):
        try:
            domicilio.localidad = form.cleaned_data['localidad']
            domicilio.colonia = form.cleaned_data['colonia']
            domicilio.calle = form.cleaned_data['calle']
            domicilio.numero = form.cleaned_data['numero']
            domicilio.codigo_postal = form.cleaned_data['codigo_postal']
            domicilio.save()

            plantel.referencia = form.cleaned_data['referencia']
            plantel.cct = form.cleaned_data['cct']
            plantel.telefono = form.cleaned_data['telefono']
            plantel.pagina_web = form.cleaned_data['pagina_web']

            plantel.domicilio = domicilio
            plantel.subsistema = self.subsistema_asignado(self.request.user)
            plantel.save()

        except Exception as e:
            raise Exception("Ocurrió un problema al guardar el plantel")


class PlantelListView(LoginRequiredMixin, GroupRequiredMixin, generic.ListView):
    model = Plantel
    template_name = "planteles/index.html"
    context_object_name = 'planteles'
    group_required = 'subsistema'

    def get_queryset(self):
        return self.model.objects.select_related("responsable", "subsistema").all()


class PlantelCreateView(LoginRequiredMixin, GroupRequiredMixin, PlantelView):
    template_name = 'planteles/create.html'
    group_required = 'subsistema'

    def get(self, request):
        context = {
            'form': self.form_class(auto_id='%s')
        }
        return render(request, self.template_name, context)

    def post(self, request):
        form = self.form_class(request.POST, auto_id='%s')
        transaction.set_autocommit(False)
        try:
            if form.is_valid():
                plantel = Plantel()
                domicilio = Domicilio()

                self.process_form(domicilio, form, plantel)

                transaction.commit()
                return HttpResponseRedirect(reverse("subsistemas:subsistemas_start"))
        except Exception as e:
            transaction.rollback()
            raise


class PlantelUpdateView(LoginRequiredMixin, GroupRequiredMixin, PlantelView):
    template_name = "planteles/edit.html"
    group_required = 'subsistema'

    def get_queryset(self, pk):
        return self.model.objects.select_related('domicilio', 'responsable').get(pk=pk)

    def get(self, request, pk=None):
        plantel = self.get_queryset(pk)

        form = PlantelFormUpdate(auto_id='%s', initial={
            'referencia': plantel.referencia,
            'cct': plantel.cct,
            'telefono': plantel.telefono,
            'pagina_web': plantel.pagina_web,

            'municipio': plantel.domicilio.localidad.municipio,
            'localidad': plantel.domicilio.localidad,
            'colonia': plantel.domicilio.colonia,
            'calle': plantel.domicilio.calle,
            'numero': plantel.domicilio.numero,
            'codigo_postal': plantel.domicilio.codigo_postal,
            'first_name': plantel.responsable.first_name,
            'last_name': plantel.responsable.last_name,
            'email': plantel.responsable.email,
            'username': plantel.responsable.username,
            'password': plantel.responsable.password,
        })
        context = {'form': form, 'plantel': plantel}

        return render(request, self.template_name, context)

    def post(self, request, pk=None):
        form = PlantelFormUpdate(request.POST, auto_id='%s')
        plantel = self.get_queryset(pk)

        transaction.set_autocommit(False)
        try:
            if form.is_valid():
                domicilio = plantel.domicilio

                self.process_update_form(domicilio, form, plantel)

                transaction.commit()
                return HttpResponseRedirect(reverse("subsistemas:subsistemas_start"))
        except Exception as e:
            transaction.rollback()
            raise
