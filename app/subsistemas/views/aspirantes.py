# -*- encoding: utf-8 -*
from django.shortcuts import render
from django.views import generic

from general.mixins import LoginRequiredMixin, GroupRequiredMixin, SubsistemaUtils


class AspirantesConPagoSinProcesoCompletoView(LoginRequiredMixin, GroupRequiredMixin, SubsistemaUtils, generic.View):
    group_required = 'subsistema'
    template_name = 'aspirantes/lista_aspirantes_con_pago_sin_pase_al_examen.html'

    def get(self, request):
        subsistema = self.subsistema_asignado(request.user)
        context = {
            'aspirantes': subsistema.list_of_aspirantes_con_pago_sin_proceso_completo()
        }
        return render(request, self.template_name, context)


class AspirantesSinPagoEfectuado(LoginRequiredMixin, GroupRequiredMixin, SubsistemaUtils, generic.View):
    group_required = 'subsistema'
    template_name = 'aspirantes/lista_aspirantes_sin_pago.html'

    def get(self, request):
        subsistema = self.subsistema_asignado(request.user)
        context = {
            'aspirantes': subsistema.list_of_aspirantes_sin_pago_efectuado()
        }
        return render(request, self.template_name, context)
