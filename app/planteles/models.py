# -*- encoding: utf-8 -*
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.db.models import Sum
from django.urls import reverse

from app.mgeo.models import Domicilio
from app.aspirantes.models import PaseExamen, Aspirante
from general.magicnumbers import NIVELES_DEMANDA, NIVEL_DEMANDA_DEFAULT


class Aula(models.Model):
    id = models.AutoField(db_column='id', primary_key=True)
    referencia = models.CharField(db_column='referencia', max_length=50)
    capacidad = models.IntegerField(db_column='capacidad')

    class Meta:
        db_table = 'aula'


class Subsistema(models.Model):
    id = models.AutoField(db_column='id', primary_key=True)
    referencia = models.CharField(db_column='referencia', max_length=30, unique=True)
    descripcion = models.CharField(db_column='descripcion', max_length=255)
    responsable = models.OneToOneField(User, db_column='responsable', null=True, on_delete=models.DO_NOTHING,
                                       related_name='responsable_subsistema_related')

    class Meta:
        db_table = 'subsistema'
        verbose_name = 'Subsistema educativo'
        verbose_name_plural = 'Subsistemas educativos'

    def __unicode__(self):
        return '%s' % self.referencia

    def asegurar_oferta_completa(self):
        incompletos = []
        for plantel in self.planteles_related.all().filter(active=True):
            ofertas = OfertaEducativa.objects.filter(plantel=plantel, activa=True)
            if not ofertas.exists():
                incompletos.append(plantel.referencia)
            else:
                for oferta in ofertas:
                    if not oferta.poblacion_estudiantil_proyeccion_related.exists():
                        incompletos.append(plantel.referencia)

        if len(incompletos) > 0:
            raise Exception("No se ha terminado la caputura de la oferta educativa de los siguientes planteles: {0}".format(', '.join(incompletos)))

    def asegurar_telefono_pagina(self):
        incompletos = []
        planteles = self.planteles_related.all().filter(active=True).filter(telefono__isnull=True, pagina_web__isnull=True)
        if planteles.exists():
            for plantel in planteles:
                incompletos.append(plantel.referencia)

        if len(incompletos) > 0:
            raise Exception(
                "No se ha terminado la caputura de teléfono y página web de los siguientes planteles: {0}".format(
                    ', '.join(incompletos)))

    def asegurar_aforo_completo(self):
        incompletos = []
        for plantel in self.planteles_related.all().filter(active=True):
            if not AulaPlantel.objects.filter(plantel=plantel).exists():
                incompletos.append(plantel.referencia)
        if len(incompletos) > 0:
            raise Exception(
                "No se ha terminado la captura del aforo de los siguientes planteles: {0}".format(
                    ', '.join(incompletos)))

    def get_absolute_url(self):
        return reverse('administracion:subsistemas_update', kwargs={'pk': self.pk})

    def demanda(self):
        try:
            subsistema = Subsistema.objects.raw('SELECT sub.id, count(*) as aspirantes '
                                                'FROM seleccion_oferta_educativa soe '
                                                'INNER JOIN plantel_especialidad pe on soe.plantel_especialidad = pe.id '
                                                'INNER JOIN plantel p on pe.plantel=p.id '
                                                'INNER JOIN subsistema sub ON p.subsistema = sub.id '
                                                'WHERE soe.preferencia = 1 and sub.id = %s '
                                                'GROUP BY sub.id', [self.id])[0]
            return subsistema.aspirantes
        except Exception as e:
            print (e.message)
            return 0

    def aspirantes_con_proceso_completo(self):
        try:
            subsistema = Subsistema.objects.raw('SELECT s.id, count(*) as aspirantes '
                                                'FROM aspirante a '
                                                'INNER JOIN auth_user au on a.usuario = au.id '
                                                'INNER JOIN pase_examen pe on a.id = pe.aspirante '
                                                'INNER JOIN seleccion_oferta_educativa soe on a.id = soe.aspirante and soe.preferencia = 1 '
                                                'INNER JOIN plantel_especialidad pes on soe.plantel_especialidad = pes.id '
                                                'INNER JOIN plantel p on pes.plantel = p.id '
                                                'INNER JOIN subsistema s on p.subsistema = s.id and s.id = %s '
                                                'GROUP BY s.id ', [self.id])[0]
            return subsistema.aspirantes
        except Exception as e:
            print (e.message)
            return 0

    @staticmethod
    def aspirantes_con_pago_sin_proceso_completo(lista):
        return len(list(lista))

    def list_of_aspirantes_con_pago_sin_proceso_completo(self):
        try:
            return Aspirante.objects.raw('SELECT a.id, au.first_name, au.last_name, a.telefono, au.email, p.referencia '
                                         'FROM aspirante a '
                                         'INNER JOIN auth_user au ON a.usuario = au.id '
                                         'INNER JOIN revision_registro rr on a.id = rr.aspirante and rr.efectuado = 1 '
                                         'INNER JOIN seleccion_oferta_educativa soe on a.id = soe.aspirante and soe.preferencia = 1 '
                                         'INNER JOIN plantel_especialidad pes on soe.plantel_especialidad = pes.id '
                                         'INNER JOIN plantel p on pes.plantel = p.id '
                                         'INNER JOIN subsistema s on p.subsistema = s.id and s.id = %s '
                                         'WHERE a.id NOT IN(SELECT pase_examen.aspirante FROM pase_examen)', [self.id])
        except Exception as e:
            print (e.message)
            return None

    @staticmethod
    def aspirantes_sin_pago_efectuado(lista):
        return len(list(lista))

    def list_of_aspirantes_sin_pago_efectuado(self):
        try:
            return Aspirante.objects.raw('SELECT a.id, au.first_name, au.last_name, a.telefono, au.email, p.referencia '
                                         'FROM aspirante a '
                                         'INNER JOIN auth_user au ON a.usuario = au.id '
                                         'INNER JOIN revision_registro rr on a.id = rr.aspirante and rr.efectuado = 0 '
                                         'INNER JOIN seleccion_oferta_educativa soe on a.id = soe.aspirante and soe.preferencia = 1 '
                                         'INNER JOIN plantel_especialidad pes on soe.plantel_especialidad = pes.id '
                                         'INNER JOIN plantel p on pes.plantel = p.id '
                                         'INNER JOIN subsistema s on p.subsistema = s.id and s.id = %s', [self.id])
        except Exception as e:
            print (e.message)
            return None


class Especialidad(models.Model):
    id = models.AutoField(db_column='id', primary_key=True)
    referencia = models.CharField(db_column='referencia', max_length=50)
    subsistema = models.ForeignKey(Subsistema, db_column='subsistema', on_delete=models.DO_NOTHING)
    descripcion = models.CharField(db_column='descripcion', max_length=255, null=True, blank=True)

    class Meta:
        db_table = 'especialidad'
        unique_together = ('referencia', 'subsistema')
        verbose_name = 'Especialidad'
        verbose_name_plural = 'Especialidades'

    def __unicode__(self):
        return '%s - %s' % (self.referencia, self.subsistema.referencia)


class Plantel(models.Model):
    id = models.AutoField(db_column='id', primary_key=True)
    referencia = models.CharField(db_column='referencia', max_length=255)
    subsistema = models.ForeignKey(Subsistema, db_column='subsistema', on_delete=models.DO_NOTHING, related_name='planteles_related')
    cct = models.CharField(db_column='cct', max_length=10)
    telefono = models.CharField(db_column='telefono', max_length=30, null=True, blank=True)
    pagina_web = models.URLField(db_column='pagina_web', max_length=255, null=True, blank=True)
    domicilio = models.ForeignKey(Domicilio, db_column='domicilio', null=True, on_delete=models.DO_NOTHING)
    responsable = models.OneToOneField(User, db_column='responsable', null=True, blank=True, on_delete=models.DO_NOTHING, related_name='responsable_plantel_related')
    especialidades = models.ManyToManyField(Especialidad, through='OfertaEducativa')
    nivel_demanda = models.CharField(db_column='nivel_demanda', max_length=16, null=True)
    sede_ceneval = models.PositiveIntegerField(db_column='sede_ceneval', null=True)
    descuento = models.IntegerField(null=True, default=0)
    active = models.BooleanField(default=True)

    class Meta:
        db_table = 'plantel'
        verbose_name = 'Plantel educativo'
        verbose_name_plural = 'Planteles educativos'

    @property
    def status(self):
        if bool(self.active):
            return "Activo"
        return "Inactivo"

    def capacidad_total(self, incluir_sedes_alternas=True):
        capacidad_total = AulaPlantel.objects.filter(plantel=self).aggregate(Sum('capacidad'))
        '''
        for aula in AulaPlantel.objects.filter(plantel=self):
            capacidad_total += aula.capacidad
        '''

        '''
        if incluir_sedes_alternas:
            sedes_alternas = SedeAlterna.objects.filter(plantel=self)
            if sedes_alternas.count() > 0:
                for sede_alterna in sedes_alternas:
                    aulas_alternas = AulaSedeAlterna.objects.filter(sede_alterna=sede_alterna)
                    for aula_alterna in aulas_alternas:
                        capacidad_total += aula_alterna.capacidad
                        '''

        if incluir_sedes_alternas:
            sedes_alternas = SedeAlterna.objects.filter(plantel=self)
            if sedes_alternas.count() > 0:
                aulas_alternas = AulaSedeAlterna.objects.filter(sede_alterna__plantel=self).aggregate(Sum('capacidad'))
                capacidad_total['capacidad__sum'] += aulas_alternas['capacidad__sum']

        return capacidad_total['capacidad__sum']

    def total_aulas(self, incluir_sedes_alternas=True):
        total_aulas = AulaPlantel.objects.filter(plantel=self).count()
        if incluir_sedes_alternas:
            sedes_alternas = SedeAlterna.objects.filter(plantel=self)
            if sedes_alternas.count() > 0:
                for sede_alterna in sedes_alternas:
                    total_aulas += AulaSedeAlterna.objects.filter(sede_alterna=sede_alterna).count()
        return total_aulas

    def capacidad_ocupada(self, incluir_sedes_alternas=True):
        lugares_ocupados = 0
        for aula in AulaPlantel.objects.filter(plantel=self):
            lugares_ocupados += PaseExamen.objects.filter(aula=aula).count()
        if incluir_sedes_alternas:
            sedes_alternas = SedeAlterna.objects.filter(plantel=self)
            if sedes_alternas.count() > 0:
                for sede_alterna in sedes_alternas:
                    aulas_alternas = AulaSedeAlterna.objects.filter(sede_alterna=sede_alterna)
                    for aula_alterna in aulas_alternas:
                        lugares_ocupados += PaseExamen.objects.filter(aula=aula_alterna).count()
        return lugares_ocupados

    def porcentaje_ocupado(self, incluir_sedes_alternas=True):
        capacidad_total = self.capacidad_total(incluir_sedes_alternas)
        if capacidad_total > 0:
            return int((float(100) / capacidad_total) * self.capacidad_ocupada(incluir_sedes_alternas))
        return 0

    def demanda(self):
        try:
            plantel = Plantel.objects.raw('SELECT p.id, count(*) as aspirantes FROM seleccion_oferta_educativa soe '
                                          'INNER JOIN plantel_especialidad pe on soe.plantel_especialidad = pe.id '
                                          'INNER JOIN plantel p on pe.plantel=p.id '
                                          'WHERE soe.preferencia = 1 and p.id=%s '
                                          'GROUP BY pe.plantel', [self.id])[0]
            return plantel.aspirantes
        except Exception as e:
            return 0

    def aspirantes_sin_pago_completado(self):
        try:

            return Aspirante.objects.raw('SELECT a.*, au.first_name, au.last_name, au.email '
                                         'FROM aspirante a '
                                         'INNER JOIN seleccion_oferta_educativa soe ON a.id = soe.aspirante '
                                         'INNER JOIN plantel_especialidad pe ON soe.plantel_especialidad = pe.id '
                                         'INNER JOIN plantel p ON pe.plantel = p.id '
                                         'INNER JOIN revision_registro rr ON a.id = rr.aspirante '
                                         'INNER JOIN auth_user au ON a.usuario = au.id '
                                         'WHERE rr.efectuado = 0 AND soe.preferencia = 1 and p.id = %s', [self.id])
        except Exception as e:
            print (e.message)
            return None

    def aspirantes_con_proceso_completo(self):
        try:
            return Aspirante.objects.raw('SELECT au.first_name, au.last_name, au.email, a.* '
                                         'FROM aspirante a '
                                         'INNER JOIN auth_user au on a.usuario = au.id '
                                         'INNER JOIN pase_examen pe on a.id = pe.aspirante '
                                         'INNER JOIN seleccion_oferta_educativa soe on a.id = soe.aspirante '
                                         'INNER JOIN plantel_especialidad pes on soe.plantel_especialidad = pes.id '
                                         'INNER JOIN plantel p on pes.plantel = p.id '
                                         'WHERE soe.preferencia = 1 and p.id=%s', [self.id])
        except Exception as e:
            print (e.message)
            return None

    @staticmethod
    def demanda_capacida_porcentaje():
        try:
            return Plantel.objects.raw('SELECT p.id, p.referencia as p_referencia, s.referencia as s_referencia, '
                                       'IFNULL(plantel_demanda.demanda,0) as p_demanda, plantel_capacidad.capacidad as p_capacidad, '
                                       'IFNULL(FLOOR(((100/plantel_capacidad.capacidad)*plantel_demanda.demanda)),0) as p_porcentaje_ocupado '
                                       'FROM plantel p '
                                       'LEFT JOIN (SELECT p.id, count(pe.id) as demanda '
                                       'FROM seleccion_oferta_educativa soe '
                                       'INNER JOIN plantel_especialidad pe on soe.plantel_especialidad = pe.id '
                                       'INNER JOIN plantel p on pe.plantel = p.id '
                                       'WHERE soe.preferencia = 1 '
                                       'GROUP BY p.id) as plantel_demanda on plantel_demanda.id = p.id '
                                       'LEFT JOIN (SELECT p.id, sum(a.capacidad) as capacidad '
                                       'FROM aula a '
                                       'INNER JOIN aula_plantel ap on a.id = ap.aula '
                                       'INNER JOIN plantel p on ap.plantel = p.id '
                                       'GROUP BY p.id) as plantel_capacidad on plantel_capacidad.id = p.id '
                                       'INNER JOIN subsistema s on p.subsistema = s.id '
                                       'HAVING p_porcentaje_ocupado >= 50 '
                                       'ORDER BY s.referencia, p.referencia')
        except:
            return None

    def __unicode__(self):
        return '%s' % self.referencia

    def numero_opciones(self):
        if self.nivel_demanda is None:
            return NIVELES_DEMANDA[NIVEL_DEMANDA_DEFAULT]
        return NIVELES_DEMANDA[self.nivel_demanda]


class OfertaEducativa(models.Model):
    id = models.AutoField(db_column='id', primary_key=True)
    plantel = models.ForeignKey(Plantel, db_column='plantel', on_delete=models.DO_NOTHING, related_name='oferta_eduacativa_related')
    especialidad = models.ForeignKey(Especialidad, db_column='especialidad', on_delete=models.DO_NOTHING)
    activa = models.BooleanField(db_column='activa', default=False)
    clave_especialidad = models.CharField(db_column='clave_especialidad', max_length=15)
    programa_estudio = models.CharField(db_column='programa_estudio', max_length=50)

    class Meta:
        db_table = 'plantel_especialidad'
        unique_together = ('plantel', 'especialidad')
        verbose_name = 'Oferta educativa'
        verbose_name_plural = 'Oferta educativa'

    def __unicode__(self):
        return '%s - %s' % (self.especialidad.referencia, self.plantel.referencia)

    @property
    def alumnos(self):
        try:
            if not self.activa:
                return 0
            return PoblacionEstudiantilProyeccion.objects.get(oferta_educativa=self).alumnos
        except Exception as e:
            return 0

    @property
    def grupos(self):
        try:
            if not self.activa:
                return 0
            return PoblacionEstudiantilProyeccion.objects.get(oferta_educativa=self).grupos
        except Exception as e:
            return 0

    @property
    def total_proyeccion(self):
        return self.alumnos * self.grupos

    @property
    def status(self):
        if bool(self.activa):
            return "Activa"
        return "Inactiva"


class SedeAlterna(models.Model):
    id = models.AutoField(db_column='id', primary_key=True)
    referencia = models.CharField(db_column='referencia', max_length=30)
    domicilio = models.ForeignKey(Domicilio, db_column='domicilio', on_delete=models.DO_NOTHING)
    sede_ceneval = models.PositiveIntegerField(db_column='sede_ceneval', null=True)
    plantel = models.ForeignKey(Plantel, db_column='plantel', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'sede_alterna'
        verbose_name = 'Sede alterna'
        verbose_name_plural = 'Sedes alternas'

    def __unicode__(self):
        return '%s (alterna de %s)' % (self.referencia, self.plantel.referencia)

    def capacidad_total(self):
        aggregate = AulaSedeAlterna.objects.filter(sede_alterna=self).aggregate(Sum('capacidad'))
        return aggregate['capacidad__sum']

    def total_aulas(self):
        return AulaSedeAlterna.objects.filter(sede_alterna=self).count()

    def capacidad_ocupada(self):
        lugares_ocupados = 0
        aulas_alternas = AulaSedeAlterna.objects.filter(sede_alterna=self)
        for aula_alterna in aulas_alternas:
            lugares_ocupados += PaseExamen.objects.filter(aula=aula_alterna).count()
        return lugares_ocupados

    def capacidad_ocupada_1(self, plantel_id):
        try:
            sedes_alternas = PaseExamen.objects.raw("SELECT '1' as id, sum(capacidad_ocupada) as capacidad_ocupada "
                                                    "FROM (SELECT count(asa.aula) as capacidad_ocupada "
                                                    "FROM pase_examen pe INNER JOIN aula a on a.id = pe.aula "
                                                    "INNER JOIN aula_sede_alterna asa on asa.aula = a.id "
                                                    "INNER JOIN sede_alterna sa on sa.id = asa.sede_alterna "
                                                    "WHERE sa.plantel = %s and sa.id = %s GROUP BY asa.aula) as a", [plantel_id, self.id])[0]
            if sedes_alternas.capacidad_ocupada is None:
                return 0
            return sedes_alternas.capacidad_ocupada
        except Exception as e:
            return 0

    def porcentaje_ocupado(self):
        capacidad_total = self.capacidad_total()
        if capacidad_total > 0:
            return int((float(100) / capacidad_total) * self.capacidad_ocupada())
        return 0


class AulaPlantel(Aula):
    aula = models.OneToOneField(Aula, db_column='aula', primary_key=True, parent_link=True, on_delete=models.DO_NOTHING)
    plantel = models.ForeignKey(Plantel, db_column='plantel', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'aula_plantel'
        unique_together = ('aula', 'plantel')
        verbose_name = 'Aula (de plantel)'
        verbose_name_plural = 'Aulas (de planteles)'

    def __unicode__(self):
        return '%s - %s' % (self.aula.referencia, self.plantel.referencia)


class AulaSedeAlterna(Aula):
    aula = models.OneToOneField(Aula, db_column='aula', primary_key=True, parent_link=True, on_delete=models.DO_NOTHING)
    sede_alterna = models.ForeignKey(SedeAlterna, db_column='sede_alterna', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'aula_sede_alterna'
        unique_together = ('aula', 'sede_alterna')
        verbose_name = 'Aula (de sede alterna)'
        verbose_name_plural = 'Aulas (de sedes alternas)'

    def __unicode__(self):
        return '%s - %s' % (self.aula.referencia, self.sede_alterna.referencia)


class PoblacionEstudiantil(models.Model):
    id = models.AutoField(db_column='id', primary_key=True)
    oferta_educativa = models.ForeignKey(OfertaEducativa, db_column='oferta_educativa', on_delete=models.DO_NOTHING,
                                         related_name='poblacion_estudiantil_related')
    semestre = models.IntegerField(db_column='semestre')
    grupos = models.IntegerField(db_column='grupos')
    reprobados = models.IntegerField(db_column='reprobados', null=True)
    alumnos = models.IntegerField(db_column='alumnos')

    class Meta:
        db_table = 'poblacion_estudiantil'

    @property
    def total(self):
        return self.grupos * self.alumnos

    @property
    def total_nuevo_ingreso(self):
        return self.alumnos - self.reprobados


class PoblacionEstudiantilProyeccion(models.Model):
    id = models.AutoField(db_column='id', primary_key=True)
    oferta_educativa = models.ForeignKey(OfertaEducativa, db_column='oferta_educativa', on_delete=models.DO_NOTHING,
                                         related_name='poblacion_estudiantil_proyeccion_related')
    semestre = models.IntegerField(db_column='semestre')
    grupos = models.IntegerField(db_column='grupos')
    alumnos = models.IntegerField(db_column='alumnos')

    class Meta:
        db_table = 'poblacion_estudiantil_proyeccion'
        unique_together = ('oferta_educativa', 'semestre')

    @property
    def total(self):
        return self.grupos * self.alumnos


class PlantelHistorico(models.Model):
    cct = models.CharField(max_length=10)
    subsistema = models.CharField(max_length=30)
    plantel = models.CharField(max_length=255)
    especialidades = models.IntegerField()
    grupos = models.IntegerField()
    alumnos = models.IntegerField()
    anio = models.IntegerField()

    class Meta:
        db_table = 'plantel_historico'
        managed = False


class OfertaEducativaHistorico(models.Model):
    municipio = models.CharField(max_length=255)
    localidad = models.CharField(max_length=255)
    subsistema = models.CharField(max_length=30)
    cct = models.CharField(max_length=10)
    plantel = models.CharField(max_length=255)
    especialidad = models.CharField(max_length=50)
    domicilio = models.TextField()
    grupos = models.IntegerField()
    alumnos_por_grupo = models.IntegerField()
    alumnos_total = models.IntegerField()
    anio = models.IntegerField()

    class Meta:
        db_table = 'poblacion_estudiantil_historico'
        managed = False
