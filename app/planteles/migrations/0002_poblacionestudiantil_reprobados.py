# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-10-21 12:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('planteles', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='poblacionestudiantil',
            name='reprobados',
            field=models.IntegerField(db_column='reprobados', null=True),
        ),
    ]
