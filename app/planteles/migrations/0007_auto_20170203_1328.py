# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-02-03 13:28
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('planteles', '0006_plantel_active'),
    ]

    operations = [
        migrations.RenameField(
            model_name='plantel',
            old_name='active',
            new_name='status',
        ),
    ]
