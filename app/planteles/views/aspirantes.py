# -*- encoding: utf-8 -*
from django.views import generic

from app.planteles.utils import PlantelUtils


class AspirantesSinPagoTemplateView(PlantelUtils, generic.TemplateView):
    template_name = 'aspirantes/sin_pago_efectuado.html'

    def get_context_data(self, **kwargs):
        context = super(AspirantesSinPagoTemplateView, self).get_context_data(**kwargs)
        plantel = self.plantel
        aspirantes_sin_pago = plantel.aspirantes_sin_pago_completado()
        context.update({
            'plantel':                   plantel,
            'aspirantes_sin_pago':       aspirantes_sin_pago,
            'aspirantes_sin_pago_count': len(list(aspirantes_sin_pago))
        })
        return context
