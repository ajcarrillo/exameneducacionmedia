# -*- encoding: utf-8 -*
from django.views import generic

from app.planteles.models import SedeAlterna
from app.planteles.utils import PlantelUtils


class PLantelHomeView(PlantelUtils, generic.TemplateView):
    template_name = 'home/home.html'

    def get_context_data(self, **kwargs):
        context = super(PLantelHomeView, self).get_context_data(**kwargs)
        plantel = self.plantel

        capacidad_total = 0 if plantel.capacidad_total(incluir_sedes_alternas=False) is None else plantel.capacidad_total(incluir_sedes_alternas=False)
        total_aulas = plantel.total_aulas(incluir_sedes_alternas=False)
        capacidad_ocupada = 0 if plantel.capacidad_ocupada(incluir_sedes_alternas=False) is None else plantel.capacidad_ocupada(incluir_sedes_alternas=False)
        oferta_eduacativa_related = plantel.oferta_eduacativa_related.all()
        demanda = plantel.demanda

        try:
            porcentaje_ocupado = int((float(100) / capacidad_total) * capacidad_ocupada)
        except:
            porcentaje_ocupado = 0

        plantel.totales = {
            'capacidad':          capacidad_total,
            'aulas':              total_aulas,
            'capacidad_ocupada':  capacidad_ocupada,
            'oferta_educativa':   oferta_eduacativa_related,
            'demanda':            demanda,
            'porcentaje_ocupado': porcentaje_ocupado
        }

        if SedeAlterna.objects.filter(plantel=plantel).exists():
            plantel.sedes_alternas = SedeAlterna.objects.filter(plantel=plantel).all()
            for sede_alterna in plantel.sedes_alternas:
                capacidad_total = sede_alterna.capacidad_total()
                aulas = sede_alterna.total_aulas()
                capacidad_ocupada = sede_alterna.capacidad_ocupada_1(plantel.id)

                sede_alterna.totales = {
                    'capacidad':          capacidad_total,
                    'aulas':              aulas,
                    'capacidad_ocupada':  capacidad_ocupada,
                    'porcentaje_ocupado': int((float(100) / capacidad_total) * capacidad_ocupada)
                }

        aspirantes_sin_pago = plantel.aspirantes_sin_pago_completado()
        aspirantes_con_proceso_completo = plantel.aspirantes_con_proceso_completo()
        context.update({
            'plantel':             plantel,
            'aspirantes_sin_pago_count': len(list(aspirantes_sin_pago)),
            'aspirantes_con_proceso_completo': len(list(aspirantes_con_proceso_completo))
        })
        return context
