# -*- encoding: utf-8 -*
from django.conf.urls import url

from app.planteles import views


# **************************************
#
#   url: /plantel/
#   namespace: planteles
#
# **************************************

urlpatterns = [
    url(r'^$', views.PLantelHomeView.as_view(), name='home'),
    url(r'^aspirantes/sin-pago-efectuado/$', views.AspirantesSinPagoTemplateView.as_view(), name='aspirante_sin_pago_efecutado'),
    url(r'^(?P<plantel_id>[\d]+)/oferta/$', views.OfertaView.as_view(), name='oferta'),
]
