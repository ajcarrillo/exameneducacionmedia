# -*- encoding: utf-8 -*
from general.mixins import LoginRequiredMixin, GroupRequiredMixin


class PlantelUtils(LoginRequiredMixin, GroupRequiredMixin, object):
    group_required = 'plantel'
    plantel = None

    def dispatch(self, request, *args, **kwargs):
        self.plantel = self._plantel_asignado(request)
        return super(PlantelUtils, self).dispatch(request, *args, **kwargs)

    def _plantel_asignado(self, request):
        user = request.user
        if hasattr(user, 'responsable_plantel_related'):
            return user.responsable_plantel_related
        return None
