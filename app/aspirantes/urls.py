# -*- encoding: utf-8 -*
from django.conf.urls import url

from app.aspirantes import views

# **************************************
#
#   url: /aspirante/
#   namespace: aspirantes
#
# **************************************

urlpatterns = [
    url(r'^$', views.DashboardView.as_view(), name='dashboard'),

    url(r'^api/get-planteles-by/$', views.get_json_plantel_by, name='api_get_planteles_by'),
    url(r'^api/get-especialidades-by/$', views.get_json_especialidad_by, name='api_get_especialidades_by'),
    url(r'^api/get-aspirante-opciones-educativas/$', views.get_json_aspirante_opciones_educativas, name='api_get_aspirante_opciones_educativas'),

    url(r'^cuestionario/$', views.CrearCuestionarioEnBlanco.as_view(), name='create_blank_survey'),
    url(r'^cuestionario/responder/$', views.PreguntaListView.as_view(), name='respond_survey'),
    url(r'^cuestionario/respuesta/$', views.CuestionarioContextoCreate.as_view(), name='save_answer'),

    url(r'^enviar-registro/$', views.EnviarRegistro.as_view(), name='enviar_registro'),
    url(r'^ficha-deposito/$', views.FichaDeposito.as_view(), name='ficha_deposito'),
    url(r'^pase-examen/generar/$', views.PaseAlExamen.as_view(), name='pase_al_examen'),
    url(r'^pase-examen/descargar/$', views.FichaDeRegistro.as_view(), name='ficha_de_registro'),


    url(r'^perfil/$', views.ProfileView.as_view(), name='profile'),
    url(r'^perfil/datos-generales/$', views.ApplicantGeneralData.as_view(), name='profile_basic_info'),
    url(r'^perfil/update-email/$', views.UpdateApplicantEmail.as_view(), name='profile_email'),
    url(r'^perfil/update-password/$', views.UpdateApplicantPassword.as_view(), name='profile_password'),
    url(r'^perfil/domicilio/$', views.AspiranteDomicilioView.as_view(), name='save_address'),
    url(r'^perfil/domicilio/actualizar$', views.AspiranteDomicilioUpdateView.as_view(), name='update_address'),
    url(r'^perfil/informacion-procedencia/$', views.InformacionProcedenciaCreateView.as_view(), name='informacion_procedencia'),
    #url(r'^perfil/domicilio/$', '', name='profile_address'),
    #url(r'^perfil/informacion-de-procedencia/$', '', name='profile_informacion_de_procedencia'),

    url(r'^mis-opciones/$', views.MiSeleccionEducativaView.as_view(), name='mis_opciones'),
    url(r'^opciones-educativas/$', views.SeleccionOfertaEducativaView.as_view(), name='opciones_educativas'),

    url(r'^registro/matricula$', views.SignupMatricula.as_view(), name='signup_matricula'),
    url(r'^registro/curp$', views.SignupCurp.as_view(), name='signup_curp'),
    url(r'^registro/extranjero$', views.SignupExtranjero.as_view(), name='signup_extranjero'),
    url(r'^registro/obtener-datos/$', views.BuscarAlumnoEnSiceeb.as_view(), name='search_on_siceeb'),
    url(r'^registro/validar-curp/$', views.ConsultarCurpRenapo.as_view(), name='search_curp')
]
