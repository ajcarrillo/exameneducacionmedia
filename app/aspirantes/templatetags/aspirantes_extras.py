# -*- encoding: utf-8 -*
from django import template
from django.contrib.auth.models import User
from django.utils.html import format_html

register = template.Library()


@register.simple_tag(name='questions')
def questions(pregunta):
    if pregunta.children_questions_related.exists():
        pass


@register.simple_tag(name='has_already_answer')
def has_already_answer(question_id, answer_id, cuestionario):
    for item in cuestionario:
        if item.pregunta_id == question_id and item.respuesta_id == answer_id:
            return 'selected'
    return ''


@register.simple_tag(name='change_color_box')
def change_color_box(percent):
    if percent == 100:
        return 'bg-green'

    if percent < 50:
        return 'bg-red'

    if percent >= 50:
        return 'bg-yellow'
