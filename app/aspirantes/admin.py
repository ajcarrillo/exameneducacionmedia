from django.contrib import admin

from .models import *


class CenevalPreguntaAdmin(admin.ModelAdmin):
    model = CenevalPregunta
    list_display = ('id', 'variable', 'nombre', 'diccionario')
    list_display_links = ('nombre',)
    search_fields = ('variable',)

    def get_queryset(self, request):
        qs = super(CenevalPreguntaAdmin, self).get_queryset(request)
        return qs.exclude(padre__isnull=True, variable__isnull=True)


class CenevalDiccionarioAdmin(admin.ModelAdmin):
    model = CenevalDiccionario
    list_display = ('id', 'nombre', 'get_respuestas')
    list_display_links = ('nombre',)
    search_fields = ('nombre',)

    def get_respuestas(self, obj):
        etiquetas = CenevalRespuesta.objects.filter(diccionario=obj)
        return " - ".join([c.etiqueta for c in etiquetas])

    get_respuestas.short_description = 'Etiquetas'


class CenevalRespuestaAdmin(admin.ModelAdmin):
    model = CenevalRespuesta
    list_display = ('diccionario', 'valor', 'etiqueta',)
    search_fields = ('etiqueta', 'diccionario__nombre')
    ordering = ('valor',)


admin.site.register(CenevalDiccionario, CenevalDiccionarioAdmin)
admin.site.register(CenevalRespuesta, CenevalRespuestaAdmin)
admin.site.register(CenevalPregunta, CenevalPreguntaAdmin)
