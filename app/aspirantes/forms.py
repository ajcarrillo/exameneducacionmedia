# -*- encoding: utf-8 -*
from django import forms

from app.aspirantes.magic_numbers import ENTIDAD_NACIMIENTO_AND_EMPTY, GENERO, GENERO_AND_EMPTY, SI_NO, COUNTRIES_NO_MEXICO
from app.aspirantes.models import Aspirante, Alumno, InformacionProcedencia
from app.mgeo.forms import DomicilioForm
from exameneducacionmedia.utils import BetterForm, CurpField, CCTField, PhoneField


class GeneralesForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-lg', 'readonly': ''}), required=True, label='Nombres')
    apellido_paterno = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-lg', 'readonly': ''}), required=True,
                                       label='Primer apellido')
    apellido_materno = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-lg', 'readonly': ''}), required=False,
                                       label='Segundo apellido')
    fecha_nacimiento = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-lg', 'readonly': ''}), required=True,
                                       label='Fecha nacimiento')
    sexo_select = forms.ChoiceField(choices=GENERO, widget=forms.Select(attrs={'class': 'form-control input-lg', 'disabled': ''}),
                                    required=False, label='Sexo')
    username = forms.CharField(min_length=6, max_length=20, required=True, label="Usuario",
                               widget=forms.TextInput(attrs={'class': 'form-control input-lg'}))
    email = forms.EmailField(required=True, label="Email",
                             widget=forms.EmailInput(attrs={'class': 'form-control input-lg'}))
    password = forms.CharField(min_length=6, max_length=20, required=True, label='Contraseña',
                               widget=forms.PasswordInput(attrs={'class': 'form-control input-lg', 'placeholder': 'La contraseña debe tener más de 6 caracteres y por lo menos un número.'}))
    repassword = forms.CharField(min_length=6, max_length=20, required=True, label='Repetir de contraseña',
                                 widget=forms.PasswordInput(attrs={'class': 'form-control input-lg'}))
    sexo = forms.CharField(widget=forms.HiddenInput())
    last_name = forms.CharField(widget=forms.HiddenInput())


class DatosGeneralesForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-lg'}), label="Nombre(s)")
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-lg'}), label="Apellidos")
    fecha_nacimiento = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-lg'}), label="Fecha de nacimiento")
    sexo = forms.ChoiceField(choices=GENERO_AND_EMPTY, widget=forms.Select(attrs={'class': 'form-control input-lg'}), label='Sexo')


class CredencialesForm(forms.Form):
    username = forms.CharField(min_length=6, max_length=20, required=True, label="Usuario",
                               widget=forms.TextInput(attrs={'class': 'form-control input-lg'}))
    email = forms.EmailField(required=True, label="Email",
                             widget=forms.EmailInput(attrs={'class': 'form-control input-lg'}))
    password = forms.CharField(min_length=6, max_length=20, required=True, label='Contraseña',
                               widget=forms.PasswordInput(attrs={'class': 'form-control input-lg'}))
    repassword = forms.CharField(min_length=6, max_length=20, required=True, label='Repetir de contraseña',
                                 widget=forms.PasswordInput(attrs={'class': 'form-control input-lg'}))


class SignupMatriculaForm(GeneralesForm, BetterForm):
    matricula = forms.CharField(min_length=10, max_length=10,
                                widget=forms.TextInput(attrs={'class': "form-control input-lg", 'pattern': "[0-9]{10}"}))
    curp = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-lg', 'readonly': ''}), required=True, label="CURP")
    entidad_nacimiento_select = forms.ChoiceField(choices=ENTIDAD_NACIMIENTO_AND_EMPTY,
                                                  widget=forms.Select(attrs={'class': 'form-control input-lg'}), required=True,
                                                  label='Entidad nacimiento')
    entidad_nacimiento = forms.CharField(max_length=2, min_length=2, widget=forms.HiddenInput(), required=False)


class SignupCurpForm(GeneralesForm, BetterForm):
    curp = CurpField(min_length=18, max_length=18, required=True, label='CURP',
                     widget=forms.TextInput(attrs={'class':   'form-control input-lg',
                                                   'pattern': "[A-Z]{4}\d{6}[HM][A-Z]{2}[B-DF-HJ-NP-TV-Z]{3}[A-Z0-9][0-9]"}))
    entidad_nacimiento_select = forms.ChoiceField(choices=ENTIDAD_NACIMIENTO_AND_EMPTY,
                                                  widget=forms.Select(attrs={'class': 'form-control input-lg', 'disabled': ''}), required=False,
                                                  label='Entidad nacimiento')
    entidad_nacimiento = forms.CharField(max_length=2, min_length=2, widget=forms.HiddenInput(), required=True)


class SignupExtranjeroForm(DatosGeneralesForm, CredencialesForm, BetterForm):
    pais_nacimiento = forms.ChoiceField(choices=COUNTRIES_NO_MEXICO, widget=forms.Select(attrs={'class': 'form-control input-lg'}), label="País de procedencia")


class SignupForm(BetterForm):
    soy_de_qroo = forms.BooleanField(required=False, label='Soy de Quintana Roo', widget=forms.CheckboxInput())
    curp = CurpField(min_length=18, max_length=18, required=True, label='CURP',
                     widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'CURP'}))
    matricula = forms.CharField(min_length=4, max_length=20, required=False, label='Matricula',
                                widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Matricula'}))
    username = forms.CharField(min_length=6, max_length=20, required=True, label="Usuario",
                               widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Usuario'}))
    email = forms.EmailField(required=True, label="Email",
                             widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Correo electrónico'}))
    password = forms.CharField(min_length=6, max_length=20, required=True, label='Contraseña',
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Contraseña'}))
    repassword = forms.CharField(min_length=6, max_length=20, required=True, label='Repetición de contraseña',
                                 widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Repite tu contraseña'}))


class AlumnoForm(forms.ModelForm):
    class Meta:
        model = Alumno
        fields = '__all__'
        exclude = ('id_alumno', 'nivel', 'estatus', 'cct',)


class AspiranteDomicilioForm(BetterForm, DomicilioForm):
    telefono = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-lg'}), label='Teléfono')


class AspiranteInformacionProcedenciaModelForm(forms.ModelForm):
    aspirante = forms.IntegerField(widget=forms.HiddenInput())
    cct_secundaria = CCTField(min_length=10, max_length=10,
                              widget=forms.TextInput(attrs={'class': 'form-control input-lg'}), label='CCT escuela', required=False)
    nombre_secundaria = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-lg'}), label='Nombre')
    fecha_egreso = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-lg'}), label='Fecha de egreso')
    primera_vez = forms.ChoiceField(choices=SI_NO, widget=forms.RadioSelect(), label='Primera vez')

    def clean_aspirante(self):
        data = self.cleaned_data['aspirante']
        try:
            return Aspirante.objects.get(pk=data)
        except Aspirante.DoesNotExist:
            raise forms.ValidationError("El aspirante no existe")

    class Meta:
        model = InformacionProcedencia
        fields = '__all__'


class ApplicantProfileform(DatosGeneralesForm):
    delete_fields = ['first_name', 'last_name', 'fecha_nacimiento', 'sexo', 'country']
    country = forms.ChoiceField(choices=COUNTRIES_NO_MEXICO, widget=forms.Select(attrs={'class': 'form-control input-lg'}), label='País de nac.')
    phone = PhoneField(widget=forms.TextInput(attrs={'class': 'form-control input-lg', 'pattern': "[0-9]{10}"}), label='Teléfono')

    def __init__(self, *args, **kwargs):
        is_foreign = kwargs.get('is_foreign', False)
        if 'is_foreign' in kwargs:
            kwargs.pop('is_foreign')
        super(ApplicantProfileform, self).__init__(*args, **kwargs)
        if not is_foreign:
            for field in self.delete_fields:
                del self.fields[field]

