# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-10-17 12:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aspirantes', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='aspirante',
            name='cuestionario_de_contexto',
        ),
        migrations.RemoveField(
            model_name='informacionprocedencia',
            name='promedio_general',
        ),
        migrations.AlterField(
            model_name='cuestionariodecontexto',
            name='PROM_ESP',
            field=models.CharField(choices=[(b'', b'Promedios'), (b'1', b'6.0 - 6.4'), (b'2', b'6.5 - 6.9'), (b'3', b'7.0 - 7.4'), (b'4', b'7.5 - 7.9'), (b'5', b'8.0 - 8.4'), (b'6', b'8.5 - 8.9'), (b'7', b'9.0 - 9.4'), (b'8', b'9.5 - 9.9'), (b'9', b'10.0')], max_length=1, null=True),
        ),
        migrations.AlterField(
            model_name='cuestionariodecontexto',
            name='PROM_MAT',
            field=models.CharField(choices=[(b'', b'Promedios'), (b'1', b'6.0 - 6.4'), (b'2', b'6.5 - 6.9'), (b'3', b'7.0 - 7.4'), (b'4', b'7.5 - 7.9'), (b'5', b'8.0 - 8.4'), (b'6', b'8.5 - 8.9'), (b'7', b'9.0 - 9.4'), (b'8', b'9.5 - 9.9'), (b'9', b'10.0')], max_length=1, null=True),
        ),
        migrations.AlterField(
            model_name='cuestionariodecontexto',
            name='PROM_SEC',
            field=models.CharField(choices=[(b'', b'Promedios'), (b'1', b'6.0 - 6.4'), (b'2', b'6.5 - 6.9'), (b'3', b'7.0 - 7.4'), (b'4', b'7.5 - 7.9'), (b'5', b'8.0 - 8.4'), (b'6', b'8.5 - 8.9'), (b'7', b'9.0 - 9.4'), (b'8', b'9.5 - 9.9'), (b'9', b'10.0')], max_length=1, null=True),
        ),
    ]
