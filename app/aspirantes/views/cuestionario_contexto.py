# -*- encoding: utf-8 -*
from django.db import transaction
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.urls import reverse_lazy
from django.views import generic

from app.aspirantes.magic_numbers import VARIABLES_CENEVAL
from app.aspirantes.models import CenevalPregunta, CenevalCuestionarioDeContexto
from app.aspirantes.views.dashboard import DashboardUtils
from general.mixins import LoginRequiredMixin, GroupRequiredMixin


class CrearCuestionarioEnBlanco(LoginRequiredMixin, GroupRequiredMixin, generic.View):
    group_required = 'alumno'

    success_url = reverse_lazy('aspirantes:respond_survey')

    def get(self, request):
        transaction.set_autocommit(False)
        aspirante = request.user.aspirante_related
        if aspirante.cuestionario_related.exists():
            return HttpResponseRedirect(self.success_url)
        preguntas = CenevalPregunta.objects.filter(variable__isnull=False)
        try:
            for pregunta in preguntas:
                respuesta = CenevalCuestionarioDeContexto()
                respuesta.aspirante = aspirante
                respuesta.pregunta = pregunta
                respuesta.save()
            transaction.commit()
            return HttpResponseRedirect(self.success_url)
        except Exception as e:
            print ('*' * 50)
            print (e.message)
            print ('*' * 50)
            transaction.rollback()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class PreguntaListView(LoginRequiredMixin, GroupRequiredMixin, DashboardUtils, generic.ListView):
    group_required = 'alumno'

    model = CenevalPregunta
    template_name = "cuestionario_contexto/list.html"
    context_object_name = "preguntas"
    paginate_by = 5

    def get_queryset(self):
        return self.model.objects.prefetch_related('children_related', 'children_related__children_related__diccionario',
                                                   'children_related__diccionario__respuestas_related').filter(padre__isnull=True,
                                                                                                               variable__isnull=True)

    def get_context_data(self, **kwargs):
        context = super(PreguntaListView, self).get_context_data(**kwargs)
        cuestionario = self.aspirante.cuestionario_related.all()
        context.update({
            'aspirante': self.aspirante,
            'porcentaje': self.porcentaje,
            'cuestionario': cuestionario,
        })

        return context

    def post(self, request):
        aspirante = request.user.aspirante_related

        page = int(request.GET.get("page", 1))
        # TODO: quitar variables ceneval y obtener las variables dinamicamente
        for nombre_campo in VARIABLES_CENEVAL:
            if request.POST.get(nombre_campo):
                pregunta = CenevalPregunta.objects.get(variable=nombre_campo)
                cuestionario = CenevalCuestionarioDeContexto.objects.get(aspirante=aspirante, pregunta=pregunta)
                cuestionario.respuesta_id = request.POST.get(nombre_campo)
                cuestionario.save()

        page += 1
        # TODO: hacer esto de manera dinamica
        if page == 6:
            return HttpResponseRedirect(reverse('aspirantes:dashboard'))
        return HttpResponseRedirect('/aspirantes/cuestionario/responder/?page={0}'.format(page))


class CuestionarioContextoCreate(generic.ListView):
    def post(self, request, page=None):
        aspirante = request.user.aspirante_related

        print(page)

        for nombre_campo in VARIABLES_CENEVAL:
            if request.POST.get(nombre_campo):
                pregunta = CenevalPregunta.objects.get(variable=nombre_campo)
                cuestionario = CenevalCuestionarioDeContexto.objects.get(aspirante=aspirante, pregunta=pregunta)
                cuestionario.respuesta_id = request.POST.get(nombre_campo)
                cuestionario.save()

        page = 1
        return HttpResponseRedirect('/aspirantes/cuestionario/responder/?page={0}'.format(page))
