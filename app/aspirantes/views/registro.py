# -*- encoding: utf-8 -*
import datetime

import requests
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User, Group
from django.db import transaction
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic

from app.administracion.models import Configuracion, EtapaProceso, Folio
from app.aspirantes.forms import AlumnoForm, SignupForm, SignupMatriculaForm, SignupCurpForm, SignupExtranjeroForm
from app.aspirantes.models import AlumnoGeneral, Aspirante
from exameneducacionmedia import settings
from exameneducacionmedia.settings import RENAPO_URL
from general.mixins import AsegurarEtapaMixin, RedirectMixin


class BuscarAlumnoEnSiceeb(generic.View):
    def get(self, request):
        if request.is_ajax():
            format = request.GET.get('format', None)
            if format is not None and format == 'json':
                return self.json_to_response()
            return HttpResponseBadRequest("La url es inválida")
        return render(request, "registro/buscar_alumno_siceeb.html", {})

    def post(self, request):
        if request.is_ajax():
            format = request.GET.get('format', None)
            if format is not None and format == 'json':
                return self.json_to_response()
            return HttpResponseBadRequest("La url es inválida")
        alumnos = self.get_data()
        message = None
        if alumnos.count() is 0:
            message = 'No se encontraron datos'
        context = {'alumnos': alumnos, 'msg': message}

        return render(request, "registro/buscar_alumno_siceeb.html", context)

    def get_data(self):
        request = self.request

        name = request.POST.get('name', None)
        first_name = request.POST.get('first_name', None)
        last_name = request.POST.get('last_name', None)
        curp = request.POST.get('curp', None)
        matricula = request.GET.get('matricula', None)

        alumnos = AlumnoGeneral.get_alumno(name, first_name, last_name, curp, matricula)

        return alumnos

    def json_to_response(self):
        alumnos = self.get_data()
        data = [{
                    'matricula':        alumno.matricula,
                    'curp':             alumno.curp,
                    'first_name':       alumno.nombre,
                    'apellido_paterno': alumno.apellido_paterno,
                    'apellido_materno': alumno.apellido_materno,
                    'fecha_nacimiento': alumno.fecha_nacimiento,
                    'sexo':             alumno.sexo,
                    'genero':           alumno.genero,
                    'last_name':        alumno.last_name,
                } for alumno in alumnos]

        return JsonResponse(data, safe=False)


class SignUpUtils(RedirectMixin, object):
    curp = None
    matricula = None
    control_escolar_id = None
    first_name = None
    last_name = None
    fecha_nacimiento = None
    sexo = None
    entidad_nacimiento = None
    pais_nacimiento = None
    email = None
    username = None
    password = None
    repassword = None
    form_class = None
    template_name = None

    def dispatch(self, request, *args, **kwargs):
        etapa = EtapaProceso.objects.get(nombre="REGISTRO")
        desc = 'La etapa de {0} '.format(etapa.nombre)
        today = datetime.date.today()

        try:
            if etapa.apertura is None:
                raise Exception('{0} no tiene fecha de apertura.'.format(desc))
            elif etapa.cierre is None:
                raise Exception('{0} no tiene fecha de cierre.'.format(desc))
            elif today < etapa.apertura:
                raise Exception('{0} aun no inicia.'.format(desc))
            elif today > etapa.cierre:
                raise Exception('{0} ya finalizó.'.format(desc))

            return super(SignUpUtils, self).dispatch(request, *args, **kwargs)
        except Exception as e:
            messages.info(request, e.message)
            return redirect(reverse('home:home'))

    def create_user_estudio_en_qroo(self):
        transaction.set_autocommit(False)
        alumno = AlumnoGeneral.obtener_alumno(matricula=self.matricula)
        if alumno is None:
            raise Exception('No tienes información en control escolar')
        if alumno is not None:
            if Aspirante.objects.filter(control_escolar_id=alumno.id_alumno).exists():
                raise Exception('Esta cuenta ya existe, inicia sesión con tu usuario y contraseña')

        # Comprobación de edad
        AlumnoGeneral.comprobar_edad(alumno=alumno)

        if Aspirante.objects.filter(curp=self.curp).exists():
            raise Exception('Esta cuenta ya existe, inicia sesión con tu usuario y contraseña')

        user = User.objects.create_user(username=self.username, password=self.password)
        aspirante = Aspirante()
        aspirante.control_escolar_id = alumno.id_alumno
        aspirante.matricula = alumno.matricula
        user.first_name = self.first_name
        user.last_name = self.last_name
        aspirante.fecha_nacimiento = self.fecha_nacimiento
        aspirante.sexo = self.sexo
        aspirante.entidad_nacimiento = self.entidad_nacimiento
        user.groups.add(Group.objects.get(name='alumno'))
        user.email = self.email
        user.save()

        aspirante.usuario = user
        aspirante.curp = self.curp
        aspirante.folio_ceneval = self.get_folio_ceneval()
        aspirante.save()
        transaction.commit()

        transaction.set_autocommit(True)
        user = authenticate(username=user.username, password=self.password)
        login(self.request, user)

    def create_user_con_curp(self):
        transaction.set_autocommit(False)
        alumno = AlumnoGeneral.obtener_alumno(curp=self.curp)
        if alumno is not None:
            raise Exception('Esta CURP está reservada para un alumno del estado de Quintana Roo')

        # Comprobación de edad
        AlumnoGeneral.comprobar_edad(curp=self.curp)

        if Aspirante.objects.filter(curp=self.curp).exists():
            raise Exception('Esta cuenta ya existe, inicia sesión con tu usuario y contraseña')

        user = User.objects.create_user(username=self.username, password=self.password)
        aspirante = Aspirante()
        user.first_name = self.first_name
        user.last_name = self.last_name
        aspirante.fecha_nacimiento = self.fecha_nacimiento
        aspirante.sexo = self.sexo
        aspirante.entidad_nacimiento = self.entidad_nacimiento
        user.groups.add(Group.objects.get(name='alumno'))
        user.email = self.email
        user.save()

        aspirante.usuario = user
        aspirante.curp = self.curp
        aspirante.folio_ceneval = self.get_folio_ceneval()
        aspirante.save()
        transaction.commit()

        transaction.set_autocommit(True)
        user = authenticate(username=user.username, password=self.password)
        login(self.request, user)

    def create_user_extranjero(self):
        transaction.set_autocommit(False)
        AlumnoGeneral.comprobar_edad(anio=self.fecha_nacimiento[0:4])
        user = User.objects.create_user(username=self.username, password=self.password)
        aspirante = Aspirante()
        user.first_name = self.first_name
        user.last_name = self.last_name
        aspirante.fecha_nacimiento = self.fecha_nacimiento
        aspirante.sexo = self.sexo
        aspirante.entidad_nacimiento = self.entidad_nacimiento
        aspirante.pais_nacimiento = self.pais_nacimiento
        user.groups.add(Group.objects.get(name='alumno'))
        user.email = self.email
        user.save()

        aspirante.usuario = user
        aspirante.curp = self.curp
        aspirante.folio_ceneval = self.get_folio_ceneval()
        aspirante.save()
        transaction.commit()

        transaction.set_autocommit(True)
        user = authenticate(username=user.username, password=self.password)
        login(self.request, user)

    def email_exists(self, form):
        if User.objects.filter(email=self.email).exists():
            form.add_error("email", "El email ya existe")
            return form

    def username_exists(self, form):
        if User.objects.filter(username=self.username).exists():
            form.add_error("username", "El usuario ya existe intanto con otro")
            return form

    def check_password(self, form):
        if self.password != self.repassword:
            form.add_error("repassword", "Las contraseñas no coinciden")
        return form

    def get_folio_ceneval(self):
        last_folio = Aspirante.get_last_folio()
        return Folio.get_folio(last_folio)


class SignupMatricula(SignUpUtils, generic.View):
    template_name = "registro/con_matricula.html"
    form_class = SignupMatriculaForm

    def get(self, request):
        form = self.form_class(auto_id='%s')
        context = {'form': form}
        return render(request, self.template_name, context)

    def post(self, request):
        form = self.form_class(request.POST, auto_id='%s')

        self.matricula = request.POST.get('matricula')
        self.password = request.POST.get('password')
        self.repassword = request.POST.get('repassword')

        if len(self.matricula) < 10:
            form.add_error("matricula", "Tu matricula debe contener 10 caracteres")
        if self.password != self.repassword:
            form.add_error("repassword", "Las contraseñas no coinciden")

        if not form.is_valid():
            context = {'form': form}
            return render(request, self.template_name, context)

        self.curp = form.cleaned_data['curp']
        self.first_name = form.cleaned_data['first_name']
        self.last_name = form.cleaned_data['last_name']
        self.fecha_nacimiento = form.cleaned_data['fecha_nacimiento']
        self.sexo = form.cleaned_data['sexo']
        self.entidad_nacimiento = form.cleaned_data['entidad_nacimiento_select']
        self.email = form.cleaned_data['email']
        self.username = form.cleaned_data['username']

        try:
            self.create_user_estudio_en_qroo()
            return HttpResponseRedirect('/aspirantes/')
        except Exception as e:
            transaction.rollback()
            return render(request, self.template_name, {'form': form, 'errors': e})


class SignupCurp(SignUpUtils, generic.View):
    template_name = "registro/con_curp.html"
    form_class = SignupCurpForm

    def get(self, request):
        form = self.form_class(auto_id='%s')
        context = {'form': form}
        return render(request, self.template_name, context)

    def post(self, request):
        form = self.form_class(request.POST, auto_id='%s')

        self.password = request.POST.get('password')
        self.repassword = request.POST.get('repassword')

        if self.password != self.repassword:
            form.add_error("repassword", "Las contraseñas no coinciden")

        if not form.is_valid():
            context = {'form': form}
            return render(request, self.template_name, context)

        self.curp = form.cleaned_data['curp']
        self.first_name = form.cleaned_data['first_name']
        self.last_name = form.cleaned_data['last_name']
        self.fecha_nacimiento = form.cleaned_data['fecha_nacimiento']
        self.sexo = form.cleaned_data['sexo']
        self.entidad_nacimiento = form.cleaned_data['entidad_nacimiento']
        self.email = form.cleaned_data['email']
        self.username = form.cleaned_data['username']

        try:
            self.create_user_con_curp()
            return HttpResponseRedirect('/aspirantes/')
        except Exception as e:
            transaction.rollback()
            return render(request, self.template_name, {'form': form, 'errors': e})


class SignupExtranjero(SignUpUtils, generic.View):
    template_name = "registro/con_extranjero.html",
    form_class = SignupExtranjeroForm

    def get(self, request):
        context = {'form': self.form_class(auto_id='%s')}
        return render(request, self.template_name, context)

    def post(self, request):
        form = self.form_class(request.POST, auto_id='%s')

        self.password = request.POST.get('password')
        self.repassword = request.POST.get('repassword')
        self.username = request.POST.get('username')
        self.email = request.POST.get('email')

        self.check_password(form)
        # self.email_exists(form)
        self.username_exists(form)

        if not form.is_valid():
            context = {'form': form}
            return render(request, self.template_name, context)

        self.first_name = form.cleaned_data['first_name']
        self.last_name = form.cleaned_data['last_name']
        self.fecha_nacimiento = form.cleaned_data['fecha_nacimiento']
        self.sexo = form.cleaned_data['sexo']
        self.pais_nacimiento = form.cleaned_data['pais_nacimiento']

        try:
            self.create_user_extranjero()
            return HttpResponseRedirect('/aspirantes/')
        except Exception as e:
            transaction.rollback()
            return render(request, self.template_name, {'form': form, 'errors': e})


class ConsultarCurpRenapo(generic.View):
    def get(self, request):
        try:
            if 'curp' not in request.GET:
                return HttpResponseBadRequest("la url es inválida")
            curp = request.GET.get('curp')
            url = "%s%s" % (RENAPO_URL, curp)
            r = requests.post(url, data={'txtCurp': curp})
            return JsonResponse(r.json())
        except Exception as e:
            return JsonResponse({'is_valid': False, 'msg': e.message}, status=500)
