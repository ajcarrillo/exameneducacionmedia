# -*- encoding: utf-8 -*
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.db import IntegrityError, transaction
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic

from app.aspirantes.forms import ApplicantProfileform
from app.aspirantes.magic_numbers import GENERO, COUNTRIES, ENTIDAD_NACIMIENTO
from app.aspirantes.views.dashboard import DashboardUtils
from general.mixins import LoginRequiredMixin, GroupRequiredMixin


class ProfileView(LoginRequiredMixin, GroupRequiredMixin, DashboardUtils, generic.View):
    template_name = 'profile/profile.html'
    group_required = 'alumno'

    def get(self, request):
        context = {
            'aspirante': self.aspirante,
        }
        return render(request, self.template_name, context)


class UpdateBasicInfo(LoginRequiredMixin, GroupRequiredMixin, DashboardUtils, generic.View):
    group_required = 'alumno'
    template_name = 'profile/datos_generales/basic_info.html'
    fields = ('first_name', 'last_name', 'password', 'email', 'telefono', 'fecha_nacimiento', 'entidad_nacimiento',
              'pais_nacimiento', 'sexo',)

    def get(self, request):
        context = {
            'aspirante': self.aspirante,
            'usr':       request.user,
            'sexo':      GENERO,
            'paises':    COUNTRIES,
            'entidades': ENTIDAD_NACIMIENTO
        }
        return render(request, self.template_name, context)

    def post(self, request):
        for field in request.POST:
            if field in field:
                try:
                    setattr(self.aspirante, field, request.POST[field])
                    setattr(request.user, field, request.POST[field])
                    self.aspirante.save()
                    request.user.save()
                except Exception as e:
                    print(e.message)
        return redirect(request.META.get('HTTP_REFERER'))


class UpdateApplicantEmail(LoginRequiredMixin, GroupRequiredMixin, DashboardUtils, generic.View):
    is_valid = False
    msg = 'El email que proporcionaste no es válido'
    group_required = 'alumno'

    @staticmethod
    def validate_email_address(email):
        try:
            validate_email(email)
            return True
        except ValidationError:
            return False

    def post(self, request):
        email = request.POST['email']
        if self.validate_email_address(email) is True:
            request.user.email = email
            request.user.save()
            self.is_valid = True
            self.msg = "Tu email se actualizó correctamente"
        return JsonResponse({'is_valid': self.is_valid, 'msg': self.msg})


class UpdateApplicantPassword(LoginRequiredMixin, GroupRequiredMixin, DashboardUtils, generic.View):
    group_required = 'alumno'

    def post(self, request):
        user = request.user
        old_password = request.POST['old_password'].strip()
        new_password = request.POST['new_password'].strip()
        confirm_password = request.POST['confirm_password'].strip()

        if new_password != confirm_password:
            messages.error(request, 'Las nuevas contraseñas no coinciden')
            return redirect(request.META.get('HTTP_REFERER'))

        if user.check_password(old_password):
            user.set_password(new_password)
            user.save()
            messages.success(request, 'Tu contraseña ha sido restaurada')
        else:
            messages.info(request, 'Introduce correctamente tu contraseña anterior')
        return redirect(request.META.get('HTTP_REFERER'))


class ApplicantGeneralData(LoginRequiredMixin, GroupRequiredMixin, DashboardUtils, generic.View):
    form_class = ApplicantProfileform
    template_name = 'profile/datos_generales/general.html'
    group_required = 'alumno'

    def is_foreign(self):
        if self.aspirante.matricula and self.aspirante.curp:
            return False
        return True

    def get(self, request):
        if not self.is_foreign():
            form = self.form_class(initial={'phone': self.aspirante.telefono}, auto_id='%s')
        else:
            form = self.form_class(is_foreign=True, initial={
                'first_name':       request.user.first_name,
                'last_name':        request.user.last_name,
                'fecha_nacimiento': self.aspirante.fecha_nacimiento,
                'sexo':             self.aspirante.sexo,
                'country':          self.aspirante.pais_nacimiento,
                'phone':            self.aspirante.telefono
            }, auto_id='%s')

        context = {
            'form': form
        }
        return render(request, self.template_name, context)

    def post(self, request):
        if not self.is_foreign():
            self.aspirante.telefono = request.POST['phone']
            self.aspirante.save()
            messages.success(request, 'Tu información se actualizó correctamente')
        else:
            user = request.user
            aspirante = self.aspirante
            transaction.set_autocommit(False)
            try:
                user.first_name = request.POST['first_name']
                user.last_name = request.POST['last_name']
                aspirante.fecha_nacimiento = request.POST['fecha_nacimiento']
                aspirante.sexo = request.POST['sexo']
                aspirante.pais_nacimiento = request.POST['country']
                aspirante.telefono = request.POST['phone']
                user.save()
                aspirante.save()
                transaction.commit()
                messages.success(request, 'Tu información se actualizó correctamente')
            except IntegrityError as e:
                transaction.rollback()
                messages.error(request, 'Tu información no ha sido actualizada')
        return redirect(reverse('aspirantes:profile'))

