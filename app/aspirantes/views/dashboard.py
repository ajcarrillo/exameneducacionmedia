# -*- encoding: utf-8 -*
from __future__ import division

import json
import datetime
from math import floor

import requests
from django.contrib import messages
from django.db import transaction, IntegrityError
from django.db.models import Max
from django.http import HttpResponse

from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic

from app.administracion.models import RevisionRegistro, Revision
from app.aspirantes.models import SeleccionOfertaEducativa, Aspirante, PaseExamen
from app.planteles.models import AulaPlantel, SedeAlterna, AulaSedeAlterna
from exameneducacionmedia.settings import BILLY_SERVICE_URL
from general.libseq.reporting import PentahoReport
from general.mixins import LoginRequiredMixin, AsegurarEtapaMixin, GroupRequiredMixin


class DashboardUtils(object):
    aspirante = None
    porcentaje = None

    def dispatch(self, request, *args, **kwargs):
        self.aspirante = Aspirante.objects.select_related('domicilio', 'domicilio__localidad', 'domicilio__localidad__municipio', 'informacion_de_procedencia_related').prefetch_related(
            'cuestionario_related').get(usuario=request.user)
        preguntas_respondidas = self.aspirante.cuestionario_related.filter(respuesta__isnull=False).count()
        total = self.aspirante.cuestionario_related.count()
        self.porcentaje = 0 if total == 0 else int(floor((preguntas_respondidas / total) * 100))
        return super(DashboardUtils, self).dispatch(request, *args, **kwargs)

    def consultar_pago(self, revision):
        if not revision:
            return None
        if not revision.pago_efectuado and revision.estado == Revision.ESTADO_EN_REVISION:
            url = '{0}/solicitud-pago/{1}'.format(BILLY_SERVICE_URL, revision.solicitud_pago)
            try:
                solicitud_pago = requests.get(url).text
                solicitud_pago = json.loads(solicitud_pago)
                if solicitud_pago['deposito'] is not None:
                    revision.pago_efectuado = True
                    revision.save()
                    return solicitud_pago['deposito']
            except:
                return None
        elif revision.pago_efectuado:
            return revision.pago_efectuado
        return None

    def asignar_pase_examen(self):
        primera_opcion = SeleccionOfertaEducativa.objects.get(aspirante=self.aspirante, preferencia=1)
        plantel_asignado = primera_opcion.oferta_educativa.plantel
        aulas_plantel = AulaPlantel.objects.filter(plantel=plantel_asignado)
        aspirante_fue_asignado = self.distribuir_aspirante_examen(aulas_plantel)
        if not aspirante_fue_asignado:
            sedes_alternas = SedeAlterna.objects.filter(plantel=plantel_asignado)
            if sedes_alternas.exists():
                for sede in sedes_alternas:
                    aulas_sede_alterna = AulaSedeAlterna.objects.filter(sede_alterna=sede)
                    if self.distribuir_aspirante_examen(aulas_sede_alterna):
                        aspirante_fue_asignado = True
                        break
        return aspirante_fue_asignado

    def distribuir_aspirante_examen(self, aulas):
        aspirante_fue_asignado = False
        for aula in aulas:
            distribucion_aspirantes = PaseExamen.objects.filter(aula=aula)
            lugares_ocupados_aula = distribucion_aspirantes.count()
            # obtención del máximo número de lista
            if not distribucion_aspirantes.exists():
                maximo_numero_lista = 0
            else:
                maximo_numero_lista = distribucion_aspirantes.aggregate(Max('numero_lista'))
                maximo_numero_lista = maximo_numero_lista['numero_lista__max']
            # ¿hay lugares liberados en el aula? (pases de examen que han sido eliminados)
            lugares_ocupados_aula = distribucion_aspirantes.count()
            lugares_liberados = []
            if lugares_ocupados_aula != maximo_numero_lista:
                lugares_liberados = list(set(range(1, maximo_numero_lista + 1)) - set(distribucion_aspirantes.values_list('numero_lista', flat=True)))
            # aula completamente llena
            if maximo_numero_lista == aula.capacidad and len(lugares_liberados) == 0:
                continue
            # lugares desocupados para este alumno
            lugares_desocupados = lugares_liberados + range(maximo_numero_lista + 1, aula.capacidad + 1)
            for l in lugares_desocupados:
                try:
                    pase_examen = PaseExamen()
                    pase_examen.aspirante = self.aspirante
                    pase_examen.aula = aula
                    pase_examen.numero_lista = l
                    pase_examen.save()
                    aspirante_fue_asignado = True
                    print 'aula {0}, lista {1}'.format(aula.id, pase_examen.numero_lista)
                    break
                except IntegrityError:
                    continue
            if aspirante_fue_asignado:
                break
        return aspirante_fue_asignado

    def get_ficha_deposito(self):
        revision = RevisionRegistro.revision_activa(self.aspirante)
        if revision.pago_efectuado:
            raise Exception('¡Tu pago ya ha sido procesado!')
        response_ficha_deposito = requests.get('%s/fichas-deposito/%s' % (BILLY_SERVICE_URL, revision.solicitud_pago))
        http_response = HttpResponse(response_ficha_deposito.content)
        http_response['content-type'] = response_ficha_deposito.headers['content-type']
        http_response['content-disposition'] = response_ficha_deposito.headers['content-disposition']
        return http_response


class DashboardView(LoginRequiredMixin, GroupRequiredMixin, DashboardUtils, generic.View):
    group_required = 'alumno'

    def get(self, request):
        if hasattr(self.aspirante, 'revision_aspirante_related'):
            revision = self.aspirante.revision_aspirante_related
            pago = self.consultar_pago(revision)
        else:
            pago = None
            revision = None

        context = {
            'pago':                     pago,
            'revision':                 revision,
            'aspirante':                self.aspirante,
            'porcentaje':               self.porcentaje,
            'has_informacion_completa': self._check_informacion_completa(),
        }
        return render(request, "dashboard_aspirantes.html", context)

    def _check_informacion_completa(self):
        if not self.aspirante.has_domicilio() or not self.aspirante.has_informacion_procedencia() or not self.aspirante.has_ceneval_survey_complete():
            return False
        return True


class EnviarRegistro(LoginRequiredMixin, GroupRequiredMixin, DashboardUtils, AsegurarEtapaMixin, generic.View):
    asegurar_etapa_de = "registro"
    group_required = 'alumno'

    def get(self, request):
        try:
            aspirante = self.aspirante
            if not aspirante.seleccion_oferta_educativa.exists():
                raise Exception("No has seleccionado tus opciones educativas")
            # genera la solicitud de pago para esta envío
            localidad_aspirante = self.aspirante.domicilio.localidad
            etapa_registro = self.get_etapa()

            solicitud_pago = {
                'concepto':      '026',
                'cantidad':      '1',
                'vigencia':      str(etapa_registro.cierre),
                'contribuyente': {
                    'nombre_completo': aspirante.usuario.get_full_name(),
                    'curp':            aspirante.curp,
                    'municipio':       localidad_aspirante.municipio.clave[1:3] if localidad_aspirante.municipio.entidad_federativa.clave == '23' else '00',
                    'localidad':       localidad_aspirante.nombre,
                    'colonia':         aspirante.domicilio.colonia,
                    'calle':           aspirante.domicilio.calle,
                    'numero':          aspirante.domicilio.numero
                }
            }

            primera_opcion = SeleccionOfertaEducativa.objects.get(aspirante=aspirante, preferencia=1)
            solicitud_pago['descuento'] = primera_opcion.oferta_educativa.plantel.descuento

            try:
                post_response = requests.post('%s/solicitud-pago/' % BILLY_SERVICE_URL, json.dumps(solicitud_pago))
            except:
                raise Exception('Por el momento no se puede generar tu ficha de depósito')

            if post_response.status_code != 201:
                if post_response.status_code == 400:
                    raise Exception(post_response.text)
                else:
                    raise Exception('Por el momento no se puede generar tu ficha de depósito')
                pass

            header_location = post_response.headers['Location']
            solicitud_creada = requests.get(BILLY_SERVICE_URL + header_location).text
            solicitud_creada = json.loads(solicitud_creada)

            revision = RevisionRegistro()
            revision.usuario_apertura = aspirante.usuario
            revision.fecha_apertura = datetime.date.today()
            revision.aspirante = aspirante
            revision.pago_efectuado = False
            revision.solicitud_pago = solicitud_creada['id']
            revision.save()
            messages.info(request, "Tu ficha se generó correctamente")
        except Exception as e:
            messages.info(request, e.message)
        return redirect(reverse('aspirantes:dashboard'))


class FichaDeposito(LoginRequiredMixin, GroupRequiredMixin, DashboardUtils, AsegurarEtapaMixin, generic.View):
    asegurar_etapa_de = 'registro'
    group_required = 'alumno'

    def get(self, request):
        try:
            return self.get_ficha_deposito()
        except Exception as e:
            messages.info(request, e.message)
            return redirect(request.META.get('HTTP_REFERER'))


# GENERAR PASE AL EXÁMEN
class PaseAlExamen(LoginRequiredMixin, GroupRequiredMixin, DashboardUtils, AsegurarEtapaMixin, generic.View):
    asegurar_etapa_de = 'registro'
    group_required = 'alumno'

    def get(self, request):
        try:
            revision = RevisionRegistro.revision_activa(self.aspirante)
            if revision.estado == Revision.ESTADO_ACEPTADO:
                raise Exception('Tu registro ya ha sido terminado con éxito')
            if revision.estado != Revision.ESTADO_EN_REVISION or not revision.pago_efectuado:
                raise Exception('Aun no puedes generar tu pase al examen')

            if self.asignar_pase_examen():
                revision.estado = Revision.ESTADO_ACEPTADO
                revision.fecha_revision = datetime.date.today()
                revision.save()
            else:
                raise Exception('Todas las aulas están ocupadas, reporta esto a la Dirección de Educación Media, Superior y Capacitación para el Trabajo, al teléfono (983) 835 0770, extensión 5006')
        except Exception as e:
            messages.info(request, e.message)
        return redirect(reverse('aspirantes:dashboard'))


class FichaDeRegistro(LoginRequiredMixin, GroupRequiredMixin, DashboardUtils, AsegurarEtapaMixin, generic.View):
    asegurar_etapa_de = 'registro'
    group_required = 'alumno'

    def get(self, request):
        try:
            revision = RevisionRegistro.revision_activa(self.aspirante)
            if not revision.estado == Revision.ESTADO_ACEPTADO:
                raise Exception('Aun no puedes generar tu ficha de registro')
            report = PentahoReport()
            report.name = 'ficha_registro'
            report.project = 'siem'
            report.set('id_aspirante', str(self.aspirante.id))
            print report
            response = HttpResponse(report.fetch())
            response['Content-Type'] = 'application/pdf'
            response['Content-Disposition'] = u'attachment; filename = "ficha de registro ({0}).pdf"'.format(request.user.username)
            return response
        except Exception as e:
            return HttpResponse(e, status=404)
