# -*- encoding: utf-8 -*
from django.contrib import messages
from django.db import transaction
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic

from app.aspirantes.models import SeleccionOfertaEducativa
from app.aspirantes.views.dashboard import DashboardUtils
from app.mgeo.models import Municipio
from app.planteles.models import Plantel, OfertaEducativa
from general.mixins import LoginRequiredMixin, AsegurarEtapaMixin, GroupRequiredMixin


class SeleccionOfertaEducativaView(LoginRequiredMixin, GroupRequiredMixin, DashboardUtils, AsegurarEtapaMixin, generic.View):
    template_name = "seleccion_oferta_educativa/start.html"
    model = SeleccionOfertaEducativa
    asegurar_etapa_de = "registro"
    group_required = 'alumno'

    def get(self, request):
        if hasattr(self.aspirante, 'revision_aspirante_related'):
            messages.info(request, 'Tu registro ha sido enviado, por lo tanto no puedes modificar tus opciones educativas')
            return redirect(reverse('aspirantes:mis_opciones'))
        context = {
            'municipios': Municipio.objects.filter(entidad_federativa=23).order_by('nombre'),
        }
        return render(request, self.template_name, context)

    def post(self, request):
        transaction.set_autocommit(False)
        ofertas = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        try:
            self.model.objects.filter(aspirante=self.aspirante).delete()
            for oferta in ofertas:
                if 'plantel_especialidad_{0}'.format(oferta) in request.POST:
                    oferta_n = request.POST.get('plantel_especialidad_{0}'.format(oferta))
                    seleccion = self.model()
                    seleccion.aspirante = self.aspirante
                    seleccion.oferta_educativa = OfertaEducativa.objects.get(pk=oferta_n)
                    seleccion.preferencia = oferta
                    seleccion.save()
            transaction.commit()
            data = {'is_valid': True}
        except Exception as e:
            transaction.rollback()
            data = {'is_valid': False, 'message': e.message}
        return JsonResponse(data, safe=False)


class MiSeleccionEducativaView(LoginRequiredMixin, GroupRequiredMixin, DashboardUtils, AsegurarEtapaMixin, generic.View):
    asegurar_etapa_de = 'registro'
    group_required = 'alumno'
    model = SeleccionOfertaEducativa
    template_name = 'seleccion_oferta_educativa/mis_opciones_educativas.html'

    def get(self, request):
        context = {
            'opciones': self.aspirante.seleccion_oferta_educativa.all()
        }
        return render(request, self.template_name, context)


def get_json_plantel_by(request):
    municipio = request.GET.get('municipio')
    planteles = Plantel.objects.filter(domicilio__localidad__municipio_id=municipio).order_by('referencia')
    data = [{'id': plantel.id, 'name': plantel.referencia} for plantel in planteles]
    return JsonResponse(data, safe=False)


def get_json_especialidad_by(request):
    plantel = request.GET.get('plantel')
    especialidades = OfertaEducativa.objects.select_related('especialidad').filter(plantel__id=plantel, activa=True).order_by('especialidad__referencia')
    data = [{'id': oferta.pk, 'name': oferta.especialidad.referencia.upper()} for oferta in especialidades]
    return JsonResponse(data, safe=False)


def get_json_aspirante_opciones_educativas(request):
    try:
        if not request.user.aspirante_related:
            raise Exception("No tiene permisos para realizar esta acción")

        seleccion = SeleccionOfertaEducativa.objects.filter(aspirante=request.user.aspirante_related)

        if not seleccion.exists():
            raise Exception("No se encontró selección previa")

        data = [{'is_valid': True, 'message': ''}, ]
        for opcion_educativa in seleccion:
            data.append({
                'municipio':   opcion_educativa.oferta_educativa.plantel.domicilio.localidad.municipio.pk,
                'preferencia': opcion_educativa.preferencia,
                'plantel':     opcion_educativa.oferta_educativa.plantel.id,
                'oferta':      opcion_educativa.oferta_educativa.id
            })
        return JsonResponse(data, safe=False)
    except Exception as e:
        message = e.message
        return JsonResponse([{'is_valid': False, 'message': message}], safe=False)
