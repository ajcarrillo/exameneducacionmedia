# -*- encoding: utf-8 -*
from django.contrib import messages
from django.db import transaction
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views import generic

from app.aspirantes.forms import AspiranteDomicilioForm
from app.aspirantes.views.dashboard import DashboardUtils
from app.mgeo.models import Domicilio
from general.mixins import LoginRequiredMixin, GroupRequiredMixin


class AjaxableResponseMixin(object):
    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse(form.labeled_errors(), status=400)
        else:
            context = {'error': form.labeled_errors(), 'regresar_a': self.request.META.get('HTTP_REFERER')}
            return render(self.request, 'errors.html', context)

    def form_valid(self, form):
        if self.request.is_ajax():
            data = {
                'is_valid': True,
            }
            return JsonResponse(data)
        else:
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))


class DomicilioUtils(DashboardUtils, generic.View):
    form_class = AspiranteDomicilioForm
    template_name = "profile/domicilio/address.html"

    def get(self, request):
        if not self.aspirante.domicilio:
            domicilio_form = self.form_class(auto_id='%s')
        else:
            domicilio_form = self.form_class(auto_id='%s', initial={
                'calle':         self.aspirante.domicilio.calle,
                'numero':        self.aspirante.domicilio.numero,
                'colonia':       self.aspirante.domicilio.colonia,
                'telefono':      self.aspirante.telefono,
                'municipio':     self.aspirante.domicilio.localidad.municipio,
                'localidad':     self.aspirante.domicilio.localidad,
                'codigo_postal': self.aspirante.domicilio.codigo_postal,
            })
        context = {
            'aspirante':             self.aspirante,
            'domicilio_form':        domicilio_form,
        }
        return render(request, self.template_name, context)

    def save(self, form, domicilio, aspirante):
        self.form_valid(form, aspirante)
        try:
            domicilio.localidad = form.cleaned_data['localidad']
            domicilio.colonia = form.cleaned_data['colonia']
            domicilio.calle = form.cleaned_data['calle']
            domicilio.numero = form.cleaned_data['numero']
            domicilio.codigo_postal = form.cleaned_data['codigo_postal']
            domicilio.save()
            aspirante.domicilio = domicilio
            aspirante.telefono = form.cleaned_data['telefono']
            aspirante.save()
        except Exception as e:
            raise Exception("Error al guardar los cambios")

    def form_valid(self, form, aspirante):
        if not form.is_valid():
            self.form_invalid(form)

    def form_invalid(self, form):
        raise Exception(form.errors)


class AspiranteDomicilioView(LoginRequiredMixin, GroupRequiredMixin, DomicilioUtils):
    group_required = 'alumno'

    def post(self, request):
        domicilio = Domicilio()
        form = self.form_class(request.POST, auto_id='%s')
        transaction.set_autocommit(False)
        try:
            self.save(form, domicilio, self.aspirante)
            transaction.commit()
            messages.success(self.request, 'El domicilio se guardó correctamente')
        except Exception as e:
            transaction.rollback()
            messages.error(self.request, e.message)
        return redirect(request.META.get('HTTP_REFERER'))


class AspiranteDomicilioUpdateView(LoginRequiredMixin, GroupRequiredMixin, DomicilioUtils):
    group_required = 'alumno'

    def post(self, request):
        domicilio = self.aspirante.domicilio
        form = self.form_class(request.POST, auto_id='%s')
        transaction.set_autocommit(False)
        try:
            self.save(form, domicilio, self.aspirante)
            transaction.commit()
            messages.success(self.request, 'El domicilio se guardó correctamente')
        except Exception as e:
            transaction.rollback()
            messages.error(self.request, e.message)
        return redirect(request.META.get('HTTP_REFERER'))
