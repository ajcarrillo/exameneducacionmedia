# -*- encoding: utf-8 -*
from django.contrib import messages
from django.shortcuts import redirect, render
from django.views import generic

from app.aspirantes.forms import AspiranteInformacionProcedenciaModelForm
from app.aspirantes.models import InformacionProcedencia
from app.aspirantes.views.dashboard import DashboardUtils
from general.mixins import LoginRequiredMixin, GroupRequiredMixin


class InformacionProcedenciaCreateView(LoginRequiredMixin, GroupRequiredMixin, DashboardUtils, generic.View):
    form_class = AspiranteInformacionProcedenciaModelForm
    model = InformacionProcedencia
    group_required = 'alumno'
    template_name = 'profile/informacion_procedencia/information_of_origin.html'

    def save(self, form):
        try:
            form.save()
            messages.success(self.request, 'Los datos se guardaron correctamente')
        except Exception as e:
            messages.add_message(self.request, messages.ERROR, e.message)
        return form

    def form_valid(self, form):
        if not form.is_valid():
            messages.add_message(self.request, messages.SUCCESS, form.errors)
            return redirect(self.request.META.get('HTTP_REFERER'))

    def get(self, request):
        if self.aspirante.has_informacion_procedencia():
            procedencia_info_form = self.form_class(instance=self.aspirante.informacion_de_procedencia_related, auto_id='%s')
        else:
            procedencia_info_form = self.form_class(auto_id='%s', initial={'aspirante': self.aspirante.pk})
        context = {
            'aspirante':             self.aspirante,
            'procedencia_info_form': procedencia_info_form,
        }
        return render(request, self.template_name, context)

    def post(self, request):
        if self.aspirante.has_informacion_procedencia():
            form = self.form_class(request.POST, instance=self.aspirante.informacion_de_procedencia_related, auto_id='%s')
        else:
            form = self.form_class(request.POST, auto_id='%s')
        self.form_valid(form)
        self.save(form)

        return redirect(request.META.get('HTTP_REFERER'))
