# -*- encoding: utf-8 -*
from __future__ import unicode_literals

import re

import datetime
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q, Max

from app.aspirantes.magic_numbers import SI_NO, GENERO, ENTIDAD_NACIMIENTO, COUNTRIES
from app.billy.models import SolicitudPago
from app.mgeo.models import Domicilio, Localidad
from general.magicnumbers import GENDER_CHOICES


class CenevalDiccionario(models.Model):
    nombre = models.CharField(max_length=140, unique=True)

    def __unicode__(self):
        return '%s' % self.nombre

    class Meta:
        db_table = 'ceneval_diccionarios'


class CenevalRespuesta(models.Model):
    diccionario = models.ForeignKey(CenevalDiccionario, related_name='respuestas_related')
    valor = models.CharField(max_length=2)
    etiqueta = models.CharField(max_length=250)

    def __unicode__(self):
        return '%s' % self.etiqueta

    class Meta:
        db_table = 'ceneval_respuestas'


class CenevalPregunta(models.Model):
    nombre = models.CharField(max_length=1024)
    variable = models.CharField(max_length=20, null=True)
    diccionario = models.ForeignKey(CenevalDiccionario, related_name='diccionarios_related', null=True)
    padre = models.ForeignKey('self', on_delete=models.CASCADE, related_name='children_related', null=True)

    def __unicode__(self):
        return '%s' % self.nombre

    class Meta:
        db_table = 'ceneval_preguntas'
        unique_together = ('variable', 'diccionario')


class Aspirante(models.Model):
    usuario = models.OneToOneField(User, db_column='usuario', on_delete=models.DO_NOTHING, related_name='aspirante_related')
    curp = models.CharField(db_column='curp', max_length=18, null=True)
    control_escolar_id = models.IntegerField(db_column='control_escolar_id', unique=True, null=True)
    domicilio = models.OneToOneField(Domicilio, db_column='domicilio', on_delete=models.DO_NOTHING, null=True)
    telefono = models.CharField(db_column='telefono', max_length=15, null=True)
    fecha_nacimiento = models.DateField(db_column='fecha_nacimiento', null=True)
    entidad_nacimiento = models.CharField(db_column='entidad_nacimiento_id', max_length=2, choices=ENTIDAD_NACIMIENTO, null=True)
    pais_nacimiento = models.CharField(db_column='pais_nacimiento', max_length=2, choices=COUNTRIES, default='MX')
    sexo = models.CharField(db_column='sexo', max_length=1, choices=GENERO, null=True)
    folio_ceneval = models.PositiveIntegerField(db_column='folio_ceneval', null=True, unique=True)
    matricula = models.CharField(max_length=10, null=True)

    class Meta:
        db_table = 'aspirante'

    def __unicode__(self):
        return '%s' % self.usuario

    @staticmethod
    def get_last_folio():
        maximo = Aspirante.objects.all().aggregate(Max('folio_ceneval'))
        maximo = 0 if maximo['folio_ceneval__max'] is None else maximo['folio_ceneval__max']
        return maximo

    @property
    def folio(self):
        return self.obtener_folio()

    def obtener_folio(self):
        return str(self.folio_ceneval).zfill(9)

    def has_domicilio(self):
        if self.domicilio is None:
            return False
        return True

    def has_informacion_procedencia(self):
        if hasattr(self, 'informacion_de_procedencia_related') and self.informacion_de_procedencia_related is not None:
            return True
        return False

    def has_ceneval_survey_complete(self):
        if self.cuestionario_related.filter(respuesta_id=None).exists() or not self.cuestionario_related.exists():
            return False
        return True

    @staticmethod
    def get_aspirante(first_name=None, last_name=None, term=None, referencia_de_pago=None):
        if referencia_de_pago is not None:
            return Aspirante.get_aspirante_by_referencia_de_pago(referencia_de_pago)

        if (term is None or term is '') and (first_name is None or first_name is '') and (last_name is None or last_name is ''):
            return Aspirante.objects.none()

        if term is not None and term is not '':
            return Aspirante.objects.filter(Q(curp__icontains=term) |
                                            Q(usuario__username__icontains=term) |
                                            Q(usuario__email__icontains=term))
        criteria = Q()
        if first_name is not '' and first_name is not None:
            criteria.add(Q(usuario__first_name__icontains=first_name), Q.AND)
        if last_name is not '' and last_name is not None:
            criteria.add(Q(usuario__last_name__icontains=last_name), Q.AND)
        return Aspirante.objects.filter(criteria)

    @staticmethod
    def get_aspirante_by_referencia_de_pago(referencia_de_pago=None):
        if referencia_de_pago is None:
            return Aspirante.objects.none()

        try:
            solicitud_de_pago = SolicitudPago.objects.get(referencia_pago__referencia__exact=referencia_de_pago)
            try:
                revision = RevisionRegistro.objects.get(solicitud_pago=solicitud_de_pago.pk)
                return Aspirante.objects.filter(pk=revision.aspirante.pk)
            except RevisionRegistro.DoesNotExist:
                return Aspirante.objects.none()
        except SolicitudPago.DoesNotExist:
            return Aspirante.objects.none()


class CenevalCuestionarioDeContexto(models.Model):
    aspirante = models.ForeignKey(Aspirante, related_name='cuestionario_related')
    pregunta = models.ForeignKey(CenevalPregunta)
    respuesta = models.ForeignKey(CenevalRespuesta, null=True)

    class Meta:
        db_table = 'aspirante_cuestionario_de_contexto'
        unique_together = ('aspirante', 'pregunta')


class InformacionProcedencia(models.Model):
    aspirante = models.OneToOneField(Aspirante, related_name='informacion_de_procedencia_related')
    cct_secundaria = models.CharField(db_column='cct_escuela', max_length=10, null=True, blank=True)
    nombre_secundaria = models.CharField(db_column='nombre_escuela', max_length=255)
    fecha_egreso = models.DateField(db_column='fecha_egreso')
    primera_vez = models.CharField(db_column='primera_vez', max_length=1, choices=SI_NO)

    class Meta:
        db_table = 'informacion_procedencia'

    def validar(self):
        errores = []

        if not self.nombre_secundaria:
            errores.append('Nombre de tu secundaria')
        if self.primera_vez not in SI_NO:
            errores.append('¿Es la primera vez que participas?')
        if len(errores) > 0:
            raise Exception('Hay datos incompletos en tu información de procedencia: {0}.'.format('. '.join(errores)))


class SeleccionOfertaEducativa(models.Model):
    id = models.AutoField(db_column='id', primary_key=True)
    aspirante = models.ForeignKey(Aspirante, db_column='aspirante', on_delete=models.DO_NOTHING, related_name='seleccion_oferta_educativa')
    oferta_educativa = models.ForeignKey('planteles.OfertaEducativa', db_column='plantel_especialidad', on_delete=models.DO_NOTHING, related_name='seleccion_oferta_educativa_related')
    preferencia = models.IntegerField(db_column='preferencia')

    class Meta:
        db_table = 'seleccion_oferta_educativa'
        unique_together = (('aspirante', 'oferta_educativa'), ('aspirante', 'preferencia'))

    @staticmethod
    def validar_seleccion(aspirante):
        primera_opcion = SeleccionOfertaEducativa.objects.filter(aspirante=aspirante, preferencia=1)
        if not primera_opcion.exists():
            raise Exception('La primera opción educativa no ha sido establecida')
        primera_opcion = primera_opcion.get()
        # Obtenemos el municipio del plantel de la primera opción, si es Benito Juarez (1808),
        # el número de opciones siempre serán 10
        municipio = Localidad.objects.get(pk=primera_opcion.oferta_educativa.plantel.domicilio.localidad).municipio.id
        if municipio == 1808:
            numero_opciones = 10
        else:
            numero_opciones = primera_opcion.oferta_educativa.plantel.numero_opciones()
        seleccion_aspirante = SeleccionOfertaEducativa.objects.filter(aspirante=aspirante)
        if seleccion_aspirante.count() != numero_opciones:
            raise Exception('El plantel elegido como primera opción requiere {0} opciones de manera obligatoria'.format(numero_opciones))


class PaseExamen(models.Model):
    id = models.AutoField(db_column='id', primary_key=True)
    aula = models.ForeignKey('planteles.Aula', db_column='aula', on_delete=models.DO_NOTHING)
    aspirante = models.OneToOneField(Aspirante, db_column='aspirante', on_delete=models.DO_NOTHING)
    numero_lista = models.IntegerField(db_column='numero_lista')
    automatico = models.BooleanField(db_column='automatico', default=False)

    class Meta:
        db_table = 'pase_examen'
        unique_together = ('aula', 'numero_lista')


class AlumnoGeneral(models.Model):
    id_alumno = models.IntegerField(db_column='id_alumno', primary_key=True)
    matricula = models.CharField(db_column='matricula', max_length=10, verbose_name='Matricula')
    curp = models.CharField(db_column='curp', max_length=18)
    nombre = models.CharField(db_column='nombre_alumno', max_length=40)
    apellido_paterno = models.CharField(db_column='ap_paterno_alumno', max_length=40)
    apellido_materno = models.CharField(db_column='ap_materno_alumno', max_length=40)
    fecha_nacimiento = models.DateField(db_column='fecha_nacimiento_alumno')
    sexo = models.CharField(db_column='sexo_alumno', max_length=1, choices=GENDER_CHOICES)

    class Meta:
        db_table = 'siceeb_t_alumnos'
        managed = False

    def _first_name(self):
        return '%s' % self.nombre

    def _last_name(self):
        return '%s %s' % (self.apellido_paterno, self.apellido_materno)

    def _get_full_name(self):
        return '%s %s %s' % (self.nombre, self.apellido_paterno, self.apellido_materno)

    def _get_sexo(self):
        if self.sexo == "H":
            return 1
        return 2

    first_name = property(_first_name)
    last_name = property(_last_name)
    full_name = property(_get_full_name)
    genero = property(_get_sexo)

    @staticmethod
    def comprobar_edad(alumno=None, curp=None, anio=None):
        current_year = datetime.datetime.now().year
        edad_minima = 13
        edad_maxima = 25

        if alumno is not None:
            if alumno.fecha_nacimiento:
                anio = unicode(alumno.fecha_nacimiento)[0:4]

        if curp is not None:
            anio = curp[4:6]
            current_year_2 = unicode(current_year)[2:4]
            # Como la CURP nada mas nos ofrece 2 dígitos, veremos si son mayores a los dígitos de éste año. Si lo son,
            # asumimos que es un año de 19XX.
            if int(anio) > int(current_year_2):
                anio = 1900 + int(anio)
            else:
                anio = 2000 + int(anio)

        if current_year - int(anio) < int(edad_minima) or current_year - int(anio) > int(edad_maxima):
            raise Exception(
                'Lo sentimos, únicamente pueden participar las personas de %s a %s años de edad. Tu edad no participa en el programa.' % (
                    edad_minima, edad_maxima))

    @staticmethod
    def resolver_alumno(curp):
        registros = AlumnoGeneral.objects.filter(curp=curp)
        # si no hay ningún resultado el alumno no existe
        if registros.count() == 0:
            return None
        # cuando según nuestros criterios encontremos un candidato, será el resuelto
        alumno_resuelto = None
        # si en el camino encontramos posibles candidatos, serán posibles
        alumnos_posibles = []
        for alumno_general in registros:
            print('iterando registro de alumno general (%s)' % alumno_general.id_alumno)
            try:
                # si tiene datos en t_datos_alumno, mucho mejor para resolver
                for alumno in Alumno.objects.filter(id=alumno_general):
                    # si tiene detalles y está inscrito, :D
                    if alumno.estatus == 'I':
                        alumno_resuelto = alumno
                        break
                    else:
                        # recolectamos posibles alumnos con detalles pero sin estado I
                        alumnos_posibles.append(alumno)
                if alumno_resuelto is not None:
                    break
            except Exception as e:
                pass
        # si hubo posibles, y no hubo un resuelto devolvemos el primero de ellos
        if alumno_resuelto is None:
            if len(alumnos_posibles) > 0:
                alumno_resuelto = alumnos_posibles[0]
            else:
                alumno_resuelto = registros[0]

        if len(alumno_resuelto.matricula) != 10:
            return None
        return alumno_resuelto

    @staticmethod
    def obtener_alumno(curp=None, matricula=None):
        if curp is None and matricula is None:
            raise Exception("Por favor proporciona tu CURP o tu matricula")

        registros = None
        if curp:
            AlumnoGeneral.validar_curp(curp.strip().upper())
            registros = AlumnoGeneral.objects.filter(curp=curp.strip().upper())

        if matricula:
            registros = AlumnoGeneral.objects.filter(matricula=matricula.strip().upper())

        if registros.count() == 0:
            return None
        # cuando según nuestros criterios encontremos un candidato, será el resuelto
        alumno_resuelto = None
        # si en el camino encontramos posibles candidatos, serán posibles
        alumnos_posibles = []
        for alumno_general in registros:
            print('iterando registro de alumno general (%s)' % alumno_general.id_alumno)
            try:
                # si tiene datos en t_datos_alumno, mucho mejor para resolver
                for alumno in Alumno.objects.filter(id=alumno_general):
                    # si tiene detalles y está inscrito, :D
                    if alumno.estatus == 'I':
                        alumno_resuelto = alumno
                        break
                    else:
                        # recolectamos posibles alumnos con detalles pero sin estado I
                        alumnos_posibles.append(alumno)
                if alumno_resuelto is not None:
                    break
            except Exception as e:
                pass
        # si hubo posibles, y no hubo un resuelto devolvemos el primero de ellos
        if alumno_resuelto is None:
            if len(alumnos_posibles) > 0:
                alumno_resuelto = alumnos_posibles[0]
            else:
                alumno_resuelto = registros[0]

        if len(alumno_resuelto.matricula) != 10:
            return None
        return alumno_resuelto

    @staticmethod
    def validar_curp(curp):
        data = curp.strip().upper()
        regexp = re.compile(
            "^([A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9])*$")
        if data == '000000000000000000':
            True
        else:
            if regexp.match(data):
                True
            else:
                raise Exception('Tu CURP no tiene formato valido. Puedes consultar tu CURP en http://consultas.curp.gob.mx/')

    @staticmethod
    def get_alumno(name=None, first_name=None, last_name=None, curp=None, matricula=None):

        criteria = Q()
        if name is not '' and name is not None:
            criteria.add(Q(nombre=name.upper()), Q.AND)
        if first_name is not '' and first_name is not None:
            criteria.add(Q(apellido_paterno=first_name.upper()), Q.AND)
        if last_name is not '' and last_name is not None:
            criteria.add(Q(apellido_materno__contains=last_name.upper()), Q.AND)
        if curp is not '' and curp is not None:
            criteria.add(Q(curp=curp), Q.AND)
        if matricula is not '' and matricula is not None:
            criteria.add(Q(matricula=matricula), Q.AND)

        return AlumnoGeneral.objects.filter(criteria)


class Alumno(AlumnoGeneral):
    id = models.OneToOneField(AlumnoGeneral, db_column='id_alumno', primary_key=True, parent_link=True,
                              on_delete=models.DO_NOTHING)
    nivel = models.CharField(db_column='id_nivel', max_length=1)
    estatus = models.CharField(db_column='estatus', max_length=1)
    cct = models.CharField(db_column='cct', max_length=10, null=True)

    class Meta:
        db_table = 'siceeb_t_datos_alumno'
        managed = False

from app.administracion.models import Revision, RevisionRegistro
