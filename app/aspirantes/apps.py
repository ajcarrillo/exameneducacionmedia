from __future__ import unicode_literals

from django.apps import AppConfig


class AspirantesConfig(AppConfig):
    name = 'app.aspirantes'
