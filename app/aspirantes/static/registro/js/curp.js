"use strict";
/**
 * Created by andres on 26/09/16.
 */
var curp = curp || {};
curp     = {
	validateCurp: function (micurp) {
		var that     = this,
			reg      = "",
			digito   = "",
			intRegex = /^\d+$/;


		if (micurp.length == 18) {
			digito = curp.calculateCheckDigit(micurp);
			reg    = /[A-Z]{4}\d{6}[HM][A-Z]{2}[B-DF-HJ-NP-TV-Z]{3}[A-Z0-9][0-9]/;
			if (micurp.search(reg)) {
				bootbox.alert("La curp: " + micurp + " no es valida, verifiqué ");
				console.log("La curp no coincide con el patron");
				return false;
			}

			if (!(parseInt(digito) == parseInt(micurp.substring(17, 18)))) {
				bootbox.alert("La curp: " + micurp + " no es valida, el digito verificador debe de ser  (" + digito + ")");
				return false;
			}
			console.log("curp valida");
			return true;
		}
		else {
			switch (micurp.length) {
				case 10 :
					reg = /[A-Z]{4}\d{6}/;
					break;
				case 11 :
					reg = /[A-Z]{4}\d{6}[HM]/;
					break;
				case 12 :
					reg = /[A-Z]{4}\d{6}[HM][A-Z]/;
					break;
				case 13 :
					reg = /[A-Z]{4}\d{6}[HM][A-Z]{2}/;
					break;
				case 14 :
					reg = /[A-Z]{4}\d{6}[HM][A-Z]{2}[B-DF-HJ-NP-TV-Z]/;
					break;
				case 15 :
					reg = /[A-Z]{4}\d{6}[HM][A-Z]{2}[B-DF-HJ-NP-TV-Z]{2}/;
					break;
				case 16 :
					reg = /[A-Z]{4}\d{6}[HM][A-Z]{2}[B-DF-HJ-NP-TV-Z]{3}/;
					break;
				case 17 :
					reg = /[A-Z]{4}\d{6}[HM][A-Z]{2}[B-DF-HJ-NP-TV-Z]{3}[A-Z0-9]/;
					break;
			}
			if (micurp.search(reg)) {
				bootbox.alert("La curp: " + micurp + " no es valida, verifiqué ");
				return false;
			}
			console.log("La sintaxis de la curp es valida continuamos con la segunda validacion.");
			return true;
		}


	},

	calculateCheckDigit: function (micurp) {
		var that        = this,
			segRaiz     = micurp.substring(0, 17),
			chrCaracter = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ",
			intFactor   = new Array(17),
			lngSuma     = 0.0,
			lngDigito   = 0.0;

		for (var i = 0; i < 17; i++) {
			for (var j = 0; j < 37; j++) {
				if (segRaiz.substring(i, i + 1) == chrCaracter.substring(j, j + 1)) {
					intFactor[i] = j;
				}
			}
		}
		for (var k = 0; k < 17; k++) {
			lngSuma = lngSuma + ((intFactor[k]) * (18 - k));
		}
		lngDigito = (10 - (lngSuma % 10));
		if (lngDigito == 10) {
			lngDigito = 0;
		}
		return lngDigito;

	},

	getCurp: function (ccurp, url, callback) {
		try {
			$.getJSON(url + ccurp, "", function (resp) {
				if (resp.codigo == "200") {
					if (resp.CURP != ccurp) {
						bootbox.alert("Se detecto que la curp que ingresaste es Historica [" + ccurp + "] se utilizara la curp actualizada por RENAPO [" + resp.CURP + "]");
					}
					resp.entidad_nacimiento = curp.getEntidadId(resp.cveEntidadNac);
					resp.genero = curp.getGenero(resp.sexo);
					resp.fecha_nacimiento = resp.fechNac.split("/").reverse().join("-");

					return callback(resp);
				}
				if (resp.codigo == "404") {
					bootbox.alert("La curp no se encuentra registrada en la base de datos nacional de Renapo");
				}
				if (resp.codigo == "503") {
					bootbox.alert("El servicio no se encuentra disponible, inténtalo mas tarde");
				}
			}).fail(function (resp) {
				console.log(resp);
				return callback(resp)
			});

		} catch (e) {
			console.error(e);
		}
	},

	getGenero: function (sexo) {
		return sexo == "H" ? 1 : 2;

	},

	getEntidadId: function (cclave) {
		try {
			var entidad_id = "";
			$.each(entidades.ENTIDADES, function (i, item) {
				if (item.letras_curp == cclave) {
					entidad_id = item.id;
				}
			});
			return entidad_id;
		}
		catch (e) {
			console.error(e);
		}


	}

};

