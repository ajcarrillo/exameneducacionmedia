"use strict";
/**
 * Created by andres on 26/09/16.
 */

(function () {
	var $txtMatricula  = $('#txt-matricula'),
		$checkSiNo     = $('input[name="es_correcto"]'),
		$checkSiNoCurp = $('input[name="es_correcto_curp"]'),
		$txtCurp       = $('#txt-curp');

	$txtMatricula.on("keyup", onKeyUpMatricula);
	$checkSiNo.on("click", onClickCheckSiNo);
	$checkSiNoCurp.on("click", onClickCheckSiNoCurp);
	$txtCurp.on("keyup", onKeyUpCurp);

	function onKeyUpCurp() {
		this.value = this.value.toUpperCase();

		if ($(this).val().length == 18) {
			if (!curp.validateCurp($(this).val())) {
				$('#curp-validation').addClass('has-error');
			} else {
				curp.getCurp($(this).val());
				$('#curp-validation').removeClass('has-error');
				$('#curp-validation').addClass('has-success');
			}

		}
	}

	function onClickCheckSiNo() {
		var soy_yo = $(this).val();
		console.log(soy_yo);
		if (soy_yo == "1") {
			$('#soy-de-qroo-signup-section').removeClass('hidden');
		} else {
			$('#soy-de-qroo-signup-section').addClass('hidden');
		}
	}

	function onClickCheckSiNoCurp() {
		var soy_yo = $(this).val();
		console.log(soy_yo);
		if (soy_yo == "1") {
			$('#signup-curp-section').removeClass('hidden');
		} else {
			$('#signup-curp-section').addClass('hidden');
		}
	}

	function onKeyUpMatricula() {
		if ($(this).val().length == 10) {
			var $form = $(this).closest('form'),
				url   = $form.attr('action') + '?format=json';

			$.post(url, $form.serialize(), function (response) {
				$('[data-user="first-name"]').val(response.first_name);
				$('[data-user="primer-apellido"]').val(response.apellido_paterno);
				$('[data-user="segundo-apellido"]').val(response.apellido_materno);
				$('[data-user="curp"]').val(response.curp);
				$('[data-user="fecha-nacimiento"]').val(response.fecha_nacimiento);
				$('[data-user="last-name"]').val(response.last_name);
				$('[data-user="sexo"]').val(response.genero);
				$('[data-user="matricula"]').val(response.matricula);

				$("input[name='genero']").each(function () {
					var $this = $(this);
					if (this.value == response.sexo) {
						$this.prop('checked', true);
					}
				});
				$('#soy-de-qroo-form').removeClass('hidden')

			}).fail(function (response) {
				console.log(response.responseText);
			});

		}
	}
}());
