"use strict";
/**
 * Created by andres on 28/09/16.
 */

(function () {
	var $txtMatricula         = $('#matricula'),
		$overlay              = $('.overlay'),
		$generalesSection     = $('#generales-form'),
		$txtCurp              = $('#curp'),
		$lblInvalidCurp       = $('#lbl-invalid-curp'),
		$cbxEntidadNacimiento = $('#entidad_nacimiento_select'),
		$searchingCurp        = $('#search-curp'),
		$validCurp            = $('#valid-curp');

	$txtMatricula.on("keyup", onKeyUpMatricula);
	$txtCurp.on("keyup", onKeyUpCurp);

	function onKeyUpCurp() {
		this.value = this.value.toUpperCase();

		if ($(this).val().length == 18) {
			if (!curp.validateCurp($(this).val())) {
				$('#curp-validation').addClass('has-error');
			} else {
				var curp2 = $(this).val(),
					url   = $('[data-url="renapo"]').val();

				$searchingCurp.removeClass('hidden');
				$lblInvalidCurp.addClass('hidden');
				$validCurp.addClass('hidden');

				curp.getCurp(curp2, url, function (response) {
					console.log(response);
					if (response.status == 500) {
						bootbox.alert("El servicio no se encuentra disponible, intentalo más tarde");
						$searchingCurp.addClass('hidden');
						$lblInvalidCurp.removeClass('hidden');
						$validCurp.addClass('hidden');
					} else {
						$searchingCurp.addClass('hidden');
						$lblInvalidCurp.addClass('hidden');
						$validCurp.removeClass('hidden');
						$('#entidad_nacimiento').val(response.entidad_nacimiento);
						$cbxEntidadNacimiento.find('option').each(function () {
							var $this = $(this);
							console.log(response.entidad_nacimiento);
							if (this.value == response.entidad_nacimiento) {
								$this.prop('selected', true);
							}
						});
					}
				});
			}

		}
	}

	function onKeyUpMatricula() {
		if ($(this).val().length == 10) {
			var data = {'matricula': $(this).val()},
				url  = $('#search-matricula-url').val() + '?format=json';
			$overlay.removeClass('hidden');
			$.getJSON(url, data, function (response) {
				console.log(response);
				if (response.length == 0) {
					bootbox.alert("No se encontró el alumno");
					$overlay.addClass('hidden');
				} else {
					$overlay.addClass('hidden');

					$lblInvalidCurp.addClass('hidden');
					$txtCurp.attr('readonly', true);
					$cbxEntidadNacimiento.attr('readonly', false);

					$generalesSection.removeClass('hidden');
					$('#first_name').val(response[0].first_name);
					$('#apellido_paterno').val(response[0].apellido_paterno);
					$('#apellido_materno').val(response[0].apellido_materno);
					$('#fecha_nacimiento').val(response[0].fecha_nacimiento);
					$('#sexo').val(response[0].genero);
					$('#last_name').val(response[0].last_name);

					if (response[0].curp.length != 18) {
						$txtCurp.val('');
						$txtCurp.removeAttr('readonly');
						$cbxEntidadNacimiento.attr('readonly', true);
						$lblInvalidCurp.removeClass('hidden');
					} else {
						$txtCurp.val(response[0].curp)
					}


					$('#sexo_select').find('option').each(function () {
						var $this = $(this);
						if (this.value == response[0].genero) {
							$this.prop('selected', true);
						}
					});
				}

			}).fail(function (response) {
				console.log(response.responseText);
			});

		}
	}
}());
