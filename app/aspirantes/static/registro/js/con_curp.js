"use strict";
/**
 * Created by andres on 28/09/16.
 */

(function () {
	var $txtCurp   = $('#curp');

	$txtCurp.on("keyup", onKeyUpCurp);

	function onKeyUpCurp() {
		this.value = this.value.toUpperCase();

		if ($(this).val().length == 18) {
			if (!curp.validateCurp($(this).val())) {
				$('#curp-validation').addClass('has-error');
			} else {
				var curp2 = $(this).val(),
					url   = $('[data-url="renapo"]').val();

				$('#credenciales-section').addClass('hidden');
				$('#generales-section').addClass('hidden');
				$('.overlay').removeClass('hidden');

				curp.getCurp(curp2, url, function (response) {
					console.log(response);
					if (response.status == 500){
						bootbox.alert("El servicio no se encuentra disponible, intentalo más tarde");
						$('.overlay').addClass('hidden');
					}else{
						$('.overlay').addClass('hidden');
						$('#first_name').val(response.nombres);
						$('#apellido_paterno').val(response.apellido1);
						$('#apellido_materno').val(response.apellido2);
						$('#fecha_nacimiento').val(response.fecha_nacimiento);

						$('#sexo').val(response.genero);
						$('#last_name').val(response.apellido1 + " " + response.apellido2);
						$('#entidad_nacimiento').val(response.entidad_nacimiento);

						$('#sexo_select').find('option').each(function () {
							var $this = $(this);
							if (this.value == response.genero) {
								$this.prop('selected', true);
							}
						});

						$('#entidad_nacimiento_select').find('option').each(function () {
							var $this = $(this);
							if (this.value == response.entidad_nacimiento) {
								$this.prop('selected', true);
							}
						});

						$('#generales-section').removeClass('hidden');
					}


				});
			}

		}
	}
}());
