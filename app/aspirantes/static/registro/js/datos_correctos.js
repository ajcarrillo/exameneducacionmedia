"use strict";
/**
 * Created by andres on 31/01/17.
 */

(function () {
	var $credencialesSection = $('#credenciales-form'),
		$checkSiNo           = $('input[name="es_correcto"]'),
		$msg                 = $('#section-msg');

	$checkSiNo.on("click", onClickCheckSiNo);

	function onClickCheckSiNo() {
		var soy_yo = $(this).val();
		console.log(soy_yo);
		if (soy_yo == "1") {
			$credencialesSection.removeClass('hidden');
			$msg.addClass('hidden')
		} else {
			$credencialesSection.addClass('hidden');
			$msg.removeClass('hidden')
		}
	}
}());
