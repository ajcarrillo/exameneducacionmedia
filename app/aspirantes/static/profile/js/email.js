"use strict";
/**
 * Created by andres on 30/01/17.
 */

(function () {
	var $btnUpdateEmail =  $("[data-button='update-email']"),
		$email = $('#email'),
		currentEmail = $email.val();

	$btnUpdateEmail.on("click", onClickUpdateEmail);

	function onClickUpdateEmail() {
		var form = $('#email-form'),
			data = form.serialize(),
			url = form.attr('action'),
			$userEmail = $('#user-email');

		$.post(url, data, function (response) {
			if (response.is_valid){
				$.notify({
					message: response.msg
				},{
					type: "success"
				});
				$userEmail.text($email.val())
			}else{
				$email.val(currentEmail);
				$.notify({
					message: response.msg
				},{
					type: "danger"
				});
			}
		})
	}
}());
