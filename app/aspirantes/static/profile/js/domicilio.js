"use strict";
/**
 * Created by andres on 12/10/16.
 */

(function () {
	var $municipio        = $('#municipio').select2({placeholder: "Municipio"}),
		$localidad        = $('#localidad').select2({placeholder: "Localidad"}),
		$btnUpdateAddress = $('[data-button="update-address"]');

	$btnUpdateAddress.on("click", onClickBtnUpdateAddress);

	function onClickBtnUpdateAddress() {
		var $addressForm = $('.address-form'),
			$addressInfo = $('.address-info');
		$addressInfo.addClass('hidden');
		$addressForm.removeClass('hidden');
		$('.select2').css({"width": "100%"});
	}

	$municipio.on("select2:select", function (evt) {
		var url         = $('#get-json-localidades').data("url"),
			municipioId = $(this).val();

		$.getJSON(url + municipioId, '', function (json) {
			$localidad.empty();
			$localidad.select2({data: json})
		})
	});
}());
