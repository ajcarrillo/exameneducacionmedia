"use strict";
/**
 * Created by andres on 18/10/16.
 */

(function () {
	var urlSeleccionPrevia = $('[data-url="api_get_aspirante_opciones_educativas"]').val(),
		$cbxEspecialidad   = $('[data-field="plantel_especialidad"]'),
		$firstMunicipio    = $('[data-id="municipio_1"]'),
		$cbxMunicipio      = $('[data-field="municipio"]'),
		$tenOptions        = $('.ten-options'),
		$fiveOptions       = $('.five-options'),
		$threeOptions      = $('.three-options'),
		$cbxPlantel        = $('[data-field="plantel"]'),
		$form              = $('#options-form');

	$cbxPlantel.select2();
	$cbxMunicipio.select2();
	$cbxEspecialidad.select2();
	$('.select2').css({"width": "100%"});

	$.getJSON(urlSeleccionPrevia, "", function (response) {
		if (response[0].is_valid) {
			response.shift();
			if (response.length == 5) {
				$fiveOptions.removeClass('hidden');
				$tenOptions.addClass('hidden');
				$tenOptions.find('select').attr('disabled', true)
			} else if (response.length == 3) {
				$tenOptions.addClass('hidden');
				$fiveOptions.addClass('hidden');
				$threeOptions.removeClass('hidden');
				$tenOptions.find('select').attr('disabled', true);
				$fiveOptions.find('select').attr('disabled', true);
				$threeOptions.find('select').attr('disabled', false);
			}else {
				$fiveOptions.removeClass('hidden');
				$tenOptions.removeClass('hidden');
				$tenOptions.find('select').attr('disabled', false)
			}
			for (var i = 0; i < response.length; i++) {
				var opcion           = response[i];
				var urlPlanteles     = $('[data-url="api_get_planteles_by"]').val() + '?municipio=' + opcion['municipio'];
				var urlEspecialidad  = $('[data-url="api_get_especialidades_by"]').val() + '?plantel=' + opcion['plantel'];
				var $cbxPlantel      = $('[data-id="plantel_' + opcion["preferencia"] + '"]');
				var $cbxMunicipio    = $('[data-id="municipio_' + opcion["preferencia"] + '"]');
				var $cbxEspecialidad = $('[data-id="oferta_' + opcion["preferencia"] + '"]');
				$cbxMunicipio.val(opcion['municipio']);
				$cbxMunicipio.select2();

				fillPlanteles($cbxPlantel, $cbxEspecialidad, urlPlanteles, urlEspecialidad, opcion['plantel'], opcion['oferta'], fillEspecialidades)

			}
		} else {
			console.log("no valido")
		}
	});

	function fillPlanteles(cbxPlantel, cbxEspecialidad, urlPlanteles, urlEspecialidad, plantel_id, especialidad_id, callback) {
		$.getJSON(urlPlanteles, "", function (response) {
			_.each(response, function (plantel) {
				var option = new Option(plantel.name, plantel.id);
				cbxPlantel.append($(option));
			});
			cbxPlantel.val(plantel_id);
			cbxPlantel.select2();
			callback(cbxEspecialidad, urlEspecialidad, especialidad_id)
		});
	}

	function fillEspecialidades(cbxEspecialidad, urlEspecialidad, especialidad_id) {
		$.getJSON(urlEspecialidad, "", function (response) {
			_.each(response, function (especialidad) {
				var option = new Option(especialidad.name, especialidad.id);
				cbxEspecialidad.append($(option));
			});
			cbxEspecialidad.val(especialidad_id);
			cbxEspecialidad.select2();
		})
	}

	$firstMunicipio.on("change", onChangeFirstMunicipio);
	$cbxMunicipio.on("change", onChangeCbxMunicipio);
	$cbxPlantel.on("change", onChangeCbxPlantel);
	$form.on("submit", onSubmitForm);

	function onSubmitForm(e) {
		e.preventDefault();
		var data      = $(this).serialize(),
			url       = $(this).attr('action'),
			planteles = [],
			planteles_diferentes,
			count,
			selects;

		if (!$tenOptions.hasClass('hidden')) {
			selects = $form.find('[data-field="plantel"]');
		} else {
			selects = $('.five-options, #first-option').find('[data-field="plantel"]');
		}

		$.each(selects, function (index) {
			planteles.push($(this).val())
		});

		planteles_diferentes = _.groupBy(planteles);
		count                = Object.keys(planteles_diferentes).length;
		if (!$tenOptions.hasClass('hidden')) {
			if (!(count >= 4)) {
				bootbox.alert("Debes elegir por lo menos 4 planteles diferentes");
				return false;
			}
		} else if (!(count >= 3)) {
			bootbox.alert("Debes elegir por lo menos 3 planteles diferentes");
			return false;
		}

		$.post(url, data, function (response) {
			if (response.is_valid) {
				bootbox.confirm({
					message: "Tus opciones se guardaron correctamente, ¿Deseas seguir modificando tus opciones?",
					buttons: {
						confirm: {
							label: 'Si',
							className: 'btn-success'
						},
						cancel: {
							label: 'No',
							className: 'btn-default'
						}
					},
					callback: function (isOk) {
						if (!isOk) {
							window.location = "/aspirantes/"
						}
					}
				});
			} else {
				bootbox.alert("Lo sentimos algo ha salido mal, intenta de nuevo.");
				console.log(response.message)
			}
		});
	}

	function onChangeCbxPlantel() {
		var plantel = $(this).val(),
			url     = $('[data-url="api_get_especialidades_by"]').val() + '?plantel=' + plantel;

		var especialidad     = $(this).data('id').replace('plantel', 'oferta');
		var $cbxEspecialidad = $('[data-id="' + especialidad + '"]');
		$cbxEspecialidad.find('option').remove().end().append('<option value="">Especialidad</option>');

		$.getJSON(url, "", function (response) {
			_.each(response, function (especialidad) {
				var option = new Option(especialidad.name, especialidad.id);
				$cbxEspecialidad.append($(option));
			})
		})
	}

	function onChangeCbxMunicipio() {
		var municipio = $(this).val(),
			url       = $('[data-url="api_get_planteles_by"]').val() + '?municipio=' + municipio;

		var plantel     = $(this).data('id').replace('municipio', 'plantel');
		var $cbxPlantel = $('[data-id="' + plantel + '"]');
		$cbxPlantel.find('option').remove().end().append('<option value="">Plantel</option>');

		var especialidad     = $(this).data('id').replace('municipio', 'oferta');
		var $cbxEspecialidad = $('[data-id="' + especialidad + '"]');
		$cbxEspecialidad.find('option').remove().end().append('<option value="">Especialidad</option>');

		$.getJSON(url, "", function (response) {
			_.each(response, function (plantel) {
				var option = new Option(plantel.name, plantel.id);
				$cbxPlantel.append($(option));
			})
		})

	}

	// TODO: Missing add ID for Puerto Morelos
	function onChangeFirstMunicipio() {
		var municipio_id = $(this).val();
		if (municipio_id == 1808) {
			$fiveOptions.removeClass('hidden');
			$tenOptions.removeClass('hidden');
			$tenOptions.find('select').attr('disabled', false);
			$fiveOptions.find('select').attr('disabled', false);
		} else if (municipio_id == 1811 || municipio_id == 1804 || municipio_id == 1806 || municipio_id == 1812 || municipio_id == 1807) {
			$fiveOptions.removeClass('hidden');
			$tenOptions.addClass('hidden');
			$tenOptions.find('select').attr('disabled', true);
			$fiveOptions.find('select').attr('disabled', false);
		} else {
			$tenOptions.addClass('hidden');
			$fiveOptions.addClass('hidden');
			$threeOptions.removeClass('hidden');
			$tenOptions.find('select').attr('disabled', true);
			$fiveOptions.find('select').attr('disabled', true);
			$threeOptions.find('select').attr('disabled', false);
		}
	}
}());
