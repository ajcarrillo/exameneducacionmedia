# -*- encoding: utf-8 -*
import datetime
from django.contrib import auth
from django.contrib import messages
from django.contrib.auth import login
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.views import generic

from app.accounts.forms import LoginAuthenticationForm, UserPasswordReset
from app.accounts.templatetags.account_extras import has_group
from app.administracion.models import EtapaProceso
from general.mixins import LoginRequiredMixin


class LoginFormView(generic.FormView):
    form_class = LoginAuthenticationForm
    template_name = 'home.html'
    success_url = reverse_lazy('administracion:users')

    def get_context_data(self, **kwargs):
        context = super(LoginFormView, self).get_context_data(**kwargs)
        today = datetime.date.today()
        registro = EtapaProceso.objects.get(nombre__exact='REGISTRO')
        is_registro = True if registro.apertura <= today <= registro.cierre else False
        context.update({'is_registro': is_registro, 'registro': registro})
        return context

    def form_valid(self, form):
        if not form.cleaned_data['rememberme']:
            self.request.session.set_expiry(0)

        login(self.request, form.user_cache)

        if has_group(form.user_cache, "departamento"):
            return HttpResponseRedirect(reverse('administracion:dashboard'))

        if has_group(form.user_cache, "subsistema"):
            return HttpResponseRedirect(reverse('subsistemas:subsistemas_start'))

        if has_group(form.user_cache, "alumno"):
            return HttpResponseRedirect(reverse('aspirantes:dashboard'))

        if has_group(form.user_cache, "plantel"):
            return HttpResponseRedirect(reverse('planteles:home'))

        return super(LoginFormView, self).form_valid(form)

    def form_invalid(self, form):
        return super(LoginFormView, self).form_invalid(form)


class ChangePasswordUser(LoginRequiredMixin, generic.FormView):
    template_name = 'change_password.html'
    form_class = UserPasswordReset

    def get_success_url(self):
        user = self.request.user
        if has_group(user, "departamento"):
            return HttpResponseRedirect(reverse('administracion:dashboard'))

        if has_group(user, "subsistema"):
            return HttpResponseRedirect(reverse('subsistemas:subsistemas_start'))

        if has_group(user, "plantel"):
            return HttpResponseRedirect(reverse('planteles:home'))

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        user = request.user
        try:
            if form.is_valid():
                old_password = form.cleaned_data['old_password']
                password = form.cleaned_data['password']
                if user.check_password(old_password):
                    user.set_password(password)
                    user.save()
                    messages.success(request, 'Tu contraseña ha sido restaurada')
                else:
                    messages.info(request, 'Introduce correctamente tu contraseña anterior')
                return redirect(request.META.get('HTTP_REFERER'))
        except Exception as e:
            print (e)
        return self.form_invalid(form)


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('home:home'))


class LoginRedirectView(generic.RedirectView):
    pattern_name = 'home:home'


class LogoutRedirectView(generic.RedirectView):
    pattern_name = 'accounts:logout'


def handler500(request):
    return render(request, '500.html', {})


def handler404(request):
    return render(request, '404.html', {})
