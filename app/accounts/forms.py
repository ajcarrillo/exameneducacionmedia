# -*- encoding: utf-8 -*
from django.contrib.auth.forms import AuthenticationForm
from django import forms


class LoginAuthenticationForm(AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control input-lg', 'placeholder': 'Usuario'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control input-lg', 'placeholder': 'Contraseña'}))
    rememberme = forms.BooleanField(required=False, widget=forms.CheckboxInput(), label="Recordar")


class NewUserForm(forms.Form):
    first_name = forms.CharField(label='Nombre', widget=forms.TextInput(
        attrs={'class': 'form-control', 'required': 'required', 'autofocus': 'autofocus'}))
    last_name = forms.CharField(label='Apellidos', widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(label='Email',
                             widget=forms.EmailInput(attrs={'class': 'form-control', 'required': 'required'}))
    username = forms.CharField(label='Usuario',
                               widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}))


class UserPasswordForm(forms.Form):
    password = forms.CharField(label='Contraseña', widget=forms.PasswordInput(attrs={'class': 'form-control', 'required': 'required'}))


class UserPasswordReset(UserPasswordForm):
    error_messages = {
        'password_mismatch': "Las dos contraseñas no coinciden",
    }
    old_password = forms.CharField(label='Contraseña anterior', widget=forms.PasswordInput(attrs={'class': 'form-control', 'required': 'required'}))
    confirm_password = forms.CharField(label='Confirmar contraseña', widget=forms.PasswordInput(attrs={'class': 'form-control', 'required': 'required'}))

    def clean_confirm_password(self):
        password_1 = self.cleaned_data.get('password')
        password_2 = self.cleaned_data.get('confirm_password')
        if password_1 and password_2:
            if password_1 != password_2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        return password_2
