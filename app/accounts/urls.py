# -*- encoding: utf-8 -*
from django.conf.urls import url

from app.accounts import views

# **************************************
#
#   url: /accounts/
#   namespace: accounts
#
# **************************************

urlpatterns = [
	url(r'^login/$', views.LoginFormView.as_view(), name='login'),
	url(r'^logout/$', views.logout, name='logout'),
	url(r'^change-password/$', views.ChangePasswordUser.as_view(), name='change_password'),
]
