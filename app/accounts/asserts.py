# -*- encoding: utf-8 -*
from app.aspirantes.models import Aspirante


def user_module_subsistema(user):
    for group in user.groups.all():
        if group.name == 'subsistema':
            return True
    return False


def user_module_registro(user):
    for group in user.groups.all():
        if group.name == 'alumno':
            return True
    return False


def user_module_aforo(user):
    for group in user.groups.all():
        if group.name == 'subsistema':
            return True
    return False


def user_module_finanzas(user):
    for group in user.groups.all():
        if group.name == 'finanzas':
            return True
    return False


def user_module_administracion(user):
    for group in user.groups.all():
        if group.name == 'departamento':
            return True
    return False


def user_module_registro_extemporaneo(user):
    for group in user.groups.all():
        if group.name == 'capturista_extemporaneo':
            return True
    return False


def user_module_registro_aspirante(user):
    if user_module_registro(user):
        aspirante = Aspirante.objects.filter(usuario=user)
        if aspirante.exists():
            return True
    return False


def user_module_password(user):
    for group in user.groups.all():
        if group.name == 'subsistema' or group.name == 'plantel' or group.name == 'apoyo':
            return True
    return False


def user_module_test_aspirante(user):
    if user_module_registro(user):
        aspirante = Aspirante.objects.filter(usuario=user)[:1].get()
        if aspirante.orientacion_vocacional:
            return True
    return False
