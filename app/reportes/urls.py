
from django.conf.urls import url

from app.reportes import views

urlpatterns = [
    url(r'^$', views.ReportesView.as_view(), name='reportes'),
    url(r'^panel/$', views.ReportesPanelView.as_view(), name='panel'),
]
