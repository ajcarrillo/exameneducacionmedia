# -*- encoding: utf-8 -*
import logging
from datetime import datetime

from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.views import generic

from general.PentahoReport.NativePentahoReport import NativePentahoReport

logger = logging.getLogger('siem')
module = 'app/reportes/views.py'


class ReportesView(generic.View):
    def get(self, request):
        try:
            report_name = request.GET.get('reporte')
            format = 'pdf' if request.GET.get('formato') is None else request.GET.get('formato')
            params = request.GET.getlist('param')

            reporte = NativePentahoReport()
            reporte.report = report_name  # name = solicitud_pago.referencia_pago.referencia
            reporte.format = format  # 'xls', 'pdf'
            for param in params:
                reporte.set(param.split(':')[0], str(param.split(':')[1]))
            return reporte.download()
        except Exception as e:
            message = '{0} - {1} - {2} - {3}'.format(datetime.today(), module, 'ReportesView', e.message)
            logger.warning(message)
            return HttpResponse(status=404)


class ReportesPanelView(generic.View):
    template_name = 'panel.html'

    def get(self, request):
        return render(request, self.template_name, None)

    def post(self, request):
        return render(request, 'testrender.html', None)
