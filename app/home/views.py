# -*- encoding: utf-8 -*
import datetime

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views import generic
from sparkpost import SparkPost

from app.accounts.forms import LoginAuthenticationForm
from app.administracion.models import EtapaProceso
from app.home.forms import PasswordResetRequestForm, SetPasswordForm
from exameneducacionmedia.settings import SPARKPOST_API_KEY, DEFAULT_FROM_EMAIL
from general.mixins import RedirectMixin


class HomeView(RedirectMixin, generic.View):
    def get(self, request):
        form = LoginAuthenticationForm
        today = datetime.date.today()
        registro = EtapaProceso.objects.get(nombre__exact='REGISTRO')
        is_registro = True if registro.apertura <= today <= registro.cierre else False
        context = {'form': form, 'is_registro': is_registro, 'registro': registro}

        return render(request, "home.html", context)


class ResetPasswordRequestView(generic.FormView):
    template_name = "reset_password/reset_password.html"
    success_url = reverse_lazy("home:home")
    form_class = PasswordResetRequestForm

    @staticmethod
    def validate_email_address(email):

        try:
            validate_email(email)
            return True
        except ValidationError:
            return False

    def reset_password(self, user, request):
        sp = SparkPost(SPARKPOST_API_KEY)

        uid = urlsafe_base64_encode(force_bytes(user.pk))
        token = default_token_generator.make_token(user)
        domain = request.META['HTTP_HOST'],
        url = reverse('home:reset_password_confirm', kwargs={'uidb64': uid, 'token': token})

        response = sp.transmissions.send(
            recipients=[user.email],
            template='siem-reset-passwod',
            from_email=DEFAULT_FROM_EMAIL,
            subject='Restablecimiento de la contraseña en SIEM',
            track_opens=True,
            track_clicks=True,
            transactional=True,
            substitution_data={
                "username":           user.get_username(),
                "user_full_name":     user.get_full_name(),
                "reset_password_url": "http://" + domain[0] + url,
            },
        )
        print(response)

        """
        c = {
            'email':              user.email,
            'domain': request.META['HTTP_HOST'],
            'site_name':          'SIEM',
            'uid':                urlsafe_base64_encode(force_bytes(user.pk)),
            'user':               user,
            'token':              default_token_generator.make_token(user),
            'protocol':           'http',
        }
        print(c)
        subject_template_name = 'profile/reset_password/password_reset_subject.txt'
        # copied from
        # django/contrib/admin/templates/registration/password_reset_subject.txt
        # to templates directory
        email_template_name = 'profile/reset_password/password_reset_email.html'
        # copied from
        # django/contrib/admin/templates/registration/password_reset_email.html
        # to templates directory
        subject = loader.render_to_string(subject_template_name, c)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        email = loader.render_to_string(email_template_name, c)
        send_mail(subject, email, DEFAULT_FROM_EMAIL,
                  [user.email], fail_silently=False)
        """

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        try:
            if form.is_valid():
                data = form.cleaned_data["email_or_username"]
            # uses the method written above
            if self.validate_email_address(data) is True:
                '''
				If the input is an valid email address, then the following code will lookup for users associated with that email address. If found then an email will be sent to the address, else an error message will be printed on the screen.
				'''
                associated_users = User.objects.filter(
                    Q(email=data) | Q(username=data))
                if associated_users.exists():
                    for user in associated_users:
                        self.reset_password(user, request)

                    result = self.form_valid(form)
                    messages.success(
                        request, 'Se ha enviado un email a {0}. Comprueba tu bandeja de entrada para continuar restableciendo tu contraseña'.format(data))
                    return result
                result = self.form_invalid(form)
                messages.error(
                    request, 'Ningún usuario está asociado con este email')
                return result
            else:
                '''
				If the input is an username, then the following code will lookup for users associated with that user. If found then an email will be sent to the user's address, else an error message will be printed on the screen.
				'''
                associated_users = User.objects.filter(username=data)
                if associated_users.exists():
                    for user in associated_users:
                        self.reset_password(user, request)
                    result = self.form_valid(form)
                    messages.success(
                        request, "Se ha enviado un email a {0}. Comprueba tu bandeja de entrada para continuar restableciendo tu contraseña.".format(data))
                    return result
                result = self.form_invalid(form)
                messages.error(
                    request, 'Este nombre de usuario no existe en el sistema')
                return result
            messages.error(request, 'Entrada inválida')
        except Exception as e:
            print(e)
        return self.form_invalid(form)


class PasswordResetConfirmView(generic.FormView):
    template_name = "reset_password/reset_password.html"
    success_url = reverse_lazy("home:home")
    form_class = SetPasswordForm

    def post(self, request, uidb64=None, token=None, *arg, **kwargs):
        """
        View that checks the hash in a password reset link and presents a
        form for entering a new password.
        """
        UserModel = get_user_model()
        form = self.form_class(request.POST)
        assert uidb64 is not None and token is not None  # checked by URLconf
        try:
            uid = urlsafe_base64_decode(uidb64)
            user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            user = None

        if user is not None and default_token_generator.check_token(user, token):
            if form.is_valid():
                new_password = form.cleaned_data['new_password2']
                user.set_password(new_password)
                user.save()
                messages.success(request, 'Tu contraseña ha sido restaurada')
                return self.form_valid(form)
            else:
                messages.error(
                    request, 'Tu contraseña no ha sido restaurada')
                return self.form_invalid(form)
        else:
            messages.error(
                request, 'El enlace para restablecer tu contraseña ya no es válido')
            return self.form_invalid(form)
