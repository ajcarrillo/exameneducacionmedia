# -*- encoding: utf-8 -*
from django import forms


class PasswordResetRequestForm(forms.Form):
    email_or_username = forms.CharField(label="Correo electrónico", max_length=254,
                                        widget=forms.EmailInput(attrs={'class': 'form-control'}))


class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': "Las contraseñas no coinciden",
    }
    new_password1 = forms.CharField(label="Nueva contraseña",
                                    widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    new_password2 = forms.CharField(label="Confirmar nueva contraseña",
                                    widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        return password2
