# -*- encoding: utf-8 -*
from django.conf.urls import url

from app.home import views

# **************************************
#
#   url: /
#   namespace: home
#
# **************************************

urlpatterns = [
    url(r'^$', views.HomeView.as_view(), name='home'),
    url(r'^reset-password-confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', views.PasswordResetConfirmView.as_view(), name='reset_password_confirm'),
    url(r'^reset-password/$', views.ResetPasswordRequestView.as_view(), name='reset_password')
]
