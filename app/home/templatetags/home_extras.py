# -*- encoding: utf-8 -*
from __future__ import unicode_literals

import datetime
from django import template

from app.administracion.models import EtapaProceso

register = template.Library()


@register.simple_tag(name='is_etapa_registro')
def is_etapa_registro():
    today = datetime.date.today()
    etapa = EtapaProceso.objects.get(nombre__exact='REGISTRO')
    if etapa.apertura <= today <= etapa.cierre:
        return True
    return False
