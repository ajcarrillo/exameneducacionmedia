# -*- encoding: utf-8 -*
from django.conf.urls import url

from app.finanzas import views

# **************************************
#
#   url: /finanzas/
#   namespace: finanzas
#
# **************************************

urlpatterns = [
    url(r'^carga-de-pagos/$', views.ProcesarReporteView.as_view(), name='procesar_pagos'),
    url(r'^problema-de-pagos/$', views.ProblemaDePagoView.as_view(), name='problema_de_pago'),
    url(r'^problema-de-pagos/buscar-aspirante/$', views.BuscarAspirante.as_view(), name='buscar_aspirante'),
    url(r'^problema-de-pagos/asignar-pago/(?P<solicitud>[\d]+)/(?P<deposito>[\d]+)/$', views.AsignarPapgo.as_view(), name='asignar-pago'),
]
