# -*- encoding: utf-8 -*
from django.contrib.auth.models import User, Group
from django.db.models import Q
from django.forms import model_to_dict
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render
from django.views import generic

from app.administracion.models import RevisionRegistro
from app.billy.models import Deposito, SolicitudPago
from general.mixins import LoginRequiredMixin, GroupRequiredMixin


class ProblemaDePagoView(generic.View):
    template_name = 'problema_de_pagos/problema_de_pago.html'

    def json_response(self, referencia):
        data_json = {}
        status = 200
        try:
            if referencia is None:
                raise Exception('Bad request')
            depositos = Deposito.objects.all().filter(referencia=referencia)
            if depositos.exists():
                response = {}
                response['depositos'] = list()
                cant_depositos = 0
                cant_solicitudes = 0
                depositos_list = list()
                for deposito in depositos:
                    # Obtenemos las solicitudes de pago relacionadas a ese deposito
                    try:
                        solicitud_pago = SolicitudPago.objects.get(deposito=deposito.id)
                        cant_solicitudes += 1
                    except SolicitudPago.DoesNotExist:
                        solicitud_pago = None
                        datasp = None

                    if solicitud_pago is not None:
                        solicitud_pago.fecha_solicitud = solicitud_pago.fecha_solicitud.isoformat()
                        solicitud_pago.monto = str(solicitud_pago.monto)
                        solicitud_pago.descuento = str(solicitud_pago.descuento)
                        solicitud_pago.total = str(solicitud_pago.total)
                        datasp = model_to_dict(solicitud_pago)

                    deposito.fecha = deposito.fecha.isoformat()
                    deposito.abono = str(deposito.abono)
                    deposito.saldo = str(deposito.saldo)

                    data = model_to_dict(deposito)
                    response['depositos'].append(data)

                    d = list()
                    data['solicitud_pago'] = datasp
                    d.append(data)
                    depositos_list.append(d)
                    cant_depositos += 1

                response['resumen'] = {
                    'cant_depositos':   cant_depositos,
                    'cant_solicitudes': cant_solicitudes
                }

                data_json.update({'json_body': response, 'is_valid': True, 'msg': ''})
            else:
                raise Exception('No se encuentra la referencia de pago')
        except Exception as e:
            data_json.update({'is_valid': False, 'msg': e.message})
            status = 400

        print ('*' * 50)
        print (data_json)
        print ('*' * 50)

        return JsonResponse(data_json, safe=False, status=status)

    def get_queryset(self, referencia=None):
        queryset = {}
        aspirante = {}
        if referencia is None:
            return queryset

        depositos = Deposito.objects.filter(referencia=referencia)
        if depositos.exists():
            queryset.update({'depositos': list()})
            solicitud_pago_to_dict = None
            cant_depositos = 0
            cant_solicitudes = 0
            for deposito in depositos:
                try:
                    solicitud_pago = SolicitudPago.objects.get(deposito=deposito.id)
                    revision_registro = RevisionRegistro.objects.get(solicitud_pago=solicitud_pago.id)
                    aspirante['username'] = revision_registro.aspirante.usuario.username
                    aspirante['nombre'] = revision_registro.aspirante.usuario.get_full_name()

                    cant_solicitudes += 1
                except SolicitudPago.DoesNotExist:
                    solicitud_pago = None

                if solicitud_pago is not None:
                    solicitud_pago.fecha_solicitud = solicitud_pago.fecha_solicitud.isoformat()
                    solicitud_pago.monto = str(solicitud_pago.monto)
                    solicitud_pago.descuento = str(solicitud_pago.descuento)
                    solicitud_pago.total = str(solicitud_pago.total)
                    solicitud_pago_to_dict = model_to_dict(solicitud_pago)

                deposito.fecha = deposito.fecha.isoformat()
                deposito.abono = str(deposito.abono)
                deposito.saldo = str(deposito.saldo)

                deposito_to_dict = model_to_dict(deposito)
                deposito_to_dict.update({'solicitud_pago': solicitud_pago_to_dict, 'aspirante': aspirante})
                queryset['depositos'].append(deposito_to_dict)
                data = list()
                data.append(deposito_to_dict)
                cant_depositos += 1

                queryset.update({'resumen': {
                    'cant_depositos':   cant_depositos,
                    'cant_solicitudes': cant_solicitudes
                }, 'referencia':            referencia})
        return queryset

    def get(self, request, referencia=None):

        if 'referencia' in request.GET:
            referencia = request.GET.get('referencia')

        return render(request, self.template_name, self.get_queryset(referencia))


class BuscarAspirante(generic.View):
    def get(self, request):
        if not request.is_ajax():
            return HttpResponse("Bad request", status=400)

        criteria = Q()
        if 'username' in request.GET and request.GET.get('username') != '':
            criteria.add(Q(username__icontains=request.GET.get('username')), Q.AND)
        if 'first_name' in request.GET and request.GET.get('first_name') != '':
            criteria.add(Q(first_name__icontains=request.GET.get('first_name')), Q.AND)
        if 'last_name' in request.GET and request.GET.get('last_name') != '':
            criteria.add(Q(last_name__icontains=request.GET.get('last_name')), Q.AND)
        users = User.objects.filter(criteria, groups__in=[Group.objects.get(name='alumno')], aspirante_related__revision_aspirante_related__isnull=False,
                                    aspirante_related__revision_aspirante_related__pago_efectuado=False)

        data = [{
                    'username':       user.username,
                    'first_name':     user.first_name,
                    'last_name':      user.last_name,
                    'solicitud_pago': user.aspirante_related.revision_aspirante_related.solicitud_pago if (hasattr(user.aspirante_related, 'revision_aspirante_related')) else None
                } for user in users]

        return JsonResponse(data, safe=False)


class AsignarPapgo(generic.View):
    def get(self, request, solicitud=None, deposito=None):
        if not request.is_ajax():
            return HttpResponse("Bad request", status=400)

        try:
            solicitud_pago = SolicitudPago.objects.get(pk=solicitud)
            deposito = Deposito.objects.get(pk=deposito)
            deposito.abono = solicitud_pago.total
            deposito.save()
            solicitud_pago.deposito = deposito
            solicitud_pago.save()
            response = {
                'is_valid': True,
                'msg': 'El pago se asignó correctamente'
            }
        except Exception as e:
            response = {
                'is_valid': False,
                'msg': e
            }

        return JsonResponse(response, safe=False)

