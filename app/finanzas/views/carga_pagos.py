# -*- encoding: utf-8 -*
import requests
from django.contrib import messages
from django.shortcuts import render
from django.views import generic

from app.billy.models import Banco
from exameneducacionmedia.settings import BILLY_SERVICE_URL
from general.mixins import LoginRequiredMixin, GroupRequiredMixin


class ProcesarReporteView(LoginRequiredMixin, GroupRequiredMixin, generic.View):
    group_required = 'departamento'
    template_name = 'procesar_reporte.html'

    def render_to_response(self, request):
        context = {
            'bancos': Banco.objects.all()
        }
        return render(request, self.template_name, context)

    def get(self, request):
        return self.render_to_response(request)

    def post(self, request):
        try:
            if not request.FILES.get('pagos-file'):
                raise Exception('No seleccionó ningún archivo')
            archivo_pagos = request.FILES['pagos-file']
            url = '{0}/pagos/deposito/reporte/'.format(BILLY_SERVICE_URL)
            billy_response = requests.post(url,
                                           files={
                                               'archivo': (archivo_pagos.name, archivo_pagos.read())
                                           },
                                           data={
                                               'banco': request.POST.get('banco')
                                           }
                                           )
            raise Exception(billy_response.text)
        except Exception as e:
            messages.info(request, e.message)
        return self.render_to_response(request)
