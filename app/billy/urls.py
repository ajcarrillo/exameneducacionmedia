# -*- encoding: utf-8 -*
from django.conf.urls import url
from app.billy import views

# **************************************
#
#   url: /billy/
#   namespace: billy
#
# **************************************

urlpatterns = [
    url(r'^salario-minimo/nuevo/$', views.SalarioMinimoView.as_view(), name='create_salario_minimo'),
    url(r'^concepto/actualizar/$', views.PaemsConceptoView.as_view(), name='update_concepto_paems'),
]
