# -*- encoding: utf-8 -*
import datetime
from django.contrib import messages
from django.db import transaction
from django.shortcuts import redirect
from django.views import generic

from app.billy.models import SalarioMinimo
from general.mixins import LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin


class SalarioMinimoView(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, generic.View):
    def post(self, request):
        transaction.set_autocommit(False)
        try:
            SalarioMinimo.objects.filter(activo=True).update(activo=False, fecha_baja=datetime.date.today())
            salario_minimo = SalarioMinimo()
            salario_minimo.monto = request.POST.get('monto')
            salario_minimo.activo = True
            salario_minimo.fecha_alta = datetime.date.today()
            salario_minimo.save()
            transaction.commit()
            messages.info(request, 'El salario mínimo se actualizó correctamente')
            print("*"*50)
            print("exito")
            print("*"*50)
        except Exception as e:
            transaction.rollback()
            messages.info(request, e)
            print('*'*50)
            print(e.message)
            print('*'*50)
        return redirect(request.META.get('HTTP_REFERER'))
