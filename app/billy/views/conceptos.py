# -*- encoding: utf-8 -*
from django.contrib import messages
from django.shortcuts import redirect
from django.views import generic

from app.billy.models import Concepto
from general.mixins import LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin


class PaemsConceptoView(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, generic.View):
    def post(self, request):
        concepto = Concepto.objects.get(pk=26)
        concepto.salarios_minimos = request.POST.get('salarios_minimos')
        concepto.save()
        messages.info(request, 'El salario mínimo se actualizó correctamente')
        return redirect(request.META.get('HTTP_REFERER'))
