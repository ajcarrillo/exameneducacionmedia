# -*- encoding: utf-8 -*
from django import forms

from app.mgeo.models import Municipio, Localidad


class DomicilioForm(forms.Form):
    municipio = forms.ModelChoiceField(Municipio.objects.filter(entidad_federativa=23), label='Municipio', widget=forms.Select(attrs={'class': 'form-control input-lg'}), empty_label='Municipio')
    localidad = forms.ModelChoiceField(Localidad.objects.none(), label='Localidad', widget=forms.Select(attrs={'class': 'form-control input-lg', 'required': 'required'}), empty_label='Localidad')
    colonia = forms.CharField(label='Colonia', widget=forms.TextInput(attrs={'class': 'form-control input-lg'}))
    calle = forms.CharField(label='Calle', widget=forms.TextInput(attrs={'class': 'form-control input-lg'}))
    numero = forms.CharField(label='Número', widget=forms.TextInput(attrs={'class': 'form-control input-lg'}))
    codigo_postal = forms.CharField(label='Código Postal', widget=forms.TextInput(attrs={'class': 'form-control input-lg'}))

    def __init__(self, *args, **kwargs):
        super(DomicilioForm, self).__init__(*args, **kwargs)

        if args:
            my = args[0]
            self.fields['localidad'].queryset = Localidad.objects.filter(pk=my['localidad'])

        if 'initial' in kwargs:
            self.fields['localidad'].queryset = Localidad.objects.filter(municipio=kwargs['initial']['municipio'])
