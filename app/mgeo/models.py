# -*- encoding: utf-8 -*
from __future__ import unicode_literals

from django.db import models


class EntidadFederativa(models.Model):
    clave = models.CharField(db_column='clave', max_length=2)
    nombre = models.CharField(db_column='nombre', max_length=255)
    abreviacion = models.CharField(db_column='abreviacion', max_length=8)
    capital = models.ForeignKey('Localidad', db_column='capital', null=True)
    clave_curp = models.CharField(max_length=2)

    class Meta:
        db_table = 'entidad_federativa'

    def __unicode__(self):
        return '%s' % self.nombre


class Municipio(models.Model):
    clave = models.CharField(db_column='clave', max_length=3)
    nombre = models.CharField(db_column='nombre', max_length=255)
    entidad_federativa = models.ForeignKey(EntidadFederativa, db_column='entidad_federativa', related_name='municipio_related')
    cabecera = models.ForeignKey('Localidad', db_column='cabecera', null=True, related_name='cabecera_related')

    class Meta:
        db_table = 'municipio'

    def __unicode__(self):
        return '%s' % self.nombre


class Localidad(models.Model):
    clave = models.CharField(db_column='clave', max_length=4)
    nombre = models.CharField(db_column='nombre', max_length=255)
    municipio = models.ForeignKey(Municipio, db_column='municipio', related_name='localidad_related')

    class Meta:
        db_table = 'localidad'

    def __unicode__(self):
        return '%s' % self.nombre


class Domicilio(models.Model):
    localidad = models.ForeignKey(Localidad, db_column='localidad', related_name='domicilio_related')
    colonia = models.CharField(db_column='colonia', max_length=255, blank=True, null=True)
    calle = models.CharField(db_column='calle', max_length=255, blank=True, null=True)
    numero = models.CharField(db_column='numero', blank=True, null=True, max_length=6)
    codigo_postal = models.CharField(db_column='codigo_postal', max_length=5, blank=True, null=True)

    class Meta:
        db_table = 'domicilio'

    def __unicode__(self):
        return '%s %s %s %s %s' % (self.calle, self.numero, self.colonia, self.codigo_postal, self.localidad)


class NivelDeDemanda(Municipio):
    A = 'ALTO'
    B = 'BAJO'
    N = 'NORMAL'
    NIVEL_DE_DEMANDA_CHOICES = (
        (A, 'ALTO'),
        (B, 'BAJO'),
        (N, 'NORMAL'),
    )
    municipio = models.OneToOneField(Municipio, primary_key=True, parent_link=True, on_delete=models.DO_NOTHING, related_name='nivel_de_demanda_related')
    nivel_de_demanda = models.CharField(choices=NIVEL_DE_DEMANDA_CHOICES, default=N, max_length=10)

    class Meta:
        db_table = 'municipio_nivel_de_demanda'
