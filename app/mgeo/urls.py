# -*- encoding: utf-8 -*
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^localidades/$', views.localidades, name="localidades"),
    url(r'^localidades/(?P<municipio_id>[0-9]+)/$', views.localidades, name="localidades")
]
