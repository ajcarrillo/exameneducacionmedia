from django.http import JsonResponse

from app.mgeo.models import Localidad, Municipio


def localidades(request, municipio_id=None):
    if municipio_id is None:
        localidades = Localidad.objects.all()
    else:
        localidades = Localidad.objects.filter(municipio=municipio_id)

    data = [{'id':   localidad.id,
             'text': localidad.nombre
             } for localidad in localidades]

    return JsonResponse(data, safe=False)
