from __future__ import unicode_literals

from django.apps import AppConfig


class MgeoConfig(AppConfig):
    name = 'app.mgeo'
