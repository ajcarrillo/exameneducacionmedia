# -*- encoding: utf-8 -*
import datetime

from app.administracion.models import EtapaProceso

today = datetime.date.today()
ETAPAS_DEL_PROCESO = (
    (1, 'OFERTA', 'Periodo de carga y validación de ofertas académicas', today, today),
    (2, 'AFORO', 'Periodo de carga y validación de aforo para aplicación de exámenes de admisión', today, today),
    (3, 'REGISTRO', 'Periodo de registro y pagos al examen de admisión', today, today),
    (4, 'AJUSTE DE OFERTA', 'Periodo de ingreso de alumnos reales que el plantel va a recibir', today, today)
)
EtapaProceso.objects.all().delete()
for etapa in ETAPAS_DEL_PROCESO:
    new_stage = EtapaProceso()
    new_stage.pk = etapa[0]
    new_stage.nombre = etapa[1]
    new_stage.descripcion = etapa[2]
    new_stage.apertura = etapa[3]
    new_stage.cierre = etapa[4]
    new_stage.save()

print ("La carga se realizó correctamente")
