# -*- encoding: utf-8 -*
from app.administracion.models import Folio

try:
    folios = Folio.objects.first()
except Folio.DoesNotExist:
    folios = Folio()

folios.inicio = 1
folios.fin = 24000
folios.save()

print 'carga de folios terminada\n'
