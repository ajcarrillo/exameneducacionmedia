# -*- encoding: utf-8 -*
import datetime
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import generic

from app.administracion.models import EtapaProceso
from general.mixins import LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin


class Etapa(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, generic.View):
    template = 'etapas/etapas.html'

    @staticmethod
    def get_queryset():
        return EtapaProceso.objects.all()

    def get(self, request):
        context = {'etapas': self.get_queryset()}
        return render(request, self.template, context)

    def post(self, request):
        transaction.set_autocommit(False)
        try:
            for etapa in EtapaProceso.objects.all():
                apertura = request.POST['%s.apertura' % etapa.nombre]
                cierre = request.POST['%s.cierre' % etapa.nombre]
                try:
                    etapa.apertura = datetime.datetime.strptime(apertura, '%Y-%m-%d').date() if apertura != '' else None
                    etapa.cierre = datetime.datetime.strptime(cierre, '%Y-%m-%d').date() if cierre != '' else None

                    etapa.save()
                except ValueError:
                    raise Exception('Error al interpretar el rango de fechas de la etapa de %s' % etapa.nombre)

            """Sacamos la información necesaria de la base de datos para realizar las comparaciones"""
            etapa_oferta = EtapaProceso.objects.get(nombre='OFERTA')
            etapa_aforo = EtapaProceso.objects.get(nombre='AFORO')
            etapa_registro = EtapaProceso.objects.get(nombre='REGISTRO')

            """Obtenemos el año actual para validar las etapas"""
            anio = datetime.date.today().year

            """Validamos los anios de apertura y cierre de las etapas"""
            if etapa_oferta.apertura is not None:
                if etapa_oferta.apertura.year != anio:
                    raise Exception('El año de la etapa {0} es inválida.'.format(etapa_oferta.nombre))
            if etapa_oferta.cierre is not None:
                if etapa_oferta.cierre.year != anio:
                    raise Exception('El año de la etapa {0} es inválida.'.format(etapa_oferta.nombre))
            if etapa_aforo.apertura is not None:
                if etapa_aforo.apertura.year != anio:
                    raise Exception('El año de la etapa {0} es inválida.'.format(etapa_aforo.nombre))
            if etapa_aforo.cierre is not None:
                if etapa_aforo.cierre.year != anio:
                    raise Exception('El año de la etapa {0} es inválida.'.format(etapa_aforo.nombre))

            if etapa_registro.apertura is not None:
                if etapa_registro.apertura.year != anio:
                    raise Exception('El año de la etapa {0} es inválida.'.format(etapa_registro.nombre))
            if etapa_registro.cierre is not None:
                if etapa_registro.cierre.year != anio:
                    raise Exception('El año de la etapa {0} es inválida.'.format(etapa_registro.nombre))

            """Validamos los rangos de fechas. Que el cierre sea mayor al de apertura y que no se aperture y cierre el mismo día"""
            if etapa_oferta.apertura is not None and etapa_oferta.cierre is not None:
                if etapa_oferta.apertura >= etapa_oferta.cierre:
                    raise Exception("La fecha de cierre de la etapa {0} es inválida.".format(etapa_oferta.nombre))

            if etapa_aforo.apertura is not None and etapa_aforo.cierre is not None:
                if etapa_aforo.apertura >= etapa_aforo.cierre:
                    raise Exception("La fecha de cierre de la etapa {0} es inválida.".format(etapa_aforo.nombre))

            if etapa_registro.apertura is not None and etapa_registro.cierre is not None:
                if etapa_registro.apertura >= etapa_registro.cierre:
                    raise Exception("La fecha de cierre de la etapa {0} es inválida.".format(etapa_registro.nombre))

            """Validamos que la oferta inicie primeramente que todos o igual que el aforo y que la etapa de registro inicie despues de la oferta """
            if etapa_oferta.apertura is not None:
                if etapa_aforo.apertura is not None:
                    if etapa_oferta.apertura > etapa_aforo.apertura:
                        raise Exception('La fecha de la etapa {0} debe aperturarse antes que la etapa AFORO'.format(etapa_oferta.nombre))
                if etapa_registro.apertura is not None:
                    # raise Exception(etapa_registro.apertura)
                    if etapa_oferta.apertura >= etapa_registro.apertura:
                        raise Exception('La fecha de la etapa {0} debe aperturarse antes que la etapa REGISTRO'.format(etapa_oferta.nombre))

            """Validamos que el aforo no cierre antes que la oferta educativa"""
            if etapa_aforo.cierre is not None:
                if etapa_oferta.cierre is not None:
                    if etapa_oferta.cierre > etapa_aforo.cierre:
                        raise Exception('La etapa de {0} no debe cerrar antes que la de OFERTA'.format(etapa_aforo.nombre))

            """Validamos que el registro se aperture unicamente hasta que las demás etapas hayan concluido"""
            if etapa_registro.apertura is not None:
                if etapa_oferta.cierre is not None:
                    if etapa_registro.apertura <= etapa_oferta.cierre:
                        raise Exception(
                            'La etapa {0} no puede aperturarse hasta que finalice las etapas de AFORO y OFERTA'.format(etapa_registro.nombre))
                if etapa_aforo.cierre is not None:
                    if etapa_registro.apertura <= etapa_aforo.cierre:
                        raise Exception(
                            'La etapa {0} no puede aperturarse hasta que finalice las etapas de AFORO y OFERTA'.format(etapa_registro.nombre))

            transaction.commit()
            return HttpResponseRedirect('/administracion/etapas')
        except Exception as e:
            response = render(request, self.template, {'error': e, 'etapas': self.get_queryset()})
            transaction.rollback()
            return response
