# -*- encoding: utf-8 -*
from django.contrib.auth.models import User, Group
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views import generic

from app.administracion.forms import SubsistemaModelForm
from app.planteles.models import Subsistema
from general.mixins import LoginRequiredMixin


class SubsistemaListView(generic.ListView):
    model = Subsistema
    template_name = 'subsistemas/list.html'
    context_object_name = 'subsistemas'

    def get_queryset(self):
        return self.model.objects.select_related('responsable').all()


class SubsistemaCreateView(generic.View):
    template_name = 'subsistemas/create.html'

    def get(self, request):
        return render(request, self.template_name, {})

    def post(self, request):
        transaction.set_autocommit(False)
        try:
            subsistema = Subsistema()

            subsistema.referencia = request.POST['referencia']
            subsistema.descripcion = request.POST['descripcion']

            username = request.POST['username']
            password = request.POST['password']
            email = request.POST['email']
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']

            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name

            user.save()

            subsistema.responsable = user

            subsistema.save()

            user.groups.clear()
            group = Group.objects.get(name='subsistema')
            user.groups.add(group)

            transaction.commit()
            return HttpResponseRedirect(reverse("administracion:subsistemas_list"))
        except Exception as e:
            response = render(request, self.template_name, {'error': e})
            transaction.rollback()
            return response


class SubsistemaUpdateView(generic.UpdateView):
    model = Subsistema
    template_name = 'subsistemas/edit.html'
    context_object_name = 'subsistema'
    form_class = SubsistemaModelForm
