# -*- encoding: utf-8 -*
from django.contrib import messages
from django.contrib.auth.models import User, Group
from django.core.exceptions import ImproperlyConfigured
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views import generic

from app.administracion.forms import UserForm
from general.mixins import JsonResponseMixin, LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin


class UsuarioListView(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, generic.ListView):
    model = User
    template_name = 'usuarios/index.html'

    def get_context_data(self, **kwargs):
        context = super(UsuarioListView, self).get_context_data(**kwargs)
        queries_without_page = self.request.GET.copy()
        if 'page' in queries_without_page:
            del queries_without_page['page']

        context.update({'queries': queries_without_page})

        return context

    def get_queryset(self):
        queryset = self.model.objects.exclude(Q(groups__name='alumno') | Q(is_superuser=True)).exclude(pk=self.request.user.id)
        if self.request.GET.get("group"):
            return queryset.filter(groups__name=self.request.GET['group'])
        elif self.request.GET.get("search"):
            term = self.request.GET['search']
            return queryset.filter(Q(first_name__icontains=term) | Q(last_name__icontains=term) | Q(username__icontains=term) | Q(email__icontains=term))
        return queryset


class UsuarioCreateView(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, generic.CreateView):
    form_class = UserForm
    model = User
    template_name = 'usuarios/create.html'

    def post(self, request, *args, **kwargs):
        form = UserForm(request.POST, auto_id='%s')
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            email = form.cleaned_data['email']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']

            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name

            user.save()

            user.groups.clear()
            for group_id in form['groups'].data:
                grupo = Group.objects.get(pk=group_id)
                if grupo.name == 'finanzas' and not request.user.is_superuser:
                    continue
                user.groups.add(grupo)

            return HttpResponseRedirect(reverse('administracion:users'))

    def get_form(self, form_class=None):
        form_class = self.form_class(auto_id='%s')
        return form_class


class UserUpdateView(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, generic.UpdateView):
    form_class = UserForm
    model = User
    template_name = 'usuarios/edit.html'

    def get_form(self, form_class=None):
        form = super(UserUpdateView, self).get_form(form_class)
        form.fields.pop('password')
        return form

    def get_initial(self):
        return {'groups': self.object.groups.all()}

    def form_valid(self, form):
        self.object = form.save()

        self.object.groups.clear()
        for group_id in self.request.POST.getlist('groups'):
            grupo = Group.objects.get(pk=group_id)
            if grupo.name == 'finanzas' and not self.request.user.is_superuser:
                continue
            self.object.groups.add(grupo)

        return super(UserUpdateView, self).form_valid(form)

    def get_success_url(self):
        if self.success_url:
            url = self.success_url % self.object.__dict__
        else:
            try:
                url = reverse('administracion:update_user', kwargs={'pk': self.object.pk})
            except AttributeError:
                raise ImproperlyConfigured(
                    "No URL to redirect to.  Either provide a url or define"
                    " a get_absolute_url method on the Model.")
        return url


class UpdatePassword(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, generic.View):
    template_name = "usuarios/update_password.html"
    model = User

    def get_user(self, pk=None):
        return self.model.objects.get(pk=pk)

    def get(self, request, pk=None):
        context = {
            'usuario': self.get_user(pk)
        }
        return render(request, self.template_name, context)

    def post(self, request, pk=None):
        user = self.get_user(pk=pk)
        user.set_password(request.POST.get('password'))
        user.save()
        messages.info(request, 'La contraseña se actualizó correctamente')
        return redirect(reverse('administracion:users'))
