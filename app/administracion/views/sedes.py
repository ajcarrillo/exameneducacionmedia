# -*- encoding: utf-8 -*
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views import generic

from app.administracion.forms import SedeAlternaForm
from app.mgeo.models import Domicilio
from app.planteles.models import SedeAlterna


class SedeView(generic.View):
    model = SedeAlterna
    form_class = SedeAlternaForm

    def process_form(self, domicilio, form, sede):
        try:
            domicilio.localidad = form.cleaned_data['localidad']
            domicilio.colonia = form.cleaned_data['colonia']
            domicilio.calle = form.cleaned_data['calle']
            domicilio.numero = form.cleaned_data['numero']
            domicilio.codigo_postal = form.cleaned_data['codigo_postal']
            domicilio.save()
            sede.referencia = form.cleaned_data['referencia']
            sede.plantel = form.cleaned_data['plantel']
            sede.domicilio = domicilio
            sede.save()
        except Exception as e:
            raise Exception("Ocurrió un problema al guardar la sede")


class SedesListView(generic.ListView):
    model = SedeAlterna
    template_name = 'sedes/list.html'
    context_object_name = 'sedes'

    def get_queryset(self):
        return self.model.objects.select_related('plantel', 'domicilio').all()


class SedesCreateView(SedeView):
    template_name = 'sedes/create.html'

    def get(self, request):
        context = {
            'form': self.form_class(auto_id='%s')
        }
        return render(request, self.template_name, context)

    def post(self, request):
        form = self.form_class(request.POST, auto_id='%s')
        transaction.set_autocommit(False)
        try:
            if form.is_valid():
                domicilio = Domicilio()
                sede = SedeAlterna()

                self.process_form(domicilio, form, sede)

                transaction.commit()
                return HttpResponseRedirect(reverse("administracion:sedes_list"))
        except Exception as e:
            transaction.rollback()
            return render(request, self.template_name, {'form': form})


class SedesEditView(SedeView):
    template_name = "sedes/edit.html"

    def get_queryset(self, pk):
        return self.model.objects.select_related('plantel', 'domicilio').get(pk=pk)

    def get(self, request, pk=None):
        sede = self.get_queryset(pk)

        form = self.form_class(auto_id='%s', initial={
            'plantel':       sede.plantel,
            'referencia':    sede.referencia,
            'municipio':     sede.domicilio.localidad.municipio,
            'localidad':     sede.domicilio.localidad,
            'colonia':       sede.domicilio.colonia,
            'calle':         sede.domicilio.calle,
            'numero':        sede.domicilio.numero,
            'codigo_postal': sede.domicilio.codigo_postal
        })
        context = {'form': form, 'sede': sede}

        return render(request, self.template_name, context)

    def post(self, request, pk=None):
        form = self.form_class(request.POST, auto_id='%s')
        sede = self.get_queryset(pk)

        transaction.set_autocommit(False)
        try:
            if form.is_valid():
                domicilio = sede.domicilio

                self.process_form(domicilio, form, sede)

                transaction.commit()
                return HttpResponseRedirect(reverse("administracion:sedes_edit", kwargs={'pk': sede.pk}))
            return render(request, self.template_name, {'form': form, 'sede': sede})
        except Exception as e:
            transaction.rollback()
            return render(request, self.template_name, {'form': form, 'sede': sede})
