# -*- encoding: utf-8 -*
from datetime import datetime
from django.http import Http404
from django.http import HttpResponse
from django.http import JsonResponse
from django.views import generic

from app.administracion.models import RevisionRegistro, Revision
from app.aspirantes.models import Aspirante
from app.billy.models import SolicitudPago
from general.mixins import LoginRequiredMixin, GroupRequiredMixin


class AspiranteListView(LoginRequiredMixin, GroupRequiredMixin, generic.ListView):
    group_required = ('departamento', 'plantel',)
    model = Aspirante
    template_name = 'aspirantes/list.html'
    context_object_name = 'aspirantes'

    def get_queryset(self):
        request = self.request

        if request.GET:
            first_name = request.GET.get('first_name', None)
            last_name = request.GET.get('last_name', None)
            term = request.GET.get('term', None)
            referencia_de_pago = request.GET.get('referencia', None)
            return self.model.get_aspirante(first_name, last_name, term, referencia_de_pago)
        return self.model.objects.none()

    def get_context_data(self, **kwargs):
        context = super(AspiranteListView, self).get_context_data(**kwargs)
        total = self.model.objects.count()
        context.update({'aspirantes_total': total})
        return context


class AspiranteTemplateView(LoginRequiredMixin, GroupRequiredMixin, generic.DetailView):
    group_required = 'departamento'
    template_name = 'aspirantes/aspirante.html'
    model = Aspirante
    aspirante = None
    pk_url_kwarg = 'aspirante_id'
    context_object_name = 'aspirante'
    queryset = Aspirante.objects.select_related('usuario',
                                                'domicilio',
                                                'domicilio__localidad',
                                                'informacion_de_procedencia_related',
                                                'revision_aspirante_related',
                                                'revision_aspirante_related__revision__usuario_apertura',
                                                'revision_aspirante_related__revision__usuario_revision').prefetch_related('seleccion_oferta_educativa__oferta_educativa__plantel',
                                                                                                                           'seleccion_oferta_educativa__oferta_educativa__especialidad').all()

    def get_object(self, queryset=None):
        queryset = self.get_queryset()
        try:
            self.aspirante = queryset.get(pk=self.kwargs[self.pk_url_kwarg])
            return self.aspirante
        except Aspirante.DoesNotExist:
            raise Http404

    def get_context_data(self, **kwargs):
        context = super(AspiranteTemplateView, self).get_context_data(**kwargs)
        solicitud_pago = None
        revision = None
        if hasattr(self.aspirante, 'revision_aspirante_related'):
            revision = self.aspirante.revision_aspirante_related
            solicitud_pago = SolicitudPago.objects.select_related('referencia_pago',
                                                                  'concepto',
                                                                  'salario_minimo',
                                                                  'deposito',
                                                                  'deposito__reporte_deposito',
                                                                  'deposito__reporte_deposito__banco').get(pk=revision.solicitud_pago)

        context.update({'solicitud_pago': solicitud_pago, 'revision': revision})
        return context


class UpdatePayment(LoginRequiredMixin, GroupRequiredMixin, generic.View):
    group_required = 'departamento'

    def post(self, request, aspirante_id):
        is_valid = False
        msg = None

        try:
            revision = RevisionRegistro.objects.get(aspirante=aspirante_id, estado=Revision.ESTADO_EN_REVISION, pago_efectuado=False)

            revision.pago_efectuado = True
            revision.fecha_revision = datetime.today()
            revision.estado = Revision.ESTADO_ACEPTADO
            revision.comentario = "El usuario {0} cambió el pago efectuado".format(request.user.username)
            revision.usuario_revision = request.user
            revision.save()
            is_valid = True
        except RevisionRegistro.DoesNotExist:
            msg = "El aspirante ya tiene su pago efectuado o no existe"
        except Exception as e:
            msg = e.message

        return JsonResponse({'is_valid': is_valid, 'msg': msg})
