# -*- encoding: utf-8 -*
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.urls import reverse_lazy
from django.views import generic

from app.administracion.forms import EnlaceForm
from app.administracion.models import Enlace


class EnlacesListView(generic.ListView):
    template_name = "enlaces/list.html"
    model = Enlace
    context_object_name = "enlaces"


class EnlaceViewUtils(object):
    template_name = None
    succes_url = "/administracion/configuraciones/enlaces/"
    form_class = EnlaceForm
    model = Enlace

    def get(self, request, pk=None):
        enlace = None
        if pk is None:
            form = self.form_class(auto_id='%s')
        else:
            enlace = self.get_query_set(pk)
            form = self.form_class(initial={
                'municipio': enlace.municipio,
                'domicilio': enlace.domicilio,
                'telefono':  enlace.telefono,
                'extension': enlace.extension,
                'fechas':    enlace.fecha_rango
            }, auto_id='%s')
        context = {'form': form, 'enlace': enlace}
        return render(request, self.template_name, context)

    def get_query_set(self, pk=None):
        return self.model.objects.get(pk=pk)

    def save_model(self, enlace, form):
        enlace.municipio = form.cleaned_data['municipio'].id
        enlace.domicilio = form.cleaned_data['domicilio']
        enlace.telefono = form.cleaned_data['telefono']
        enlace.extension = form.cleaned_data['extension']
        enlace.fecha_inicio = self.request.POST.get('fecha_inicio')
        enlace.fecha_fin = self.request.POST.get('fecha_fin')
        enlace.hora_inicio = self.request.POST.get('hora_inicio')
        enlace.hora_fin = self.request.POST.get('hora_fin')
        enlace.save()


class EnlaceCreateView(EnlaceViewUtils, generic.View):
    template_name = "enlaces/create.html"

    def post(self, request):
        form = self.form_class(request.POST, auto_id='%s')
        if form.is_valid():
            enlace = self.model()
            self.save_model(enlace, form)
            self.get_succes_url()
            return HttpResponseRedirect(self.succes_url)
        return render(request, self.template_name, {'form': form})

    def get_succes_url(self):
        if '_save_and_add_another' in self.request.POST:
            self.succes_url = reverse('administracion:enlaces_create')
            return self.succes_url


class EnlaceEditView(EnlaceViewUtils, generic.View):
    template_name = "enlaces/edit.html"

    def post(self, request, pk=None):
        form = self.form_class(request.POST, auto_id='%s')
        if form.is_valid():
            enlace = self.get_query_set(pk)
            self.save_model(enlace, form)
            self.get_succes_url()
            return HttpResponseRedirect(self.succes_url)
        return render(request, self.template_name, {'form': form})

    def get_succes_url(self):
        if '_save_and_add_another' in self.request.POST:
            self.succes_url = reverse('administracion:enlaces_edit', kwargs={'pk': self.kwargs['pk']})
            return self.succes_url


class EnlaceDeleteView(generic.DeleteView):
    model = Enlace
    success_url = reverse_lazy('administracion:enlaces')

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            self.delete(request, *args, **kwargs)
            data = {'is_valid': True}
            return JsonResponse(data, safe=False)
