# -*- encoding: utf-8 -*
import datetime
from django.db.models import Sum
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render
from django.views import generic

from app.administracion.models import RevisionOfertaEducativa, Revision
from app.mgeo.models import Municipio
from app.planteles.models import Subsistema, PlantelHistorico, OfertaEducativa, OfertaEducativaHistorico
from general.mixins import LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, UtilsMixin


class RevisionOfertaEducativaListView(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, UtilsMixin,
                                      generic.ListView):
    model = RevisionOfertaEducativa
    template_name = "revision_ofertas_educativas/list.html"
    context_object_name = "revisiones"

    def get_queryset(self):
        queryset = self.model.objects.select_related('subsistema', 'usuario_apertura', 'usuario_revision')
        if 'estado' in self.request.GET:
            return queryset.filter(estado=self.request.GET.get('estado'))
        if 'subsistema' in self.request.GET:
            return queryset.filter(subsistema_id=self.request.GET.get('subsistema'))
        return queryset.filter(estado=Revision.ESTADO_EN_REVISION)

    def get_context_data(self, **kwargs):
        context = super(RevisionOfertaEducativaListView, self).get_context_data(**kwargs)
        context.update({
            'subsistemas': Subsistema.objects.all(),
            'en_revision': self.model.objects.filter(estado=Revision.ESTADO_EN_REVISION).count()
        })

        return context


class RevisionOfertaEducativaSubsistemaListView(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, generic.View):
    def get(self, request, subsistema_id=None):
        if request.is_ajax():
            format = request.GET.get('format', None)
            if format == 'json':
                return self.json_to_response()
            return HttpResponseBadRequest("La url es inválida")

        context = self.get_data(request.GET.get('revision'))
        return render(request, "revision_ofertas_educativas/subsistema.html", context)

    def get_data(self, revision_id):
        subsistema = Subsistema.objects.get(pk=self.kwargs['subsistema_id'])
        subsistema_historico = PlantelHistorico.objects.filter(subsistema=subsistema.referencia).values('subsistema',
                                                                                                        'anio').annotate(
            total_especialidades=Sum('especialidades'), total_alumnos=Sum('alumnos'),
            total_grupos=Sum('grupos')).order_by('-anio')

        oferta = OfertaEducativa.objects.filter(especialidad__subsistema=subsistema, activa=True)

        especialidades_now = oferta.count()
        grupos_now = 0
        alumnos_now = 0
        for item in oferta:
            grupos_now = item.grupos + grupos_now
            alumnos_now = (item.alumnos * item.grupos) + alumnos_now

        context = {
            'subsistema':           subsistema,
            'subsistema_historico': subsistema_historico,
            'especialidades_now':   especialidades_now,
            'grupos_now':           grupos_now,
            'alumnos_now':          alumnos_now,
            'revision_id':          revision_id
        }

        return context

    def json_to_response(self):
        context = self.get_data()

        query = context['subsistema_historico']

        data = [{
                    'subsistema': subs          ['subsistema'],
                    'anio': subs                ['anio'],
                    'total_grupos': subs        ['total_grupos'],
                    'total_especialidades': subs['total_especialidades'],
                    'total_alumnos': subs       ['total_alumnos']
                } for subs in query.order_by('anio')]

        return JsonResponse(data, safe=False)


class CompararOfertaEducativaView(generic.View):
    template_name = 'revision_ofertas_educativas/comparativa.html'

    def get(self, request, subsistema_id):
        subsistema = Subsistema.objects.get(pk=subsistema_id)
        revision = Revision.objects.get(pk=request.GET.get('revision'))

        # TODO: Almacenar el año en una variable o setting para poder hacer estas consultas dinámicas segun el año del proceso
        historicos = OfertaEducativaHistorico.objects.filter(subsistema=subsistema.referencia, anio=2016)
        for historico in historicos:
            try:
                oferta = OfertaEducativa.objects.get(especialidad__referencia=historico.especialidad, plantel__referencia=historico.plantel)
                if oferta is not None:
                    if oferta.poblacion_estudiantil_proyeccion_related.exists():
                        proyeccion_actual = oferta.poblacion_estudiantil_proyeccion_related.first()
                        if not oferta.activa:
                            historico.grupos_actual = 0
                            historico.alumnos_actual = 0
                            historico.total_por_grupo_actual = 0
                        else:
                            historico.grupos_actual = proyeccion_actual.grupos
                            historico.alumnos_actual = proyeccion_actual.alumnos
                            historico.total_por_grupo_actual = proyeccion_actual.alumnos * proyeccion_actual.grupos
                    else:
                        historico.grupos_actual = 0
                        historico.alumnos_actual = 0
                        historico.total_por_grupo_actual = 0
                else:
                    historico.grupos_actual = 0
                    historico.alumnos_actual = 0
                    historico.total_por_grupo_actual = 0
            except Exception as e:
                historico.grupos_actual = 0
                historico.alumnos_actual = 0
                historico.total_por_grupo_actual = 0

        especialidades_nuevas = OfertaEducativa.objects.prefetch_related('poblacion_estudiantil_proyeccion_related').select_related('plantel',
                                                                                                                                    'plantel__subsistema',
                                                                                                                                    'plantel__domicilio',
                                                                                                                                    'especialidad').filter(
            plantel__subsistema=subsistema).filter(activa=True).exclude(
            especialidad__referencia__in=OfertaEducativaHistorico.objects.filter(subsistema=subsistema.referencia, anio=2016).values_list(
                'especialidad',
                flat=True),
            plantel__referencia__in=OfertaEducativaHistorico.objects.filter(subsistema=subsistema.referencia, anio=2016).values_list('plantel',
                                                                                                                                     flat=True))

        context = {
            'revision':              revision,
            'historicos':            historicos,
            'subsistema':            subsistema,
            'especialidades_nuevas': especialidades_nuevas,

        }

        return render(request, self.template_name, context)


class RechazarOfertaEducativa(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, UtilsMixin, generic.View):
    def post(self, request):
        try:
            motivo_rechazo = request.POST.get("motivo_rechazo")
            pk = request.POST.get("revision_id")
            revision = RevisionOfertaEducativa.objects.get(pk=pk)
            revision.estado = "C"
            revision.comentario = motivo_rechazo
            revision.fecha_revision = datetime.date.today()
            revision.usuario_revision = request.user
            revision.save()
            return JsonResponse({'is_valid': True}, safe=False)
        except:
            raise


class AceptarOfertaEducativaView(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, UtilsMixin, generic.View):
    def post(self, request, revision_id):
        revision = RevisionOfertaEducativa.objects.get(pk=revision_id)
        revision.fecha_revision = datetime.datetime.today()
        revision.estado = "A"
        revision.usuario_revision = request.user
        revision.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
