# -*- encoding: utf-8 -*
import datetime
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.views import generic

from app.administracion.models import RevisionAforo, Revision
from app.planteles.models import Subsistema
from general.mixins import LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, UtilsMixin
from general.libseq.reporting import PentahoReport


class RevisionAforoListView(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, UtilsMixin, generic.ListView):
    model = RevisionAforo
    template_name = "revision_aforo/list.html"
    context_object_name = "revisiones"

    def get_queryset(self):
        queryset = self.model.objects.select_related('subsistema', 'usuario_apertura', 'usuario_revision')
        if 'estado' in self.request.GET:
            return queryset.filter(estado=self.request.GET.get('estado'))
        if 'subsistema' in self.request.GET:
            return queryset.filter(subsistema_id=self.request.GET.get('subsistema'))
        return queryset.filter(estado=Revision.ESTADO_EN_REVISION)

    def get_context_data(self, **kwargs):
        context = super(RevisionAforoListView, self).get_context_data(**kwargs)
        context.update({
            'subsistemas': Subsistema.objects.all(),
            'en_revision': self.model.objects.filter(estado=Revision.ESTADO_EN_REVISION).count()
        })

        return context


class ImpresionAforoView(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, UtilsMixin, generic.View):
    def get(self, request, pk):
        revision = RevisionAforo.objects.get(pk=pk)
        formato = request.GET.get('formato')

        diccionario_formato = {
            'pdf': 'application/pdf',
            'xls': 'application/vnd.ms-excel'
        }

        reporte = PentahoReport()
        reporte.name = 'aforo_avance_registro'
        reporte.set('id_subsistema', str(revision.subsistema.id))
        reporte.project = 'siem'
        reporte.format = formato
        response = HttpResponse(reporte.fetch())
        response['Content-Type'] = diccionario_formato[formato]
        response['Content-Disposition'] = 'attachment; filename = "aforo ({0}).{1}"'.format(
            revision.subsistema.referencia, formato)
        return response


class GuardarMotivoRechazo(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, UtilsMixin, generic.View):
    def post(self, request):
        try:
            motivo_rechazo = request.POST.get("motivo_rechazo")
            pk = request.POST.get("revision_id")
            revision = RevisionAforo.objects.get(pk=pk)
            revision.estado = "C"
            revision.comentario = motivo_rechazo
            revision.save()
            return JsonResponse({'is_valid': True}, safe=False)
        except:
            raise


class AceptarAforoView(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin,  UtilsMixin, generic.View):
    def post(self, request, pk):
        revision = RevisionAforo.objects.get(pk=pk)
        revision.fecha_revision = datetime.datetime.today()
        revision.estado = "A"
        revision.usuario_revision = request.user
        revision.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
