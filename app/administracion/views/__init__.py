# -*- encoding: utf-8 -*
from .aspirantes import *
from .dashboard import *
from .enlaces import *
from .etapas import *
from .folios_ceneval import *
from .miscelaneos import *
from .nivel_demanda import *
from .planteles import *
from .revision_aforo import *
from .revision_ofertas_educativas import *
from .sedes import *
from .subsistemas import *
from .usuarios import *
