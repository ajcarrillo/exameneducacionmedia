# -*- encoding: utf-8 -*
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views import generic

from app.administracion.models import Configuracion


class MiscelaneosView(generic.View):
    template_name = "miscelaneos/miscelaneos.html"

    def get_context(self):
        miscelaneos = Configuracion.objects.all()
        context = {'miscelaneos': miscelaneos}
        return context

    def get(self, request):
        context = self.get_context()
        return render(request, self.template_name, context)

    def post(self, request):
        transaction.set_autocommit(False)
        try:
            for configuracion in Configuracion.objects.all():
                valor = request.POST['row_id_%s' % configuracion.id]
                try:
                    configuracion.valor = valor if valor != '' else None
                    configuracion.save()
                except ValueError:
                    raise Exception('No se pudo guardar la configuracion de %s', configuracion.descripcion)

            transaction.commit()
            return HttpResponseRedirect(reverse('administracion:miscelaneos'))
        except Exception as e:
            context = self.get_context()
            context.update({'errors': e})
            transaction.rollback()
            return render(request, self.template_name, context)
