# -*- encoding: utf-8 -*-

import decimal

from django.contrib import messages
from django.contrib.auth.models import Group, User
from django.db import transaction
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic

from app.billy.models import SalarioMinimo, Concepto
from app.planteles.models import Plantel, Subsistema
from general.mixins import LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, GroupRequiredMixin


class PlantelListView(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, generic.ListView):
    model = Plantel
    template_name = "planteles/list.html"
    context_object_name = 'planteles'

    def get_queryset(self):
        queryset = self.model.objects.select_related('responsable', 'subsistema')
        if 'subsistema' in self.request.GET:
            return queryset.filter(subsistema__id=self.request.GET.get('subsistema'))

        if 'search' in self.request.GET:
            term = self.request.GET['search']
            return queryset.filter(Q(referencia__icontains=term) | Q(cct__icontains=term))

        return queryset

    def get_context_data(self, **kwargs):
        context = super(PlantelListView, self).get_context_data(**kwargs)
        salario_minimo = SalarioMinimo.objects.get(activo=True)
        concepto = Concepto.objects.get(clave='026')
        monto = concepto.salarios_minimos * salario_minimo.monto
        total = str(monto.quantize(decimal.Decimal('0'), rounding=decimal.ROUND_HALF_UP)),
        context.update({
            'total': total    [0],
            'concepto':       concepto,
            'subsistemas':    Subsistema.objects.all(),
            'salario_minimo': salario_minimo,
        })
        return context


class PlantelUpdateView(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, generic.View):
    def post(self, request, pk):
        try:
            plantel = Plantel.objects.get(pk=pk)
            plantel.descuento = request.POST.get('precio')
            plantel.save()
            data = {'is_valid': True}
        except Exception as e:
            data = {'is_valid': False, 'message': e.message}

        return JsonResponse(data, safe=False)


class PlantelChangeResponsible(LoginRequiredMixin, GroupRequiredMixin, generic.View):
    group_required = 'departamento'
    template_name = 'planteles/change_responsible.html'

    def get(self, request, pk):
        context = {
            'group':      Group.objects.values('pk').get(name__exact='plantel'),
            'plantel_id': pk,
        }
        return render(request, self.template_name, context)

    def post(self, request, pk):
        plantel = Plantel.objects.get(pk=pk)
        transaction.set_autocommit(False)
        try:
            first_name = request.POST.get('first_name')
            last_name = request.POST.get('last_name')
            email = request.POST.get('email')
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name

            user.save()

            plantel.responsable = user

            plantel.save()

            user.groups.clear()
            group = Group.objects.get(name='plantel')
            user.groups.add(group)
            messages.success(request, "El responsable del plantel se cambió correctamente")
            transaction.commit()
        except Exception as e:
            transaction.rollback()
            messages.error(request, e.message)
            return redirect(request.META.get('HTTP_REFERER'))

        return redirect(reverse('administracion:plantel_list'))
