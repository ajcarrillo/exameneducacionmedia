# -*- encoding: utf-8 -*
import datetime

from django.contrib import messages
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic

from app.administracion.models import EtapaProceso, RevisionOfertaEducativa, Revision, RevisionAforo
from app.aspirantes.models import Aspirante
from app.planteles.models import OfertaEducativa, Plantel
from general.mixins import GroupRequiredMixin, LoginRequiredMixin


class AdministracionDashboard(LoginRequiredMixin, GroupRequiredMixin, generic.TemplateView):
    group_required = 'departamento'
    template_name = "dashboard/dashboard.html"

    def get_context_data(self, **kwargs):
        context = super(AdministracionDashboard, self).get_context_data(**kwargs)
        today = datetime.date.today()
        etapa_oferta = EtapaProceso.objects.get(nombre='OFERTA')

        oferta_educativa = OfertaEducativa.objects.filter(activa=True).count()
        planteles = Plantel.objects.all()
        new_members = Aspirante.objects.select_related().filter(usuario__date_joined__day=today.day,
                                                                usuario__date_joined__month=today.month,
                                                                usuario__date_joined__year=today.year).count()

        planteles_list = Plantel.demanda_capacida_porcentaje()
        context.update({
            'planteles':            planteles.count(),
            'new_members':          new_members,
            'planteles_list':       planteles_list,
            'aforo_revision':       RevisionAforo.objects.filter(estado=Revision.ESTADO_EN_REVISION).count(),
            'is_etapa_oferta':      True if etapa_oferta.apertura <= today else False,
            'oferta_revision':      RevisionOfertaEducativa.objects.filter(estado=Revision.ESTADO_EN_REVISION).count(),
            'oferta_educativa':     oferta_educativa,
            'planteles_list_count': len(list(planteles_list))
        })
        return context


class DesactivarOfertaEducativa(LoginRequiredMixin, GroupRequiredMixin, generic.View):
    group_required = 'departamento'

    def post(self, request):
        transaction.set_autocommit(False)
        try:
            etapa_oferta = EtapaProceso.objects.get(nombre='OFERTA')
            today = datetime.date.today()
            if etapa_oferta.apertura <= today:
                raise Exception("La etapa de oferta ya inició por lo tanto no se puede desactivar la oferta educativa")
            oferta = OfertaEducativa.objects.all()
            for item in oferta:
                item.activa = False
                item.save()
            transaction.commit()
            messages.success(request, 'La oferta se reinició correctamente')
            return HttpResponseRedirect(reverse('administracion:dashboard'))
        except Exception as e:
            transaction.rollback()
            messages.warning(request, e.message)
            return redirect(request.META.get('HTTP_REFERER'))
