# -*- encoding: utf-8 -*
from django.contrib import messages
from django.shortcuts import redirect
from django.views import generic

from app.mgeo.models import Municipio, NivelDeDemanda
from general.mixins import LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin


class MunicipioListView(LoginRequiredMixin, NeedsDepartamentoGroupAccessMixin, generic.ListView):
    model = NivelDeDemanda
    template_name = "nivel_demanda/list.html"
    context_object_name = "municipios"

    def get_queryset(self):
        return self.model.objects.select_related('cabecera').filter(entidad_federativa__id=23)

    def post(self, request):
        municipio = self.model.objects.get(pk=request.POST.get('municipio'))
        municipio.nivel_de_demanda = request.POST.get('nivel_demanda')
        municipio.save()
        messages.add_message(request, messages.INFO, 'El nivel de demanda se actualizó correctamente')
        return redirect(request.META.get('HTTP_REFERER'))
