# -*- encoding: utf-8 -*
from django import forms
from django.contrib.auth.models import User, Group

from app.administracion.models import Configuracion, Enlace
from app.mgeo.forms import DomicilioForm
from app.mgeo.models import Municipio, Localidad
from app.planteles.models import Subsistema, Plantel
from exameneducacionmedia.utils import BetterForm


class UserForm(forms.ModelForm):
    first_name = forms.CharField(label='Nombre',
                                 widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required', 'autofocus': 'autofocus'}))
    last_name = forms.CharField(label='Apellidos', widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(label='Email', widget=forms.EmailInput(attrs={'class': 'form-control', 'required': 'required'}))
    username = forms.CharField(label='Usuario', widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}))
    password = forms.CharField(label='Contraseña', widget=forms.PasswordInput(attrs={'class': 'form-control', 'required': 'required'}))
    groups = forms.ModelMultipleChoiceField(Group.objects, label='Grupos',
                                            widget=forms.SelectMultiple(attrs={'class': 'form-control', 'required': 'required'}))

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'password', ]


class SubsistemaModelForm(forms.ModelForm):
    referencia = forms.CharField(label='Referencia',
                                 widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required', 'autofocus': 'autofocus', 'maxlength': '30'}))
    descripcion = forms.CharField(label='Descripción', widget=forms.Textarea(attrs={'class': 'form-control', 'required': 'required', 'maxlength': '255'}))

    class Meta:
        model = Subsistema
        fields = ['referencia', 'descripcion', ]


class SedeAlternaForm(BetterForm, DomicilioForm):
    plantel = forms.ModelChoiceField(Plantel.objects, label='Plantel', widget=forms.Select(attrs={'class': 'form-control', 'required': 'required'}))
    referencia = forms.CharField(label='Sede', widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required', 'autofocus': 'autofocus', 'maxlength': '30'}))


class EnlaceForm(BetterForm, forms.Form):
    municipio = forms.ModelChoiceField(Municipio.objects.filter(entidad_federativa=23), empty_label="Municipio", widget=forms.Select(attrs={'class': 'form-control'}), label="Municipio")
    domicilio = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'class': 'form-control'}), label="Domicilio")
    telefono = forms.CharField(max_length=10, widget=forms.TextInput(attrs={'class': 'form-control'}), label="Telefono")
    extension = forms.CharField(max_length=5, widget=forms.TextInput(attrs={'class': 'form-control'}), label="Extension")
    fechas = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label="Fechas")
