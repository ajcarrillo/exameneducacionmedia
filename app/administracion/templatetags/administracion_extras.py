# -*- encoding: utf-8 -*-

from __future__ import unicode_literals

import decimal

from django import template
from django.utils.html import format_html, format_html_join

from app.administracion.models import RevisionAforo, RevisionOfertaEducativa, Revision, EtapaProceso
from app.planteles.models import PoblacionEstudiantilProyeccion

register = template.Library()


@register.simple_tag(name='oferta_en_revision')
def oferta_en_revision():
    alert = ""
    count = RevisionOfertaEducativa.objects.filter(estado=Revision.ESTADO_EN_REVISION).count()
    if count > 0:
        alert = format_html("<i class='label pull-right bg-green'>{}</i>", count)

    return alert


@register.simple_tag(name='aforo_en_revision')
def oferta_en_revision():
    alert = ""
    count = RevisionAforo.objects.filter(estado=Revision.ESTADO_EN_REVISION).count()
    if count > 0:
        alert = format_html("<i class='label pull-right bg-green'>{}</i>", count)

    return alert


@register.simple_tag()
def aforo_aceptar(revision):
    boton = format_html("<button type='button' class='btn btn-success disabled'><i class='glyphicon glyphicon-check'></i>Aceptar</button>")
    if revision.estado == "R":
        boton = format_html("<button type='submit' class='btn btn-success'><i class='glyphicon glyphicon-check'></i>Aceptar</button>")

    return boton


@register.simple_tag()
def aforo_rechazar(revision):
    boton = format_html("<button type='button' class='btn btn-danger disabled'><i class='glyphicon glyphicon-remove'></i>Rechazar</button>")
    if revision.estado == "R":
        boton = format_html("<button type='button' class='btn btn-danger' data-button='motivo-rechazo'><i class='glyphicon glyphicon-remove'></i>Rechazar</button>")

    return boton


@register.simple_tag()
def oferta_aceptar(revision):
    boton = format_html("<button type='button' class='btn btn-success disabled'><i class='glyphicon glyphicon-check'></i> Aceptar</button>")
    if revision.estado == "R":
        boton = format_html("<button type='submit' class='btn btn-success'><i class='glyphicon glyphicon-check'></i> Aceptar</button>")
    return boton


@register.simple_tag()
def oferta_rechazar(revision):
    boton = format_html("<button type='button' class='btn btn-danger disabled'><i class='glyphicon glyphicon-remove'></i> Rechazar</button>")
    if revision.estado == "R":
        boton = format_html("<button type='button' class='btn btn-danger' data-button='motivo-rechazo'><i class='glyphicon glyphicon-remove'></i> Rechazar</button>")

    return boton


@register.simple_tag()
def plantel_precio(descuento_por_ciento, precio_examen):
    descuento = (int(precio_examen) * descuento_por_ciento) / 100
    return int(precio_examen) - descuento


@register.simple_tag()
def stage_date(etapa=None):
    if etapa is None:
        return ""

    etapa = etapa.upper()
    string = "Apertura: {0} - Cierre: {1}"
    try:
        etapa = EtapaProceso.objects.get(nombre__exact=etapa)
        return string.format(etapa.apertura, etapa.cierre)
    except EtapaProceso.DoesNotExist:
        return ""


@register.simple_tag()
def oferta_validacion(municipio, subsistema_id, subsitema_referencia):
    proyeccion = PoblacionEstudiantilProyeccion.objects.raw(u'''
        SELECT
        pep.id,
        m.nombre municipio,
        l.nombre localidad,
        s.referencia subsistema,
        p.cct cct,
        p.referencia plantel,
        e.referencia especialidad,
        CONCAT(d.calle,
                ' #',
                d.numero,
                ' ',
                d.colonia,
                ' C.P. ',
                d.codigo_postal) domicilio,
        pep.grupos,
        pep.alumnos,
        (pep.grupos * pep.alumnos) as total_alumnos,
        ifnull((
        select grupos
        from poblacion_estudiantil_historico peh
        where peh.municipio = m.nombre
        and peh.localidad = l.nombre
        and peh.subsistema = s.referencia
        and peh.cct = p.cct
        and peh.plantel = p.referencia
        and peh.especialidad = e.referencia
        and peh.anio = 2016
        ), "0") as grupos_historico,
        ifnull((
        select alumnos_por_grupo
        from poblacion_estudiantil_historico peh
        where peh.municipio = m.nombre
        and peh.localidad = l.nombre
        and peh.subsistema = s.referencia
        and peh.cct = p.cct
        and peh.plantel = p.referencia
        and peh.especialidad = e.referencia
        and peh.anio = 2016
        ), "0") as alumnos_historico,
        ifnull((
        select alumnos_total
        from poblacion_estudiantil_historico peh
        where peh.municipio = m.nombre
        and peh.localidad = l.nombre
        and peh.subsistema = s.referencia
        and peh.cct = p.cct
        and peh.plantel = p.referencia
        and peh.especialidad = e.referencia
        and peh.anio = 2016
        ), "0") as alumnos_total_historico
    FROM exameneducacionmedia.poblacion_estudiantil_proyeccion pep
            LEFT JOIN exameneducacionmedia.plantel_especialidad o ON o.id = pep.oferta_educativa
            AND o.activa = 1
            LEFT JOIN exameneducacionmedia.plantel p ON o.plantel = p.id
            LEFT JOIN exameneducacionmedia.subsistema s ON p.subsistema = s.id
            LEFT JOIN exameneducacionmedia.especialidad e ON o.especialidad = e.id
            LEFT JOIN exameneducacionmedia.poblacion_estudiantil s1 ON o.id = s1.oferta_educativa
            AND s1.semestre = 1
            LEFT JOIN exameneducacionmedia.domicilio d ON p.domicilio = d.id
            LEFT JOIN mginegi.localidad l ON d.localidad_id = l.id
            LEFT JOIN mginegi.municipio m ON l.municipio = m.id
            where s.id = ''' + str(subsistema_id) + u'''
            and m.nombre = "''' + municipio + u'''"
    union
    select
    concat ("") as id,
    peh.municipio,
    peh.localidad,
    peh.subsistema,
    peh.cct,
    peh.plantel,
    peh.especialidad,
    peh.domicilio,
    concat ("0") as grupos,
    concat ("0") as alumnos,
    concat ("0") as total_alumnos,
    peh.grupos,
    peh.alumnos_por_grupo,
    peh.alumnos_total as alumnos_total_hisorico
    from poblacion_estudiantil_historico peh
            where peh.anio=2016
            and peh.municipio = "''' + municipio + u'''"
            and peh.subsistema = "''' + subsitema_referencia + u'''"

            and concat(
            peh.municipio,
            peh.localidad,
            peh.subsistema,
            peh.cct,
            peh.plantel,
            peh.especialidad) not in (
            SELECT
            concat(
            m.nombre,
            l.nombre,
            s.referencia,
            p.cct,
            p.referencia,
            e.referencia
            ) as reference
    FROM exameneducacionmedia.poblacion_estudiantil_proyeccion pep
            LEFT JOIN exameneducacionmedia.plantel_especialidad o ON o.id = pep.oferta_educativa
            AND o.activa = 1
            LEFT JOIN exameneducacionmedia.plantel p ON o.plantel = p.id
            LEFT JOIN exameneducacionmedia.subsistema s ON p.subsistema = s.id
            LEFT JOIN exameneducacionmedia.especialidad e ON o.especialidad = e.id
            LEFT JOIN exameneducacionmedia.poblacion_estudiantil s1 ON o.id = s1.oferta_educativa
            AND s1.semestre = 1
            LEFT JOIN exameneducacionmedia.domicilio d ON p.domicilio = d.id
            LEFT JOIN mginegi.localidad l ON d.localidad_id = l.id
            LEFT JOIN mginegi.municipio m ON l.municipio = m.id
            where s.id = ''' + str(subsistema_id) + u'''
            and m.nombre = "''' + municipio + u'''"
            )''')

    # if len(list(proyeccion)) == 0:
    # return ""

    grupos = 0
    alumnos = 0
    grupos_historico = 0
    alumnos_historico = 0
    total_alumnos = 0
    total_alumnos_historico = 0
    for oferta in proyeccion:
        grupos += int(oferta.grupos)
        alumnos += int(oferta.alumnos)
        grupos_historico += int(oferta.grupos_historico)
        alumnos_historico += 0 if oferta.alumnos_historico is None else int(oferta.alumnos_historico)
        total_alumnos += int(oferta.total_alumnos)
        total_alumnos_historico += int(oferta.alumnos_total_historico)

    table = format_html_join('', u""
                                 u"<tr>"
                                 u"<td>{}</td>"
                                 u"<td>{}</td>"
                                 u"<td>{}</td>"
                                 u"<td>{}</td>"
                                 u"<td>{}</td>"
                                 u"<td>{}</td>"
                                 u"<td>{}</td>"
                                 u"<td>{}</td>"
                                 u"<td>{}</td>"
                                 u"<td>{}</td>"
                                 u"<td>{}</td>"
                                 u"<td>{}</td>"
                                 u"<td>{}</td>"
                                 u"</tr>", (
                                 (u.municipio,
                                  u.localidad,
                                  u.subsistema,
                                  u.cct,
                                  u.plantel,
                                  u.domicilio,
                                  u.especialidad,
                                  u.grupos,
                                  u.alumnos,
                                  u.total_alumnos,
                                  u.grupos_historico,
                                  u.alumnos_historico,
                                  u.alumnos_total_historico
                                  ) for u in proyeccion))

    table += format_html('''
    <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>{}</td>
    <td>{}</td>
    <td></td>
    <td>{}</td>
    <td>{}</td>
    <td></td>
    <td>{}</td>
    </tr>''', len(list(proyeccion)), grupos, total_alumnos, grupos_historico, total_alumnos_historico)

    return table
