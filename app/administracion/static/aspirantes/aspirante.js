"use strict";
/**
 * Created by andres on 19/12/16.
 */

(function () {
	var $paymentStatusSelect = $('#select-payment-status');

	$paymentStatusSelect.on("change", onChangePaymenteStatusSelect);

	function onChangePaymenteStatusSelect() {
		var $form = $('#payment-status-form'),
			data = $form.serialize(),
			url = $form.attr('action');

		$.post(url, data, function (response) {
			if (response.is_valid){
				$.notify({
					message: "El pago se ha actualizado"
				},{
					type: "success"
				});
			}else{
				$.notify({
					message: response.msg
				},{
					type: "warning"
				});
			}
		}).fail(function (response) {
			console.log(response.msg);
			$.notify({
				message: "Lo sentimos ha ocurrido un error, intenta de nuevo"
			},{
				type: "warning"
			});
		})
	}
}());
