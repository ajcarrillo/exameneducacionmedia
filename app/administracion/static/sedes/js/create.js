"use strict";
/**
 * Created by andres on 18/08/16.
 */

(function () {
	var $municipio = $('#municipio').select2({placeholder: "Seleccione..."}),
		$localidad = $('#localidad').select2({placeholder: "Seleccione..."}),
		$plantel   = $('#plantel').select2({placeholder: "Seleccione..."});

	$municipio.on("select2:select", function (evt) {
		var url         = $('#get-json-localidades').data("url"),
			municipioId = $(this).val();

		$.getJSON(url + municipioId, '', function (json) {
			$localidad.empty();
			$localidad.select2({data: json})
		})
	});
}());
