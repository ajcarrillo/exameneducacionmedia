/**
 * Created by andres on 15/08/16.
 */
"use strict";

(function () {
	var registroApertura,
		registroCierre,
		aforoApertura,
		aforoCierre,
		ofertaApertura,
		ofertaCierre,
		form;

	registroApertura = document.getElementById('REGISTRO-apertura');
	registroCierre   = document.getElementById('REGISTRO-cierre');
	aforoApertura    = document.getElementById('AFORO-apertura');
	aforoCierre      = document.getElementById('AFORO-cierre');
	ofertaApertura   = document.getElementById('OFERTA-apertura');
	ofertaCierre     = document.getElementById('OFERTA-cierre');
	form             = document.getElementById('etapas-form');

	$('.datepicker').datepicker({
		format: 'yyyy-mm-dd',
		language: 'es',
		todayHighlight: true
	}).on("hide", function (e) {
		checkRegistro();
		checkAforo();
		checkOferta();
	});

	var checkRegistro = function () {
		var apertura = toDate(registroApertura.value),
			cierre   = toDate(registroCierre.value);

		registroApertura.setCustomValidity("");
		registroCierre.setCustomValidity("");

		/*Validar que el año sea el correcto*/
		if (!isValidYear(registroApertura.value)) {
			registroApertura.setCustomValidity(registroApertura.validationMessage + " El año es inválido");
		}
		if (!isValidYear(registroCierre.value)) {
			registroCierre.setCustomValidity(registroCierre.validationMessage + " El año es inválido");
		}

		/*Validar que el cierre sea mayor a la apertura y que no se aperture y cierre el mismo día*/
		if (apertura >= cierre) {
			registroCierre.setCustomValidity(registroCierre.validationMessage + " La fecha de cierre es inválida")
		}

		/*Validamos que el registro se aperture unicamente hasta que las demás etapas hayan concluido*/
		if (apertura <= toDate(ofertaCierre.value) || apertura <= toDate(aforoCierre.value)) {
			registroApertura.setCustomValidity(registroApertura.validationMessage + " El registro no puede aperturarse hasta que finalicen las etapas de AFORO y OFERTA");
		}
	};

	var checkOferta = function () {
		var apertura = toDate(ofertaApertura.value),
			cierre   = toDate(ofertaCierre.value);

		ofertaApertura.setCustomValidity("");
		ofertaCierre.setCustomValidity("");

		/*Validar año*/
		if (!isValidYear(ofertaApertura.value)) {
			ofertaApertura.setCustomValidity("El año es inválido");
		}
		if (!isValidYear(ofertaCierre.value)) {
			ofertaCierre.setCustomValidity("El año es inválido");
		}

		/*Validar que el cierre sea mayor a la apertura y que no se aperture y cierre el mismo día*/
		if (apertura >= cierre) {
			ofertaCierre.setCustomValidity("La fecha de cierre es inválida")
		}

		/*Validamos que la oferta inicie primeramente que todos o igual que el aforo y que la etapa de registro inicie despues de la oferta*/
		if (apertura > toDate(aforoApertura.value) || apertura >= toDate(registroApertura.value)) {
			ofertaApertura.setCustomValidity("La fecha de apertura de la OFERTA debe aperturarse antes que la etapa AFORO")
		}
	};

	var checkAforo = function () {
		var apertura = toDate(aforoApertura.value),
			cierre   = toDate(aforoCierre.value);

		aforoApertura.setCustomValidity("");
		aforoCierre.setCustomValidity("");

		/*Validar año*/
		if (!isValidYear(aforoApertura.value)) {
			aforoApertura.setCustomValidity("El año es inválido");
		}
		if (!isValidYear(aforoCierre.value)) {
			aforoCierre.setCustomValidity("El año es inválido");
		}

		/*Validar que el cierre sea mayor a la apertura y que no se aperture y cierre el mismo día*/
		if (apertura >= cierre) {
			aforoApertura.setCustomValidity("La fecha de apertura es inválida")
		}

		/*Validamos que el aforo no cierre antes que la oferta educativa*/
		if (toDate(ofertaCierre.value) > cierre) {
			ofertaCierre.setCustomValidity("El AFORO no debe cerrar antes que la etapa de OFERTA")
		}
	};

	form.addEventListener('submit', function () {
		checkRegistro();
		checkAforo();
		checkOferta();
		if (!this.checkValidity()) {
			event.preventDefault();
		}
	}, false);

	function isValidYear(date) {
		var today      = new Date(),
			actualYear = today.getFullYear(),
			parts      = date.split("-");

		return (parts[0] == actualYear);
	}

	function toDate(dateStr) {
		var parts = dateStr.split("-");
		return Date.UTC(parts[0], parts[1] - 1, parts[2]);
	}
}());
