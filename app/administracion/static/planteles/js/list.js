"use strict";
/**
 * Created by andres on 27/10/16.
 */

(function () {
	var $slider = $('[data-slider-id="discount-slider"]');
	$slider.slider();

	$slider.on("slide, slideStop", function (e) {
		var plantel    = $(this).data('plantel');
		var total      = $(this).data('total');
		var $txtPrecio = $('[data-precio="' + plantel + '"]');
		var descuento  = e.value;
		var precio     = (total * descuento) / 100;
		var $label     = $('[data-label="' + plantel + '"]');
		var $form      = $('[data-form="' + plantel + '"]');
		var url        = $form.attr("action");
		var data       = $form.serialize();

		$label.text(e.value + '%');
		$txtPrecio.val(Math.ceil(total - precio));

		$.post(url, data, function (response) {
			if (response.is_valid) {
				$.notify({
					message: "El descuento se actualizó correctamente"
				},{
					type: "success"
				});
			} else {
				$.notify({
					message: "Lo sentimos algo ha salido mal, intenta de nuevo"
				},{
					type: "warning"
				});
			}
		}).fail(function (response) {
			$.notify({
				message: "Lo sentimos algo ha salido mal, intenta de nuevo"
			},{
				type: "warning"
			});
			console.log(response);
		})

	});
}());
