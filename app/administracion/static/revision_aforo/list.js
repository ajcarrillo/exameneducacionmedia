"use strict";
/**
 * Created by lewis on 14/09/16.
 */

(function () {
    var $btnMotivoRechazo = $("[data-button='motivo-rechazo']");
    $btnMotivoRechazo.on("click", onClickBtnMotivoRechazo);

    function onClickBtnMotivoRechazo() {
        var motivo_rechazo = "",
            data = "",
            $formMotivoRechazo = $(this).closest("form"),
            $inputMotivoRechazo = $formMotivoRechazo.find("#motivo_rechazo");
        bootbox.dialog({
            message: "" +
            "<form data-form='motivo-r'>" +
            "<div id='error'>" +
            "<label class='control-label' for='motivo-rechazo'>" +
            "</label>" +
            "<textarea class='form-control' id='motivo-rechazo' required></textarea>" +
            "<span class='help-block' style='visibility: hidden' id='message-error'>El motivo de rechazo no puede ser vacío</span>" +
            "</div>" +
            "</form>",



            title: "Motivo de rechazo",
            buttons: {
                danger: {
                    label: "Confirmar",
                    className: "",
                    callback: function () {
                        motivo_rechazo = $("#motivo-rechazo").val();
                        $inputMotivoRechazo.val(motivo_rechazo);
                        data = $formMotivoRechazo.serialize();

                        if(motivo_rechazo.length === 0 || motivo_rechazo === "") {
                            $("#error").attr("class", "form-group has-error");
                            $("#message-error").attr("style", "visibility: visible");
                            return false;
                        }

                        $("#error").removeAttr("class");
                        $("#message-error").hide();


                        $.ajax({
                            url: "/administracion/revisiones/aforo/motivo-rechazo/",
                            type: "POST",
                            data: data

                        })
                            .done(function (response) {
                                if (!response.is_valid) {
                                    bootbox.alert({
                                        message: response.error,
                                        title: "Advertencia!",
                                        buttons: {
                                            ok: {
                                                label: "Aceptar",
                                                className: "btn-default"
                                            }
                                        }
                                    });
                                } else {
                                    location.reload();
                                }
                            })
                            .fail(function (xhr) {
                                console.log(xhr);
                            });
                    },
                    success: {
                        label: "Cancelar",
                        className: "btn-default",
                        callback: function () {
                            console.log("dont post");
                        }
                    }

                }
            }
        });
    }
}());
