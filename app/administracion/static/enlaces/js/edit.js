"use strict";
/**
 * Created by andres on 01/10/16.
 */

(function () {
	var $fechas = $('#fechas');
	$fechas.daterangepicker({
			timePicker24Hour: true,
			timePicker: true,
			timePickerIncrement: 30,
			locale: {
				format: "YYYY-MM-DD H:mm",
				applyLabel: "Aceptar",
				cancelLabel: "Cancelar",
				fromLabel: "De",
				toLabel: "Hasta",
				daysOfWeek: [
					"Dom",
					"Lun",
					"Mar",
					"Mie",
					"Jue",
					"Vie",
					"Sab"
				],
				monthNames: [
					"Enero",
					"Febrero",
					"Marzo",
					"Abril",
					"Mayo",
					"Junio",
					"Julio",
					"Agosto",
					"Septiembre",
					"Octubre",
					"Noviembre",
					"Diciembre"
				]
			}
		}
	);
	$fechas.on("apply.daterangepicker", onApplyDatePicker);
	$fechas.on("hide.daterangepicker", onApplyDatePicker);

	function onApplyDatePicker(ev, picker) {
		$('#fecha_inicio').val(picker.startDate.format("YYYY-MM-DD"));
		$('#fecha_fin').val(picker.endDate.format("YYYY-MM-DD"));
		$('#hora_inicio').val(picker.startDate.format("H:mm"));
		$('#hora_fin').val(picker.endDate.format("H:mm"));
	}
}());
