"use strict";
/**
 * Created by andres on 01/10/16.
 */

(function () {
	var $deleteEnlace = $('[data-button="delete-enlace"]');

	$deleteEnlace.on("click", onClickDeleteButton);

	function onClickDeleteButton() {
		var formId = $(this).data('form');
		bootbox.confirm({
			message: "¿Desea eliminar el enlace, esta acción no se puede deshacer?",
			buttons: {
				confirm: {
					label: 'Si',
					className: 'btn-success'
				},
				cancel: {
					label: 'No',
					className: 'btn-danger'
				}
			},
			callback: function (response) {
				if (response) {
					var $form = $(formId),
						url   = $form.attr('action');
					$.post(url, $form.serialize(), function (response) {
						if (response.is_valid) {
							location.reload()
						}
					}).fail(function () {
						bootbox.alert("Lo sentimos ha ocurrido un error, intente de nuevo")
					});
				}
			}
		});
	}

}());
