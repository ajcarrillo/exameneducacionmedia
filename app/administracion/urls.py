# -*- encoding: utf-8 -*
from django.conf.urls import url

from app.administracion import views

# **************************************
#
#   url: /administracion/
#   namespace: administracion
#
# **************************************

urlpatterns = [
    url(r'^aspirantes/$', views.AspiranteListView.as_view(), name='aspirantes_list_view'),
    url(r'^aspirantes/(?P<aspirante_id>[\d]+)/$', views.AspiranteTemplateView.as_view(), name='aspirantes_detail'),
    url(r'^aspirantes/(?P<aspirante_id>[\d]+)/update-payment/$', views.UpdatePayment.as_view(), name='aspirantes_update_payment'),

    url(r'^etapas/$', views.Etapa.as_view(), name='etapas'),

    url(r'^nivel-de-demanda/$', views.MunicipioListView.as_view(), name='list_municipios'),

    url(r'^panel-de-control/$', views.AdministracionDashboard.as_view(), name='dashboard'),
    url(r'^panel-de-control/desactivar-oferta/$', views.DesactivarOfertaEducativa.as_view(), name='dashboard_desactivar_oferta'),

    url(r'^planteles/$', views.PlantelListView.as_view(), name='plantel_list'),
    url(r'^planteles/cambiar-responsable/(?P<pk>[\d]+)/$', views.PlantelChangeResponsible.as_view(), name='plantel_responsible'),
    url(r'^planteles/precio/(?P<pk>[\d]+)/$', views.PlantelUpdateView.as_view(), name='plantel_precio'),

    url(r'^revisiones/aforo/$', views.RevisionAforoListView.as_view(), name='revision_aforo'),
    url(r'^revisiones/aforo/aceptar/(?P<pk>[\d]+)/$', views.AceptarAforoView.as_view(), name='aforo_aceptar'),
    url(r'^revisiones/aforo/impresion/(?P<pk>[\d]+)/$', views.ImpresionAforoView.as_view(), name='impresion_aforo'),
    url(r'^revisiones/aforo/motivo-rechazo/$', views.GuardarMotivoRechazo.as_view(), name='aforo_motivo_rechazo'),
    url(r'^revisiones/oferta/$', views.RevisionOfertaEducativaListView.as_view(), name='revision_oferta'),
    url(r'^revisiones/oferta/subsistema/(?P<subsistema_id>[\d]+)/$', views.RevisionOfertaEducativaSubsistemaListView.as_view(), name='revision_oferta_subsistema'),
    url(r'^revisiones/oferta/subsistema/(?P<subsistema_id>[\d]+)/comparativa/$', views.CompararOfertaEducativaView.as_view(), name='revision_oferta_comparar'),
    url(r'^revisiones/oferta/subsistema/(?P<revision_id>[\d]+)/aceptar/$', views.AceptarOfertaEducativaView.as_view(), name='revision_oferta_aceptar'),
    url(r'^revisiones/oferta/subsistema/rechazar/$', views.RechazarOfertaEducativa.as_view(), name='revision_oferta_rechazar'),

    url(r'^sedesalternas/$', views.SedesListView.as_view(), name='sedes_list'),
    url(r'^sedesalternas/nuevo/$', views.SedesCreateView.as_view(), name='sedes_create'),
    url(r'^sedesalternas/editar/(?P<pk>[\d]+)/$', views.SedesEditView.as_view(), name='sedes_edit'),

    url(r'^subsistemas/$', views.SubsistemaListView.as_view(), name='subsistemas_list'),
    url(r'^subsistemas/nuevo$', views.SubsistemaCreateView.as_view(), name='subsistemas_create'),
    url(r'^subsistemas/editar/(?P<pk>[\d]+)/$', views.SubsistemaUpdateView.as_view(), name='subsistemas_update'),

    url(r'^usuarios/$', views.UsuarioListView.as_view(), name='users'),
    url(r'^usuarios/nuevo/$', views.UsuarioCreateView.as_view(), name='new_user'),
    url(r'^usuarios/cambiar-contrasena/(?P<pk>[\d]+)/$', views.UpdatePassword.as_view(), name='update_password'),
    url(r'^usuarios/(?P<pk>[\d]+)/$', views.UserUpdateView.as_view(), name='update_user'),

    url(r'^configuraciones/miscelaneos/$', views.MiscelaneosView.as_view(), name='miscelaneos'),

    url(r'^configuraciones/enlaces/$', views.EnlacesListView.as_view(), name='enlaces'),
    url(r'^configuraciones/enlaces/nuevo$', views.EnlaceCreateView.as_view(), name='enlaces_create'),
    url(r'^configuraciones/enlaces/editar/(?P<pk>[\d]+)/$', views.EnlaceEditView.as_view(), name='enlaces_edit'),
    url(r'^configuraciones/enlaces/eliminar/(?P<pk>[\d]+)/$', views.EnlaceDeleteView.as_view(), name='enlaces_delete'),
]
