# -*- encoding: utf-8 -*
from __future__ import unicode_literals

import datetime
from django.contrib.auth.models import User
from django.core.exceptions import ImproperlyConfigured
from django.db import models
from django.db.models import Q, Max

from app.aspirantes.models import Aspirante, SeleccionOfertaEducativa
from app.mgeo.models import Municipio
from app.planteles.models import Subsistema


class Revision(models.Model):
    ESTADO_EN_REVISION = 'R'
    ESTADO_ACEPTADO = 'A'
    ESTADO_CANCELADO = 'C'
    CHOICES_ESTADO = (
        (ESTADO_ACEPTADO, 'Aceptado'),
        (ESTADO_EN_REVISION, 'En revisión'),
        (ESTADO_CANCELADO, 'Cancelado'),
    )
    id = models.AutoField(db_column='id', primary_key=True)
    usuario_apertura = models.ForeignKey(User, db_column='usuario_apertura', related_name='envios_realizados', on_delete=models.DO_NOTHING)
    fecha_apertura = models.DateField(db_column='fecha_apertura')
    usuario_revision = models.ForeignKey(User, db_column='usuario_revision', null=True, related_name='envios_revisados', on_delete=models.DO_NOTHING)
    fecha_revision = models.DateField(db_column='fecha_revision', null=True)
    estado = models.CharField(db_column='estado', max_length=1, choices=CHOICES_ESTADO, default=ESTADO_EN_REVISION)
    comentario = models.CharField(db_column='comentario', max_length=255, null=True)

    class Meta:
        db_table = 'revision'

    def descripcion_estado(self):
        for tuple in self.CHOICES_ESTADO:
            if tuple[0] == self.estado:
                return tuple[1]
        return 'Desconocido'


class RevisionOfertaEducativa(Revision):
    revision = models.OneToOneField(Revision, db_column='revision', primary_key=True, parent_link=True, on_delete=models.DO_NOTHING)
    subsistema = models.ForeignKey(Subsistema, db_column='subsistema', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'revision_oferta_educativa'


class RevisionAforo(Revision):
    revision = models.OneToOneField(Revision, db_column='revision', primary_key=True, parent_link=True, on_delete=models.DO_NOTHING)
    subsistema = models.ForeignKey(Subsistema, db_column='subsistema', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'revision_aforo'


class RevisionRegistro(Revision):
    revision = models.OneToOneField(Revision, db_column='revision', on_delete=models.DO_NOTHING)
    aspirante = models.OneToOneField(Aspirante, db_column='aspirante', on_delete=models.DO_NOTHING, related_name='revision_aspirante_related')
    solicitud_pago = models.IntegerField(db_column='solicitud_pago', unique=True)
    pago_efectuado = models.BooleanField(db_column='efectuado', default=False)

    class Meta:
        db_table = 'revision_registro'

    @staticmethod
    def revision_activa(aspirante):
        revisiones = RevisionRegistro.objects.filter(
            Q(estado=Revision.ESTADO_EN_REVISION) | Q(estado=Revision.ESTADO_ACEPTADO), aspirante=aspirante)
        if revisiones.exists():
            if revisiones.count() > 1:
                raise Exception('No puedes tener más de una revisión activa (en revisión o aceptada)')
            return revisiones.get()
        return None


class EtapaProceso(models.Model):
    id = models.AutoField(db_column='id', primary_key=True)
    nombre = models.CharField(db_column='nombre', max_length=50)
    descripcion = models.CharField(db_column='descripcion', max_length=255, null=True)
    apertura = models.DateField(db_column='apertura', null=True)
    cierre = models.DateField(db_column='cierre', null=True)

    class Meta:
        db_table = 'etapa_proceso'

    @staticmethod
    def is_etapa(etapa_name):
        today = datetime.date.today()
        etapa = EtapaProceso.objects.get(nombre=etapa_name.upper())
        if etapa.apertura <= today <= etapa.cierre:
            return True
        return False

    def asegurar_envio(self, entidad, validar_informacion=True):
        # asegura tanto que se encuentre en el periodo del proceso, como las restricciones particulares
        # para cada etapa, como estado de los envíos a revisión, cumplimiento con etapas previas
        self.asegurar_periodo()
        if self.nombre == 'OFERTA':
            self._assert_envio_oferta(entidad)
        elif self.nombre == 'AFORO':
            self._assert_envio_aforo(entidad)
        # el envío de registro es el único que valida automáticamente la información enviada
        # y no espera que un administrador lo haga, por lo tanto, aparte de asegurar el envío
        # o uno ya existente, se le puede pedir que valide completamente la información
        # enviada para que cumpla con las características y requerimientos del proceso
        elif self.nombre == 'REGISTRO':
            self._assert_envio_registro(entidad, validar_informacion)
        else:
            raise Exception('Etapa del proceso no reconocida')

    def asegurar_captura_completa(self):
        if self.nombre == 'OFERTA':
            self._asegurar_oferta_completa()
        elif self.nombre == 'AFORO':
            self._asegurar_aforo_completo()
        else:
            raise Exception('Etapa no reconocida')

    def _asegurar_oferta_completa(self):
        incompletos = []
        for subsistema in Subsistema.objects.all():
            if not RevisionOfertaEducativa.objects.filter(subsistema=subsistema, estado=Revision.ESTADO_ACEPTADO).exists():
                incompletos.append(subsistema.referencia)
        if len(incompletos) > 0:
            raise Exception('No se ha terminado la captura de la oferta educativa de los siguientes subsistemas: {0}'.format(', '.join(incompletos)))

    def _asegurar_aforo_completo(self):
        incompletos = []
        for subsistema in Subsistema.objects.all():
            if not RevisionAforo.objects.filter(subsistema=subsistema, estado=Revision.ESTADO_ACEPTADO).exists():
                incompletos.append(subsistema.referencia)
        if len(incompletos) > 0:
            raise Exception(
                'No se ha terminado la captura del aforo para la aplicación del examen de admisión de los siguientes subsistemas: {0}'.format(
                    ', '.join(incompletos)))

    def asegurar_periodo(self):
        hoy = datetime.date.today()
        desc = 'Esta etapa del PAEMS ({0})'.format(self.nombre)
        if self.apertura is None:
            raise Exception('{0} no tiene fecha de apertura. {1}.'.format(desc, self))
        elif self.cierre is None:
            raise Exception('{0} no tiene fecha de cierre. {1}.'.format(desc, self))
        elif hoy < self.apertura:
            raise Exception('{0} aun no inicia. {1}.'.format(desc, self))
        elif hoy > self.cierre:
            raise Exception('{0} ya finalizó. {1}'.format(desc, self))
        return True

    def _assert_envio_oferta(self, subsistema):
        # para modificar o enviar la oferta, el subsistema no debe tener revisión de oferta pendiente o aceptada
        if RevisionOfertaEducativa.objects.filter(subsistema=subsistema, estado=Revision.ESTADO_ACEPTADO).count() > 0:
            raise Exception('Su oferta educativa ya ha sido aceptada, ya no puede modificarla ni reenviarla.')
        if RevisionOfertaEducativa.objects.filter(subsistema=subsistema, estado=Revision.ESTADO_EN_REVISION).count() > 0:
            raise Exception('Su oferta educativa está en revisión, no puede modificarla ni reenviarla.')

    def _assert_envio_aforo(self, subsistema):
        # para modificar o enviar el aforo, el subsistema debe tener la revisión de su oferta académica aceptada
        if RevisionOfertaEducativa.objects.filter(subsistema=subsistema, estado=Revision.ESTADO_ACEPTADO).count() == 0:
            raise Exception('No se puede modificar el aforo de este subsistema hasta que su oferta educativa haya sido aceptada.')
        # para modificar o enviar el aforo, el subsistema no debe tener revisión de aforo pendiente o aceptado
        if RevisionAforo.objects.filter(subsistema=subsistema, estado=Revision.ESTADO_ACEPTADO).count() > 0:
            raise Exception('El aforo de aplicación de este subsistema ya ha sido aceptado, ya no puede modificarlo ni reenviarlo.')
        if RevisionAforo.objects.filter(subsistema=subsistema, estado=Revision.ESTADO_EN_REVISION).count() > 0:
            raise Exception('El aforo de aplicación de este subsistema está en revisión, no puede modificarlo ni reenviarlo.')
        pass

    def _assert_envio_registro(self, aspirante, validar_informacion):
        etapa_aforo = EtapaProceso.objects.get(nombre='AFORO')
        etapa_aforo.asegurar_captura_completa()
        etapa_oferta = EtapaProceso.objects.get(nombre='OFERTA')
        etapa_oferta.asegurar_captura_completa()
        # asegurar revisiones
        revision = RevisionRegistro.revision_activa(aspirante)
        if revision:
            if revision.estado == Revision.ESTADO_ACEPTADO:
                raise Exception('Tu registro ya ha terminado correctamente, no puedes reenviarlo')
            elif revision.estado == Revision.ESTADO_EN_REVISION:
                if revision.pago_efectuado:
                    raise Exception('Ya no puedes reenviar, tu pago ha sido aceptado, sólo falta procesar tu registro')
                raise Exception('Tu pago aun no ha sido procesado, tu envío continúa en revisión')
        # si no se requiere validar automáticamente la información... (p.ej. para guardado temporal)
        if not validar_informacion: return
        # opciones educativas
        SeleccionOfertaEducativa.validar_seleccion(aspirante)
        # encuesta
        # El integrar con ceneval la encuesta socioeconmica se quita como requisito para el envío del registro
        '''
		if aspirante.encuesta_socioeconomica is None:
			raise Exception('Aun no has contestado tu encuesta socioeconómica')
		aspirante.encuesta_socioeconomica.validar()
		'''
        # secundario de procedencia
        if aspirante.informacion_procedencia is None:
            raise Exception('Aun no has completado tu información de procedencia')
        aspirante.informacion_procedencia.validar()

    def __str__(self):
        desc = 'La etapa de {0}'.format(self.nombre)
        if self.apertura is None and self.cierre is None:
            desc = '{0} no tiene un periodo establecido'.format(desc)
        else:
            if self.apertura is not None:
                desc = '{0} inicia en el {1}{2}'.format(desc, self.apertura, ' y ' if self.cierre is not None else '')
            if self.cierre is not None:
                desc = '{0} termina en el {1}'.format(desc, self.cierre)
        return desc


class Configuracion(models.Model):
    descripcion = models.CharField(max_length=255)
    valor = models.CharField(max_length=255)
    obligatorio = models.BooleanField()

    class Meta:
        db_table = 'configuracion'


class Enlace(models.Model):
    municipio = models.IntegerField()
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()
    hora_inicio = models.TimeField()
    hora_fin = models.TimeField()
    domicilio = models.TextField()
    telefono = models.CharField(max_length=10)
    extension = models.CharField(max_length=5)

    class Meta:
        db_table = 'enlace'

    @property
    def municipio_name(self):
        return Municipio.objects.get(id=self.municipio)

    @property
    def fecha_rango(self):
        return "%s %s - %s %s" % (self.fecha_inicio, self.hora_inicio, self.fecha_fin, self.hora_fin)


class Folio(models.Model):
    inicio = models.PositiveIntegerField(verbose_name='Primer folio')
    fin = models.PositiveIntegerField(verbose_name='Último folio')

    class Meta:
        db_table = 'folios'

    def __unicode__(self):
        return "%s - %s" % (self.inicio, self.fin)

    @staticmethod
    def get_folio(maximo):
        try:
            folios = Folio.objects.first()
            folio = maximo + 1
            if folios.inicio <= folio <= folios.fin:
                return folio
            return None
        except Folio.DoesNotExist:
            return None
