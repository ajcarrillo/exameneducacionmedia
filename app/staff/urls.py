# -*- encoding: utf-8 -*
# -*- encoding: utf-8 -*
from django.conf.urls import url

from app.staff import views

# **************************************
#
#   url: /administracion/
#   namespace: administracion
#
# **************************************

urlpatterns = [
    url(r'^view-siem-log-file/$', views.ReadSiemLogTemplateView.as_view(), name='siem_log_file'),
]
