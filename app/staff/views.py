# -*- encoding: utf-8 -*
from django.shortcuts import redirect
from django.urls import reverse
from django.views import generic

from exameneducacionmedia.settings import BASE_DIR
from general.mixins import LoginRequiredMixin, GroupRequiredMixin


class ReadSiemLogTemplateView(LoginRequiredMixin, GroupRequiredMixin, generic.TemplateView):
    group_required = 'departamento'
    template_name = 'read_siem_log_file.html'

    def dispatch(self, request, *args, **kwargs):
        dispatch = super(ReadSiemLogTemplateView, self).dispatch(request, *args, **kwargs)
        if not request.user.is_staff:
            return redirect(reverse('administracion:dashboard'))
        return dispatch

    def get_context_data(self, **kwargs):
        context = super(ReadSiemLogTemplateView, self).get_context_data(**kwargs)
        lines = None
        with open(BASE_DIR + '/siem.log') as fp:
            lines = fp.read().replace('\n', '<br>')
        context.update({'lines': lines})
        return context
