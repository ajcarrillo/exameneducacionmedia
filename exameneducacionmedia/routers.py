# -*- encoding: utf-8 -*
class ControlEscolarRouter(object):
    def db_for_read(self, model, **hints):
        if model._meta.db_table in ('siceeb_t_alumnos', 'siceeb_t_datos_alumno'):
            return 'siceeb'
        return None

    def allow_syncdb(self, db, model):
        if db == 'siceeb':
            return False
        return None


class BillyRouter(object):
    def db_for_read(self, model, **hints):
        if model._meta.db_table in ('banco', 'convenio', 'concepto', 'reporte_deposito', 'deposito', 'referencia_pago', 'contribuyente', 'convenio_pago_concepto', 'salario_minimo', 'solicitud_pago',):
            return 'pago_derechos_1'
        return None

    def db_for_write(self, model, **hints):
        if model._meta.db_table in ('banco', 'convenio', 'concepto', 'reporte_deposito', 'deposito', 'referencia_pago', 'contribuyente', 'convenio_pago_concepto', 'salario_minimo', 'solicitud_pago',):
            return 'pago_derechos_1'
        return None

    def allow_syncdb(self, db, model):
        if db is 'pago_derechos_1':
            return False
        return None


'''
class MarcoGeograficoRouter(object):
	def db_for_read(self, model, **hints):
		if model._meta.db_table in ['entidad_federativa', 'municipio', 'localidad']:
			return 'mginegi'
		return None

	def allow_syncdb(Self, db, model):
		if db == 'mginegi':
			return False
		return None
'''
