# -*- encoding: utf-8 -*
import re
from django import forms
from django.core.exceptions import ValidationError
from django.forms.utils import ErrorDict


class BetterForm(forms.Form):
    def labeled_errors(self):
        labeled_errors = ErrorDict()
        for field in self:
            if field.name in self.errors:
                labeled_errors[field.label] = self.errors[field.name]
        return labeled_errors


class CurpField(forms.CharField):
    def clean(self, value):
        try:
            curp = value.upper()
            regexp = re.compile(
                "^([A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9])*$")
            if curp == '000000000000000000' or regexp.match(curp):
                return curp
            raise Exception('Tu CURP no tiene formato valido. Puedes consultar tu CURP en http://consultas.curp.gob.mx/')
        except Exception as e:
            raise ValidationError(message=e)


class CCTField(forms.CharField):
    def clean(self, value):
        try:
            cct = value.upper()
            regexp = re.compile("^([0-9]{2})([A-Z]{3})([0-9]{4})([A-Z]{1})$")
            if len(cct) > 0:
                if regexp.match(cct):
                    return cct
                else:
                    raise Exception('La Clave de Centro de trabajo de tu secundaria no tiene el formato correcto.')
        except Exception as e:
            raise ValidationError(message=e)


class PhoneField(forms.CharField):
    def clean(self, value):
        try:
            phone = value.upper()
            regexp = re.compile("^[0-9]{10}$")
            if len(phone) > 0:
                if regexp.match(phone):
                    return phone
                else:
                    raise Exception('El teléfono no tiene el formato correcto')
        except Exception as e:
            raise ValidationError(message=e)
