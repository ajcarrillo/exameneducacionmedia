"""exameneducacionmedia URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from app.accounts.views import LoginRedirectView, LogoutRedirectView
from exameneducacionmedia import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^', include('app.home.urls', namespace='home')),

    url(r'^administracion/', include('app.administracion.urls', namespace='administracion')),
    url(r'^administracion/', include('app.staff.urls', namespace='administracion')),
    url(r'^aspirantes/', include('app.aspirantes.urls', namespace='aspirantes')),
    url(r'^accounts/', include('app.accounts.urls', namespace='accounts')),
    url(r'^planteles/', include('app.planteles.urls', namespace='planteles')),
    url(r'^reportes/', include('app.reportes.urls', namespace='reportes')),
    url(r'^subsistemas/', include('app.subsistemas.urls', namespace='subsistemas')),

    url(r'^mgeo/', include('app.mgeo.urls', namespace='mgeo')),
    url(r'^login/$', LoginRedirectView.as_view(), name='redirect_to_login'),
    url(r'^logout/$', LogoutRedirectView.as_view(), name='redirect_to_logout'),

    url(r'^billy/', include('app.billy.urls', namespace='billy')),

    url(r'^finanzas/', include('app.finanzas.urls', namespace='finanzas')),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
