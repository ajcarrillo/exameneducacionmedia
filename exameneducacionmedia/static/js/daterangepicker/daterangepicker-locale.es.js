"use strict";
/**
 * Created by andres on 01/11/16.
 */
var locale = {
	format: "YYYY-MM-DD",
	applyLabel: "Aceptar",
	cancelLabel: "Cancelar",
	fromLabel: "De",
	toLabel: "Hasta",
	daysOfWeek: [
		"Dom",
		"Lun",
		"Mar",
		"Mie",
		"Jue",
		"Vie",
		"Sab"
	],
	monthNames: [
		"Enero",
		"Febrero",
		"Marzo",
		"Abril",
		"Mayo",
		"Junio",
		"Julio",
		"Agosto",
		"Septiembre",
		"Octubre",
		"Noviembre",
		"Diciembre"
	]
};
