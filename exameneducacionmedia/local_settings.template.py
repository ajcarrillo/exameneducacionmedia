import os

DEBUG = True

MAINTENANCE_MODE = False

PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))

DATABASES = {
    'default':       {
        'ENGINE':   'django.db.backends.mysql',
        'NAME':     'exameneducacionmedia',
        'USER':     '',
        'PASSWORD': '',
        'HOST':     '127.0.0.1',
        'PORT':     '',
    },
    'siceeb':        {
        'ENGINE':   'django.db.backends.mysql',
        'NAME':     'siceeb',
        'USER':     '',
        'PASSWORD': '',
        'HOST':     '127.0.0.1',
        'PORT':     '',
    },
    'mginegi':       {
        'ENGINE':   'django.db.backends.mysql',
        'NAME':     'mginegi',
        'USER':     '',
        'PASSWORD': '',
        'HOST':     '127.0.0.1',
        'PORT':     '',
    },
    'pago_derechos_1': {
        'ENGINE':   'django.db.backends.mysql',
        'NAME':     'pago_derechos_1',
        'USER':     '',
        'PASSWORD': '',
        'HOST':     '127.0.0.1',
        'PORT':     '',
    }
}

SECRET_KEY = 'Go to your terminal en type, ./manage.py keygen, then copy the string result and paste here :D'

RENAPO_URL = 'IP Address to get renapo service'

BILLY_SERVICE_URL = 'IP Address to get billy service'

STATIC_DIR = "path/to/static/files"

SPARKPOST = {
    'SPARKPOST_API_KEY':  'YOUR API KEY',
    'DEFAULT_FROM_EMAIL': 'YOUR DEFAULT FROM EMAIL'
}
