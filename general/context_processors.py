# -*- encoding: utf-8 -*
from django.contrib.auth.models import Group
from django.urls import reverse

menus = {
    'departamento': {
        'menu': [
            {'name': 'Cambiar contraseña', 'submenus': [], 'url': reverse('accounts:change_password'), 'groups': ['departamento', 'plantel', 'subsistema']},
            {'name': 'Panel de control Depto.', 'submenus': [], 'url': reverse('administracion:dashboard'), 'groups': ['departamento']},
            {'name': 'Panel de control Plantel', 'submenus': [], 'url': reverse('planteles:home'), 'groups': ['plantel']},
            {'name':     'Finanzas', 'submenus': [
                {'name': 'Carga de pagos', 'url': reverse('finanzas:procesar_pagos'), 'groups': ['finanzas']},
                {'name': 'Problema de pago', 'url': reverse('finanzas:problema_de_pago'), 'groups': ['finanzas']},
            ], 'groups': ['finanzas'], 'url': None},
            {'name':     'Configuración del Proceso', 'submenus': [
                {'name': 'Enlaces', 'url': reverse('administracion:enlaces'), 'groups': ['departamento']},
                {'name': 'Etapas del proceso', 'url': reverse('administracion:etapas'), 'groups': ['departamento']},
                {'name': 'Miscelaneos', 'url': reverse('administracion:miscelaneos'), 'groups': ['departamento']},
            ], 'groups': ['departamento'], 'url': None},
            {'name':     'Administración', 'submenus': [
                {'name': 'Aspirantes', 'url': reverse('administracion:aspirantes_list_view'), 'groups': ['departamento', 'plantel']},
                {'name': 'Nivel de demanda', 'url': reverse('administracion:list_municipios'), 'groups': ['departamento']},
                {'name': 'Planteles', 'url': reverse('administracion:plantel_list'), 'groups': ['departamento']},
                {'name': 'Sedes alternas', 'url': reverse('administracion:sedes_list'), 'groups': ['departamento']},
                {'name': 'Subsistemas', 'url': reverse('administracion:subsistemas_list'), 'groups': ['departamento']},
                {'name': 'Usuarios', 'url': reverse('administracion:users'), 'groups': ['departamento']},
            ], 'groups': ['departamento', 'plantel'], 'url': None},
            {'name':     'Revisión', 'submenus': [
                {'name': 'Aforo', 'url': reverse('administracion:revision_aforo'), 'groups': ['departamento']},
                {'name': 'Oferta educativa', 'url': reverse('administracion:revision_oferta'), 'groups': ['departamento']},
            ], 'groups': ['departamento'], 'url': None},
            {'name': 'Planteles', 'url': None, 'groups': ['subsistema'], 'submenus': [
                {'name': 'Planteles', 'url': reverse('subsistemas:subsistemas_start'), 'groups': ['subsistema']},
                {'name': 'Especialidades', 'url': reverse('subsistemas:especialidades_list'), 'groups': ['subsistema']}
            ]},
        ]
    }
}


def groups(request):
    def inner():
        return Group.objects.exclude(name='alumno').values('id', 'name')

    return {'ctx_groups': inner}


def menu(request):
    def inner():
        m = {'menu': []}
        mm = menus['departamento']
        user_groups = request.user.groups.values_list("name", flat=True)
        for item in mm['menu']:
            if set(item['groups']).intersection(set(user_groups)):
                submenus = []
                me = {'name': item['name'], 'url': '' if item['url'] is None else item['url']}
                if item['submenus']:
                    for submenu in item['submenus']:
                        if set(submenu['groups']).intersection(set(user_groups)):
                            sub = {'name': submenu['name'], 'url': '' if submenu['url'] is None else submenu['url']}
                            submenus.append(sub)
                            me['submenus'] = submenus
                m['menu'].append(me)

        return {'menu': m}

    return inner()
