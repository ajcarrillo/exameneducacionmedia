# -*- encoding: utf-8 -*
TIPOS_ESCUELA = ('TELESECUNDARIA', 'GENERAL', 'PARTICULAR', 'TECNICA', 'ABIERTA', 'CONAFE')
NIVELES_ESTUDIO = ('BACHILLERATO', 'LICENCIATURA', 'TECNICO SUPERIOR', 'POSGRADO')
SI_NO = ('SI', 'NO')
EXAMENES_EXTRAODINARIOS = ('0', '1', '2', '3', '4', '5')
MATERIAS_REPROBADAS = {
    '0': 'Ninguna',
    '2': '1 - 2',
    '4': '3 - 4',
    '5': '5 o más'
}
PROMEDIOS_GENERALES = {
    '7':  '6.0 - 7.0',
    '8':  '7.1 - 8.0',
    '9':  '8.1 - 9.0',
    '10': '9.1 a 10.0'
}
TIPOS_CONVOCATORIA = ('EXPORIENTA', 'INTERNET', 'RADIO', 'TELEVISION', 'OTRA')
LIBROS_LEIDOS = {
    '0': 'Ninguno',
    '2': 'De 1 a 2',
    '4': 'De 3 a 4',
    '5': 'Más de 4'
}
TIPOS_VIVIENDA = ('PROPIA', 'RENTADA', 'PRESTADA', 'PENSION')
SOSTENES_ECONOMICOS = ('PADRE', 'MADRE', 'AMBOS', 'HERMANOS', 'TU', 'OTRO')
GRADOS_ESCOLARES_PADRES = (
    'SIN ESTUDIOS', 'PRIMARIA', 'SECUNDARIA', 'BACHILLERATO', 'TECNICO', 'LICENCIATURA', 'POSGRADO')
TRASLADOS_CASA_ESCUELA = ('CAMION', 'AUTO', 'TAXI', 'PIE', 'PUBLICO', 'BICICLETA', 'MOTOCICLETA', 'OTRO')
BIENES_SERVICIOS = (
    'TELEFONO', 'DRENAJE', 'AUTOMOVIL', 'ALUMBRADO', 'TELEVISOR', 'DVD', 'CABLEVISION', 'INTERNET', 'MP3', 'MICROONDAS')
TOTAL_PERSONAS_CASA = {
    '2': '1 - 2',
    '4': '3 - 4',
    '6': '5 - 6',
    '7': 'Más de 6'
}
OCUPACIONES_PADRES = {
    'FUNCIONARIO':  'Funcionario o gerente (nivel directivo)',
    'NEGOCIO':      'Dueño de negocio, empresa, doctor, despacho o comercio establecido',
    'PROFESOR':     'Profesor de primaria, secundaria, licenciatura o posgrado',
    'EMPLEADO':     'Empleado administrativo',
    'TRANSPORTE':   'Operador de transporte',
    'CONSTRUCCION': 'Trabajador de la construcción',
    'AGROPECUARIO': 'Trabajador agropecuario',
    # 'VENDEDOR': 'Vendedor en comercio o empresa',
    'COMERCIANTE':  'Comerciante',
    'CASA':         'Ama de casa',
    'JUBILADO':     'Jubilado',
    'GOBIERNO':     'Empleado de Gobierno',
    'OTRA':         'Otra'
}
DEPENDIENTES_ECONOMICOS = {
    '1': '1',
    '3': '2 - 3',
    '5': '4 - 5',
    '6': 'Más de 6'
}
INGRESOS_MENSUALES = {
    '2000':  '$1,000 a $2,000',
    '3000':  '$2,001 a $3,000',
    '4000':  '$3,001 a $4,000',
    '5000':  '$4,001 a $5,000',
    '7000':  '$5,001 a $7,000',
    '10000': '$7,001 a $10,000',
    '10001': 'Más de $10,000',
}
SALARIOS_MENSUALES = {
    '500':   'Menos de $500',
    '1500':  '$501 a $1,500',
    '3000':  '$1,501 a $3,000',
    '5000':  '$3,001 a $5,000',
    '8000':  '$5,001 a $8,000',
    '10000': '$8,001 a $10,000',
    '10001': 'Más de $10,000',
}
TIPOS_TRABAJO = {
    'NINGUNO':   'No trabajo',
    'FAMILIAR':  'Trabajo familiar',
    'CONTRATO':  'Trabajo permanente con plazo o contrato',
    'TEMPORADA': 'Trabajo por temporadas'
}
HORAS_TRABAJO_SEMANAL = {
    '15': 'Menos de 15',
    '20': 'Entre 15 y 20',
    '30': 'Entre 21 y 30',
    '31': 'Más de 30'
}
HORAS_DEPORTE_SEMANAL = {
    '3':  '1 - 3',
    '7':  '4 - 7',
    '10': '8 - 10',
    '11': 'Más de 10'
}
GENDER_CHOICES = (
    ('M', 'Masculino'),
    ('F', 'Femenino')
)

CARACTERISTICAS_BUENA_ESCUELA = (
    'BIBLIOTECA', 'CAFETERIA', 'AREAS_VERDES', 'CANCHAS', 'WIFI', 'MAESTROS', 'IDIOMAS', 'INTERCAMBIO',
    'PRACTICA', 'ACTIVIDADES', 'LECTURA', 'TEC_INTERACTIVA', 'BEBEDEROS', 'CLIMA', 'MOBILIARIO',
    'LABORATORIOS', 'BECAS', 'RADIO', 'PAPELERIA', 'LOCKERS', 'SEGURIDAD', 'AUDITORIO',
    'AUDIOVISUAL', 'AMBIENTE')

CHOICES_USUARIO_GRUPOS = (
    ('subsistema', 'Subsistema'),
    ('departamento', 'Departamento'),
    ('finanzas', 'Finanzas'),
    ('plantel', 'Plantel educativo'),
    ('capturista_extemporaneo', 'Capturista de la etapa extemporánea'),
    ('apoyo', 'Apoyo para la Coordinación'),
    ('coordinador', 'Coordinador general del proceso de ingreso')
)

NIVEL_DEMANDA_DEFAULT = 'NORMAL'
NIVELES_DEMANDA = {
    'ALTO':   10,
    'BAJO':   1,
    'NORMAL': 5
}
PROGRAMAS_ESTUDIO = (
    ('EDUCACION', 'Educación'),
    ('ARTES', 'Artes y Humanidades'),
    ('SOCIALES', 'Ciencias sociales, administración y derecho'),
    ('NATURALES', 'Ciencias naturales, exactas y de la computación'),
    ('INGENIERIA', 'Ingeniería, manufactura y construcción'),
    ('AGRONOMIA', 'Agronomía y veterinaria'),
    ('SALUD', 'Salud'),
    ('SERVICIOS', 'Servicios'),
)

PROGRAMAS_ESTUDIO_AND_EMPTY = (
    ('', 'Programa de estudio'),
    ('EDUCACION', 'Educación'),
    ('ARTES', 'Artes y Humanidades'),
    ('SOCIALES', 'Ciencias sociales, administración y derecho'),
    ('NATURALES', 'Ciencias naturales, exactas y de la computación'),
    ('INGENIERIA', 'Ingeniería, manufactura y construcción'),
    ('AGRONOMIA', 'Agronomía y veterinaria'),
    ('SALUD', 'Salud'),
    ('SERVICIOS', 'Servicios'),
)

NIVEL_DEMANDA = (
    ('ALTO', 'ALTO'),
    ('BAJO', 'BAJO'),
    ('NORMAL', 'NORMAL'),
)
