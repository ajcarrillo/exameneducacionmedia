# -*- encoding: utf-8 -*
from __future__ import print_function
import string
import random
from django.core.management import BaseCommand


class Command(BaseCommand):
    help = ""

    def handle(self, *args, **options):
        """
		Pseudo-random django secret key generator.
		- Does print SECRET key to terminal which can be seen as unsafe.
		"""

        # Get ascii Characters numbers and punctuation (minus quote characters as they could terminate string).
        chars = ''.join([string.ascii_letters, string.digits, string.punctuation]).replace('\'', '').replace('"', '').replace('\\', '')

        secret_key = ''.join([random.SystemRandom().choice(chars) for i in range(50)])

        print(secret_key)
