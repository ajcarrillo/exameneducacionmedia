# -*- encoding: utf-8 -*

import logging
from exameneducacionmedia import local_settings
from django.core.management.base import CommandError
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Resets a database."

    def handle(self, *args, **options):
        """
        Resets a database.

        Note: Transaction wrappers are in reverse as a work around for
        autocommit, anybody know how to do this the right way?
        """

        engine = local_settings.DATABASES['default']['ENGINE']

        if engine == 'sqlite3':
            import os
            try:
                logging.info("Unlinking sqlite3 database")
                os.unlink(local_settings.DATABASES['default']['NAME'])
            except OSError:
                pass
        elif engine == 'django.db.backends.mysql':
            import MySQLdb as Database
            kwargs = {
                'user'  : local_settings.DATABASES['default']['USER'],
                'passwd': local_settings.DATABASES['default']['PASSWORD'],
            }
            if local_settings.DATABASES['default']['HOST'].startswith('/'):
                kwargs['unix_socket'] = local_settings.DATABASES['default']['HOST']
            else:
                kwargs['host'] = local_settings.DATABASES['default']['HOST']
            if local_settings.DATABASES['default']['PORT']:
                kwargs['port'] = int(local_settings.DATABASES['default']['PORT'])
            connection = Database.connect(**kwargs)
            drop_query = 'DROP DATABASE IF EXISTS %s' % local_settings.DATABASES['default']['NAME']
            create_query = 'CREATE DATABASE %s' % local_settings.DATABASES['default']['NAME']
            logging.info('Executing... "' + drop_query + '"')
            connection.query(drop_query)
            logging.info('Executing... "' + create_query + '"')
            connection.query(create_query)
        elif engine == 'postgresql' or engine == 'postgresql_psycopg2':
            if engine == 'postgresql':
                import psycopg as Database
            elif engine == 'postgresql_psycopg2':
                import psycopg2 as Database

            if local_settings.DATABASES['default']['NAME'] == '':
                from django.core.exceptions import ImproperlyConfigured
                raise ImproperlyConfigured, "You need to specify DATABASE_NAME in your Django settings file."
            if local_settings.DATABASES['default']['USER']:
                conn_string = "user=%s" % (local_settings.DATABASES['default']['USER'])
            if local_settings.DATABASES['default']['PASSWORD']:
                conn_string += " password='%s'" % local_settings.DATABASES['default']['PASSWORD']
            if local_settings.DATABASES['default']['HOST']:
                conn_string += " host=%s" % local_settings.DATABASES['default']['HOST']
            if local_settings.DATABASES['default']['PORT']:
                conn_string += " port=%s" % local_settings.DATABASES['default']['PORT']
            connection = Database.connect(conn_string)
            connection.set_isolation_level(0)  # autocommit false
            cursor = connection.cursor()
            drop_query = 'DROP DATABASE %s' % local_settings.DATABASES['default']['NAME']
            logging.info('Executing... "' + drop_query + '"')

            try:
                cursor.execute(drop_query)
            except Database.ProgrammingError, e:
                logging.info("Error: " + str(e))

            # Encoding should be SQL_ASCII (7-bit postgres default) or prefered UTF8 (8-bit)
            create_query = ("""
CREATE DATABASE %s
    WITH OWNER = %s
        ENCODING = 'UTF8'
        TABLESPACE = pg_default;
""" % (local_settings.DATABASES['default']['NAME'], local_settings.DATABASES['default']['USER']))
            logging.info('Executing... "' + create_query + '"')
            cursor.execute(create_query)

        else:
            raise CommandError, "Unknown database engine %s", engine

        logging.info("Reset success")
