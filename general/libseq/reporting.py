# -*- encoding: utf-8 -*-
import requests
import urllib
import base64
import json
import settings


class PentahoReport(object):
    service_url = 'http://127.0.0.1:8080/report/service'
    name = None
    format = None
    project = None
    _parameters = dict()

    def __init__(self, name=None, project=None):
        self.name = name
        self.format = 'pdf'

    def set(self, key, value):
        self._parameters[key] = value

    def get(self, key):
        if key in self._parameters:
            return self._parameters[key]
        return None

    def fetch(self, format=None):
        if format is None:
            format = self.format
        if format not in ('pdf', 'rtf', 'xls'):
            raise Exception('Formato no válido')

        request = requests.get(str(self), auth=requests.auth.HTTPBasicAuth('seq', 'seq'))
        return request.content

    def __str__(self):
        request_def = dict()
        request_def['name'] = self.name if self.name is not None else ''
        request_def['format'] = self.format if self.format is not None else ''
        request_def['project'] = self.project if self.project is not None else ''
        request_def['data'] = base64.b64encode(json.dumps(self._parameters))
        if 'reporting_service_url' not in settings.REPORTING:
            raise Exception('Se necesita la configuración REPORTING[reporting_service_url]')
        return '%s?%s' % (settings.REPORTING['reporting_service_url'], urllib.urlencode(request_def))
