# -*- encoding: utf-8 -*
from django.urls import reverse

menus = {
    'departamento': {
        'menu': [
            {
                'Finanzas':                  [
                    {'name': 'Carga de pagos', 'url': reverse('finanzas:procesar_pagos')},
                    {'name': 'Planteles', 'url': reverse('finanzas:problema_de_pago')},
                ],
                'Configuración del Proceso': [
                    {'name': 'Etapas del proceso', 'url': reverse('administracion:etapas')},
                    {'name': 'Miscelaneos', 'url': reverse('administracion:miscelaneos')},
                    {'name': 'Enlaces', 'url': reverse('administracion:enlaces')},
                ],
                'Administración':            [
                    {'name': 'Aspirantes', 'url': reverse('administracion:aspirantes_list_view')},
                    {'name': 'Subsistemas', 'url': reverse('administracion:subsistemas_list')},
                    {'name': 'Planteles', 'url': reverse('administracion:plantel_list')},
                    {'name': 'Nivel de demanda', 'url': reverse('administracion:list_municipios')},
                    {'name': 'Sedes alternas', 'url': reverse('administracion:sedes_list')},
                    {'name': 'Usuarios', 'url': reverse('administracion:users')},
                ],
                'Revisión':                  [
                    {'name': 'Oferta educativa', 'url': reverse('administracion:revision_oferta')},
                    {'name': 'Aforo', 'url': reverse('administracion:revision_aforo')},
                ]
            }
        ]
    },
}
