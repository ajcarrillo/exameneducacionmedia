# -*- encoding: utf-8 -*
from __future__ import division
import datetime
import inspect
from math import floor

from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import ImproperlyConfigured, PermissionDenied
from django.db.models import Q
from django.http import HttpResponse, JsonResponse, StreamingHttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils import six
from django.utils.decorators import method_decorator
from django.utils.encoding import force_text

from app.accounts.asserts import user_module_administracion, user_module_subsistema, user_module_registro
from app.accounts.templatetags.account_extras import has_group
from app.administracion.models import RevisionAforo, RevisionOfertaEducativa, Revision, EtapaProceso
from app.aspirantes.models import Aspirante
from app.planteles.models import Subsistema


class UtilsMixin(object):
    def subsistema_asignado(self):
        try:
            return Subsistema.objects.get(responsable=self.request.user)
        except Subsistema.DoesNotExist:
            raise Exception('Usted no es reponsable de ningún subsistema educativo')


class JsonResponseMixin(object):
    def response_handler(self):
        format = self.request.GET.get('format', None)
        if format == 'json':
            return self.json_to_response()

        context = self.get_context_data()
        return self.render_to_response(context)

    def json_to_response(self):
        data = self.get_data()
        return JsonResponse(data, safe=False)


class AjaxableResponseMixin(object):
    def form_invalid(self, form):
        response = super(AjaxableResponseMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super(AjaxableResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
            }
            return JsonResponse(data)
        else:
            return response


class LoginRequiredMixin(object):
    @method_decorator(login_required(login_url='/login/'))
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class NeedsDepartamentoGroupAccessMixin(object):
    @method_decorator(user_passes_test(user_module_administracion, login_url='/login/'))
    def dispatch(self, request, *args, **kwargs):
        response = None
        try:
            http_response = super(NeedsDepartamentoGroupAccessMixin, self).dispatch(request, *args, **kwargs)
            response = http_response if http_response is not None else response
        except Exception as e:
            if not request.is_ajax():
                context = {'error': e, 'regresar_a': request.META.get('HTTP_REFERER')}
                response = render(self.request, 'errors.html', context)
            else:
                response = JsonResponse({'is_valid': False, 'error': e.message})

        return response


class NeedsSubsistemaGroupAccessMixin(object):
    @method_decorator(user_passes_test(user_module_subsistema, login_url='/login/'))
    def dispatch(self, request, *args, **kwargs):
        response = None
        try:
            http_response = super(NeedsSubsistemaGroupAccessMixin, self).dispatch(request, *args, **kwargs)
            response = http_response if http_response is not None else response
        except Exception as e:
            if not request.is_ajax():
                context = {'error': e, 'regresar_a': request.META.get('HTTP_REFERER')}
                response = render(self.request, 'errors.html', context)
            else:
                response = JsonResponse({'is_valid': False, 'error': e.message})

        return response

    def subsistema_asignado(self, user):
        try:
            return Subsistema.objects.get(responsable=user)
        except Subsistema.DoesNotExist:
            raise Exception('Usted no es reponsable de ningún subsistema educativo')

    def tiene_revision_pendiente(self, user):
        subsistema = self.subsistema_asignado(user)
        if RevisionOfertaEducativa.objects.filter(subsistema=subsistema).filter(
                Q(estado=Revision.ESTADO_EN_REVISION) | Q(estado=Revision.ESTADO_ACEPTADO)).exists():
            raise Exception("La oferta no se puede modificar ya que se encuentra en revisión o ha sido aceptada")

    def tiene_revision_aforo_pendiente(self, user):
        subsistema = self.subsistema_asignado(user)
        if RevisionAforo.objects.filter(subsistema=subsistema).filter(
                Q(estado=Revision.ESTADO_EN_REVISION) | Q(estado=Revision.ESTADO_ACEPTADO)).exists():
            raise Exception("El aforo no se puede modificar ya que se encuentra en revisión o ha sido aceptada")


class SubsistemaUtils(object):
    def dispatch(self, request, *args, **kwargs):
        response = None
        try:
            http_response = super(SubsistemaUtils, self).dispatch(request, *args, **kwargs)
            response = http_response if http_response is not None else response
        except Exception as e:
            if not request.is_ajax():
                context = {'error': e, 'regresar_a': request.META.get('HTTP_REFERER')}
                response = render(self.request, 'errors.html', context)
            else:
                response = JsonResponse({'is_valid': False, 'error': e.message})

        return response

    def subsistema_asignado(self, user):
        try:
            return Subsistema.objects.get(responsable=user)
        except Subsistema.DoesNotExist:
            raise Exception('Usted no es reponsable de ningún subsistema educativo')

    def tiene_revision_pendiente(self, user):
        subsistema = self.subsistema_asignado(user)
        if RevisionOfertaEducativa.objects.filter(subsistema=subsistema).filter(
                Q(estado=Revision.ESTADO_EN_REVISION) | Q(estado=Revision.ESTADO_ACEPTADO)).exists():
            raise Exception("No se pueden realizar los cambios ya que el subsistema tiene su oferta educativa en revisión o ha sido aceptada")

    def tiene_revision_aforo_pendiente(self, user):
        subsistema = self.subsistema_asignado(user)
        if RevisionAforo.objects.filter(subsistema=subsistema).filter(
                Q(estado=Revision.ESTADO_EN_REVISION) | Q(estado=Revision.ESTADO_ACEPTADO)).exists():
            raise Exception("El aforo no se puede modificar ya que se encuentra en revisión o ha sido aceptada")


class AsegurarEtapaMixin(object):
    asegurar_etapa_de = None
    today = datetime.date.today()

    def get_etapa(self):
        if self.asegurar_etapa_de is None:
            raise ImproperlyConfigured(
                "%(cls)s is missing an Etapa. Define "
                "%(cls)s.asegurar_etapa_de attribute." % {
                    'cls': self.__class__.__name__
                }
            )
        try:
            etapa = EtapaProceso.objects.get(nombre=self.asegurar_etapa_de.upper())
        except EtapaProceso.DoesNotExist:
            raise Exception("La etapa no existe")
        return etapa

    def asegurar_periodo(self):
        etapa = self.get_etapa()
        desc = 'La etapa de {0} '.format(etapa.nombre)

        if etapa.apertura is None:
            raise Exception('{0} no tiene fecha de apertura.'.format(desc))
        elif etapa.cierre is None:
            raise Exception('{0} no tiene fecha de cierre.'.format(desc))
        elif self.today < etapa.apertura:
            raise Exception('{0} aun no inicia.'.format(desc))
        elif self.today > etapa.cierre:
            raise Exception('{0} ya finalizó.'.format(desc))
        return True

    def dispatch(self, request, *args, **kwargs):
        response = None
        try:
            self.asegurar_periodo()
            http_response = super(AsegurarEtapaMixin, self).dispatch(request, *args, **kwargs)
            response = http_response if http_response is not None else response
        except Exception as e:
            if not request.is_ajax():
                regresar_a = request.META.get('HTTP_REFERER')
                if regresar_a is None:
                    regresar_a = reverse("home:home")
                context = {'error': e.message, 'regresar_a': regresar_a}
                response = render(self.request, 'errors.html', context)
            else:
                response = JsonResponse({'is_valid': False, 'error': e.message})

        return response


class RedirectMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            if has_group(request.user, "departamento"):
                return redirect(reverse('administracion:dashboard'))

            if has_group(request.user, "subsistema"):
                return redirect(reverse('subsistemas:subsistemas_start'))

            if has_group(request.user, "alumno"):
                return redirect(reverse('aspirantes:dashboard'))

        return super(RedirectMixin, self).dispatch(request, *args, **kwargs)


class AccessMixin(object):
    """
    'Abstract' mixin that gives access mixins the same customizable
    functionality.
    """
    login_url = None
    raise_exception = False
    redirect_field_name = REDIRECT_FIELD_NAME  # Set by django.contrib.auth
    redirect_unauthenticated_users = False

    def get_login_url(self):
        """
        Override this method to customize the login_url.
        """
        login_url = self.login_url or settings.LOGIN_URL
        if not login_url:
            raise ImproperlyConfigured(
                'Define {0}.login_url or settings.LOGIN_URL or override '
                '{0}.get_login_url().'.format(self.__class__.__name__))

        return force_text(login_url)

    def get_redirect_field_name(self):
        """
        Override this method to customize the redirect_field_name.
        """
        if self.redirect_field_name is None:
            raise ImproperlyConfigured(
                '{0} is missing the '
                'redirect_field_name. Define {0}.redirect_field_name or '
                'override {0}.get_redirect_field_name().'.format(
                    self.__class__.__name__))
        return self.redirect_field_name

    def handle_no_permission(self, request):
        if self.raise_exception:
            if self.redirect_unauthenticated_users and not self.request.user.is_authenticated():
                return self.no_permissions_fail(request)
            else:
                if inspect.isclass(self.raise_exception) and issubclass(self.raise_exception, Exception):
                    raise self.raise_exception
                if callable(self.raise_exception):
                    ret = self.raise_exception(request)
                    if isinstance(ret, (HttpResponse, StreamingHttpResponse)):
                        return ret
                raise PermissionDenied

        return self.no_permissions_fail(request)

    def no_permissions_fail(self, request=None):
        """
        Called when the user has no permissions and no exception was raised.
        This should only return a valid HTTP response.
        By default we redirect to login.
        """
        return redirect_to_login(request.get_full_path(),
                                 self.get_login_url(),
                                 self.get_redirect_field_name())


class GroupRequiredMixin(AccessMixin):
    group_required = None

    def get_group_required(self):
        if self.group_required is None or (
            not isinstance(self.group_required,
                           (list, tuple) + six.string_types)
        ):
            raise ImproperlyConfigured(
                '{0} requires the "group_required" attribute to be set and be '
                'one of the following types: string, unicode, list or '
                'tuple'.format(self.__class__.__name__))
        if not isinstance(self.group_required, (list, tuple)):
            self.group_required = (self.group_required,)
        return self.group_required

    def check_membership(self, groups):
        """ Check required group(s) """
        if self.request.user.is_superuser:
            return True
        user_groups = self.request.user.groups.values_list("name", flat=True)
        return set(groups).intersection(set(user_groups))

    def dispatch(self, request, *args, **kwargs):
        self.request = request
        in_group = False
        if self.request.user.is_authenticated():
            in_group = self.check_membership(self.get_group_required())

        if not in_group:
            return self.handle_no_permission(request)

        return super(GroupRequiredMixin, self).dispatch(
            request, *args, **kwargs)
