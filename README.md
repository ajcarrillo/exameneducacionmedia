# SIEM - Registro al Exámen de Ingreso a la Educación Media :woman_tone4: :person_with_blond_hair_tone4:

## Dependencias

```
node js
bower
```

## :baby_symbol: Baby steps I

* Hacer una copia del archivo ```loca_settings.template.py``` y cambiar nombre a ```local_settings.py``` 

Instalar dependencias:

- Instalar node desde su sitio [Node website](https://nodejs.org)

```
$ pip install -r requirements.txt
$ sudo npm install -g bower
$ npm install -g bower-npm-resolver
$ bower install
```

### Actualizar dependencias (Backend)

Si instalas un paquete de python deberás actualizar el archivo ```requirements.txt``` y añadir tu nuevo paquete ya sea 
manualmente ó utilizando el comando ```$ pip freeze > requirements.txt```

**Nota: El comando ```$ pip freeze > requirements.txt``` te devuelve todos los paquetes instalados en tu environment, incluidos
los paquetes que no usas en el proyecto, es muy importante que mantengas limpio este archivo de paquetes que no son 
dependencias para el proyecto.**

### Actualizar dependencias (Fronted con Bower)

Si necesitas algún plugin o framework del lado del frontend, de preferencia utiliza el manejador de dependencias [Bower](https://bower.io/).
Para instalar dependencias através de bower ejecuta el siguiente comando:

```
$ bower install <package>#<version> --save
```

Es importante que uses el flag `--save` ya que ésta permite que el paquete que estás instalando se agregue al proyecto como depencia en el archivo
`/bower.json`.

Para desinstalar un paquete ejecuta el siguiente comando:

```
$ bower uninstall <package> --save
```

De nueva cuenta el flag `--save` removerá la dependencia del archivo `/bower.json`


### Generar ```SECRET_KEY```

Para generar la ```SECRET_KEY``` debes ejecutar el siguiente comando:

```
./manage.py keygen
```

Esto dara como resultado una cadena que deberás pegar en tu archivo ```local_settings.py``` en la variable ```SECRET_KEY```.

## :baby_symbol: Baby steps II

### Base de datos

Configuración de tu base de datos.

```python
DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.mysql',
		'NAME': 'YourDatabaseName',
		'USER': 'YourUsername',
		'PASSWORD': 'YourPassword',
		'HOST': 'SomeIPAddress',
		'PORT': 'ThePort',
	}
}
```

Ejecutar el siguiente comando para aplicar las migraciones e inicializar la base de datos

```
$ ./manage.py migrate
```

Si por alguna razón necesitas reiniciar la base de datos tienes disponible el siguiente comando.

```
$ ./manage.py reset_db
```

**Nota: el comando ```reset_db``` elimina TODAS las tablas de de la base de datos, ```TODAS```, por lo cual después de
 ejecutar el comando deberás aplicar las migraciones nuevamente**
 
 
Pre-llenar tablas de base de datos.

El llenado de las tablas se hacen através de scripts escritos en python y ejecutados desde tu terminal:

```
# La ejecución de scripts se realiza con el siguiente comando
$ ./manage.py shell < myscript.py
```

Dentro de la carpeta `scripts`, de cada aplicacion, encontraras los scripts que deberás ejecutar.

## :baby_symbol: Baby steps III

### Pentaho Reports

Clonar proyecto *https://gitlab.com/educacionqroomx/PentahoReport en la carpeta `general`, utilizando el siguiente comando:

```
$ git clone -b report-designer-python https://gitlab.com/educacionqroomx/PentahoReport.git
```

Y sigue las instrucciones del Readme.


Happy Coding!! :slight_smile::upside_down:

P.S. Alimentate sanamente, come frutas y verduras. :tomato::hot_pepper::corn:



